package com.fans.generator.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.generator.entity.TableInfo;
import com.fans.generator.service.IGenService;
import com.fans.utils.utils.Convert;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

@RestController
@RequestMapping("/v1/generator/gen")
public class GenController extends BaseController {

	@Autowired
	private IGenService genService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(TableInfo tableInfo, PageVo pv, String condition, HttpServletResponse response) {
		pv.setSortField("create_time");
		pv.setSortOrder("desc");
		Page<TableInfo> page = new PageFactory<TableInfo>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(tableInfo);
		map.put("condition", condition);
		List<TableInfo> result = genService.selectTableList(page, tableInfo);
		page.setRecords(result);
		return renderString(response, FansResp.successData(page));
	}
	
	/**
     * 生成代码
     */
	@PostMapping("/genCode/{tableName}")
    public void genCode(HttpServletResponse response, @PathVariable("tableName") String tableName) throws IOException
    {
        byte[] data = genService.generatorCode(tableName);
        IOUtils.write(data, response.getOutputStream());
    }


	/**
	 * 批量生成代码
	 */
	@PostMapping("/batchGenCode")
	public void batchGenCode2(HttpServletResponse response, String tableNames) throws IOException {
		String[] tableNamesArr = Convert.toStrArray(tableNames);
		byte[] data = genService.generatorCode(tableNamesArr);
		IOUtils.write(data, response.getOutputStream());
	}
}
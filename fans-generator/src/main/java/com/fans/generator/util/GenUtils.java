package com.fans.generator.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.velocity.VelocityContext;

import com.fans.common.config.Global;
import com.fans.common.constant.SysConstant;
import com.fans.generator.entity.ColumnInfo;
import com.fans.generator.entity.TableInfo;
import com.fans.utils.utils.DateUtils;
import com.fans.utils.utils.StringUtils;

/**
 * 代码生成器 工具类
 * 
 * @author fanhaohao
 */
public class GenUtils
{
    /** 项目空间路径 */
    private static final String PROJECT_PATH = getProjectPath();

    /** mybatis空间路径 */
    private static final String MYBATIS_PATH = "main/resources/mapper";

    /** html空间路径 */
	private static final String TEMPLATES_PATH = "main/resources/views";

    /** 类型转换 */
    public static Map<String, String> javaTypeMap = new HashMap<String, String>();

    /**
     * 设置列信息
     */
    public static List<ColumnInfo> transColums(List<ColumnInfo> columns)
    {
        // 列信息
        List<ColumnInfo> columsList = new ArrayList<>();
        for (ColumnInfo column : columns)
        {
            // 列名转换成Java属性名
            String attrName = StringUtils.convertToCamelCase(column.getColumnName());
            column.setAttrName(attrName);
            column.setAttrname(StringUtils.uncapitalize(attrName));

            // 列的数据类型，转换成Java类型
            String attrType = javaTypeMap.get(column.getDataType());
            column.setAttrType(attrType);

            columsList.add(column);
        }
        return columsList;
    }

    /**
     * 获取模板信息
     * 
     * @return 模板列表
     */
    public static VelocityContext getVelocityContext(TableInfo table)
    {
        // java对象数据传递到模板文件vm
        VelocityContext velocityContext = new VelocityContext();
        String packageName = Global.getPackageName();
		velocityContext.put("tableName", table.getTableName()); // 表名
		velocityContext.put("tableComment", replaceKeyword(table.getTableComment())); // 表备注
		velocityContext.put("primaryKey", table.getPrimaryKey()); // 主键
		velocityContext.put("className", table.getClassName());// 如Gen
		velocityContext.put("classname", table.getClassname());// 如gen
		velocityContext.put("controllerName", table.getClassName() + "Controller"); // 如GenController
		velocityContext.put("IServiceName", "I" + table.getClassName() + "Service"); // 如IGenService
		velocityContext.put("servicename", table.getClassname() + "Service"); // 如genService
		velocityContext.put("serviceName", table.getClassName() + "ServiceImpl"); // 如GenServiceImpl
        velocityContext.put("columns", table.getColumns());
        velocityContext.put("moduleName", getModuleName(packageName)); //模块名 com.fans.admin.test  test 为模块名
        velocityContext.put("basePackage", getBasePackage(packageName)); //基础路径 com.fans.admin.test   com.fans.admin 为基础路径 
		velocityContext.put("package", packageName); // 基础路径 com.fans.admin.test
		velocityContext.put("author", Global.getAuthor()); // 作者
		velocityContext.put("datetime", DateUtils.getDate()); // 创建时间
		velocityContext.put("version", Global.getVersion()); // 创建版本
		velocityContext.put("copyright", Global.getCopyright()); // 版权
		velocityContext.put("enableCache", Global.getEnableCache()); // 是否开启缓存
		velocityContext.put("apiPrefix", "v1"); // 统一接口前缀（访问 /v1/../..）
		velocityContext.put("superEntityClass", "BaseEntity");//entity 父包
		velocityContext.put("superEntityClassPackage", "com.fans.common.base.BaseEntity");//entity 父包 路径
		velocityContext.put("superMapperClass", "BaseMapper");//mapper 父包
		velocityContext.put("superMapperClassPackage", "com.baomidou.mybatisplus.mapper.BaseMapper");//mapper 父包 路径
		velocityContext.put("superServiceClass", "IService");//service 父包
		velocityContext.put("superServiceClassPackage", "com.baomidou.mybatisplus.service.IService");//service 父包 路径
		velocityContext.put("superServiceImplClass", "ServiceImpl");//serviceImpl 父包
		velocityContext.put("superServiceImplClassPackage", "com.baomidou.mybatisplus.service.impl.ServiceImpl");//serviceImpl 父包 路径
		velocityContext.put("superControllerClass", "BaseController");//controller 父包
		velocityContext.put("superControllerClassPackage", "com.fans.common.base.BaseController");//controller 父包 路径
		// entity引入包
		List<String> importPackages = new ArrayList<>();
		importPackages.add("");
		velocityContext.put("importPackages", importPackages);
		// uuid
		Map<String, Object> uuidMap = new HashMap<>();
		uuidMap.put("uuid1", UUID.randomUUID().toString().replaceAll("-", ""));
		uuidMap.put("uuid2", UUID.randomUUID().toString().replaceAll("-", ""));
		uuidMap.put("uuid3", UUID.randomUUID().toString().replaceAll("-", ""));
		uuidMap.put("uuid4", UUID.randomUUID().toString().replaceAll("-", ""));
		uuidMap.put("uuid5", UUID.randomUUID().toString().replaceAll("-", ""));
		velocityContext.put("uuid", uuidMap);

        return velocityContext;
    }

    /**
     * 获取模板信息
     * 
     * @return 模板列表
     */
    public static List<String> getTemplates()
    {
        List<String> templates = new ArrayList<String>();
		templates.add("templates/controller.java.vm");
		templates.add("templates/entity.java.vm");
		templates.add("templates/entityWarpper.java.vm");
		templates.add("templates/mapper.java.vm");
		templates.add("templates/mapper.xml.vm");
		templates.add("templates/service.java.vm");
		templates.add("templates/serviceImpl.java.vm");
		templates.add("templates/vue.vm");
		templates.add("templates/common.vm");
        return templates;
    }

    /**
     * 表名转换成Java类名
     */
    public static String tableToJava(String tableName)
    {
		if (SysConstant.AUTO_REOMVE_PRE.equals(Global.getAutoRemovePre()))
        {
            tableName = tableName.substring(tableName.indexOf("_") + 1);
        }
        if (StringUtils.isNotEmpty(Global.getTablePrefix()))
        {
            tableName = tableName.replace(Global.getTablePrefix(), "");
        }
        return StringUtils.convertToCamelCase(tableName);
    }

    /**
     * 获取文件名
     */
    public static String getFileName(String template, TableInfo table, String moduleName)
    {
        // 小写类名
        String classname = table.getClassname();
        // 大写类名
        String className = table.getClassName();
        String javaPath = PROJECT_PATH;
		String htmlPath = TEMPLATES_PATH + "/" + moduleName;

		if (template.contains("controller.java.vm")) {
			return javaPath + "controller" + "/" + className + "Controller.java";
		}

		if (template.contains("entity.java.vm")) {
			return javaPath + "entity" + "/" + className + ".java";
		}

		if (template.contains("entityWarpper.java.vm"))
        {
			return javaPath + "warpper" + "/" + className + "Warpper.java";
        }

		if (template.contains("mapper.java.vm"))
        {
			return javaPath + "dao" + "/" + className + "Mapper.java";
        }

		if (template.contains("mapper.xml.vm")) {
			return javaPath + "dao" + "/" + "mapper" + "/" + className + "Mapper.xml";
		}

		if (template.contains("service.java.vm"))
        {
            return javaPath + "service" + "/" + "I" + className + "Service.java";
        }

		if (template.contains("serviceImpl.java.vm"))
        {
            return javaPath + "service" + "/impl/" + className + "ServiceImpl.java";
        }

		if (template.contains("vue.vm"))
        {
			return htmlPath + "/" + classname + ".vue";
        }

		if (template.contains("common.vm")) {
			return classname + "Common.txt";
		}
        return null;
    }

    /**
     * 获取模块名
     * 
     * @param packageName 包名
     * @return 模块名
     */
    public static String getModuleName(String packageName)
    {
        int lastIndex = packageName.lastIndexOf(".");
        int nameLength = packageName.length();
        String moduleName = StringUtils.substring(packageName, lastIndex + 1, nameLength);
        return moduleName;
    }

    public static String getBasePackage(String packageName)
    {
        int lastIndex = packageName.lastIndexOf(".");
        String basePackage = StringUtils.substring(packageName, 0, lastIndex);
        return basePackage;
    }

    public static String getProjectPath()
    {
        String packageName = Global.getPackageName();
        StringBuffer projectPath = new StringBuffer();
        projectPath.append("main/java/");
        projectPath.append(packageName.replace(".", "/"));
        projectPath.append("/");
        return projectPath.toString();
    }

    public static String replaceKeyword(String keyword)
    {
        String keyName = keyword.replaceAll("(?:表|信息)", "");
        return keyName;
    }

    static
    {
        javaTypeMap.put("tinyint", "Integer");
        javaTypeMap.put("smallint", "Integer");
        javaTypeMap.put("mediumint", "Integer");
        javaTypeMap.put("int", "Integer");
        javaTypeMap.put("integer", "integer");
        javaTypeMap.put("bigint", "Long");
        javaTypeMap.put("float", "Float");
        javaTypeMap.put("double", "Double");
        javaTypeMap.put("decimal", "BigDecimal");
        javaTypeMap.put("bit", "Boolean");
        javaTypeMap.put("char", "String");
        javaTypeMap.put("varchar", "String");
        javaTypeMap.put("tinytext", "String");
        javaTypeMap.put("text", "String");
        javaTypeMap.put("mediumtext", "String");
        javaTypeMap.put("longtext", "String");
        javaTypeMap.put("time", "Date");
        javaTypeMap.put("date", "Date");
        javaTypeMap.put("datetime", "Date");
        javaTypeMap.put("timestamp", "Date");
    }
}

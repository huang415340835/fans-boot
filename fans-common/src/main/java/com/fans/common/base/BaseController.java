package com.fans.common.base;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fans.utils.utils.WriterUtil;

/**
 * @ClassName: BaseController
 * @Description: 基础控制器
 * @author fanhaohao
 * @date 2018年12月13日 下午2:41:10
 */
public abstract class BaseController{
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * 
	 * @Title：renderString  
	 * @Description: 客户端返回JSON字符串
	 * @author: fanhaohao
	 * @date 2018年12月13日 下午2:39:10 
	 * @param @param response
	 * @param @param object
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	protected String renderString(HttpServletResponse response, Object object) {
		return WriterUtil.renderString(response, object);
	}
	
	/**
	 * 
	 * @Title：renderStringDealNull  
	 * @Description: 空值时也返回属性
	 * @author: fanhaohao
	 * @date 2018年12月13日 下午2:39:24 
	 * @param @param response
	 * @param @param object
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	protected String renderStringDealNull(HttpServletResponse response, Object object) {
		return WriterUtil.renderStringDealNull(response, object);
    }
	
	
}

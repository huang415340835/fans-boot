package com.fans.common.vo;

import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;

@SuppressWarnings("serial")
@TableName(value = "sys_log")
public class LogVo extends BaseEntity<LogVo> {
	/**
	 * 用户操作
	 */
	private String operation;
	/**
	 * 日志内容
	 */
	private String content;
	/**
	 * 操作IP
	 */
	private String ip;
	/**
	 * 操作的方式
	 */
	private String method;
	/**
	 * 日志类型（1：接入日志；2：错误日志）
	 */
	private String type;

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}

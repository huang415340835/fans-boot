
package com.fans.common.config.properties;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName: IgnoreUrlsProperties
 * @Description: Security可以进行忽略的地址
 * @author fanhaohao
 * @date 2018年12月7日 下午5:21:30
 */
@Configuration
@ConditionalOnExpression("!'${ignore}'.isEmpty()")
@ConfigurationProperties(prefix = "ignore")
public class IgnoreUrlsProperties {
	private List<String> httpUrls = new ArrayList<>();

	private List<String> webUrls = new ArrayList<>();

	public List<String> getHttpUrls() {
		return httpUrls;
	}

	public void setHttpUrls(List<String> httpUrls) {
		this.httpUrls = httpUrls;
	}

	public List<String> getWebUrls() {
		return webUrls;
	}

	public void setWebUrls(List<String> webUrls) {
		this.webUrls = webUrls;
	}

}

package com.fans.common.config.db.dynamic.config.util;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @ClassName: DynamicDataSource
 * @Description: 动态数据源
 * @author fanhaohao
 * @date 2018年12月5日 下午3:15:51
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

	private Map<Object, Object> datasources;

	public DynamicDataSource() {
		datasources = new HashMap<>();

		super.setTargetDataSources(datasources);

	}

	public <T extends DataSource> void addDataSource(DataSourceKey key, T data) {
		datasources.put(key, data);
	}

	@Override
	protected Object determineCurrentLookupKey() {
		return DataSourceHolder.getDataSourceKey();
	}

}

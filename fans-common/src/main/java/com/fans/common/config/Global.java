package com.fans.common.config;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.StringUtils;
import com.fans.utils.utils.YamlUtil;

/**
 * 全局配置类
 * 
 * @author fanhaohao
 */
public class Global
{
    private static final Logger log = LoggerFactory.getLogger(Global.class);

    private static String NAME = "application.yml";

    /**
     * 当前对象实例
     */
    private static Global global = null;

    /**
     * 保存全局属性值
     */
    private static Map<String, String> map = new HashMap<String, String>();

    private Global()
    {
    }

    /**
     * 静态工厂方法 获取当前对象实例 多线程安全单例模式(使用双重同步锁)
     */

    public static synchronized Global getInstance()
    {
        if (global == null)
        {
            synchronized (Global.class)
            {
                if (global == null) {
					global = new Global();
				}
            }
        }
        return global;
    }

    /**
     * 获取配置
     */
    public static String getConfig(String key)
    {
        String value = map.get(key);
        if (value == null)
        {
            Map<?, ?> yamlMap = null;
            try
            {
                yamlMap = YamlUtil.loadYaml(NAME);
                value = String.valueOf(YamlUtil.getProperty(yamlMap, key));
                map.put(key, value != null ? value : StringUtils.EMPTY);
            }
            catch (FileNotFoundException e)
            {
                log.error("获取全局配置异常 {}", key);
            }
        }
        return value;
    }

    /**
     * 获取作者
     */
    public static String getAuthor()
    {
		return StringUtils.nvl(getConfig("gen.author"), "fanhaohao");
    }

    /**
     * 生成包路径
     */
    public static String getPackageName()
    {
		return StringUtils.nvl(getConfig("gen.packageName"), "com.fans");
    }

    /**
     * 是否自动去除表前缀
     */
    public static String getAutoRemovePre()
    {
        return StringUtils.nvl(getConfig("gen.autoRemovePre"), "true");
    }

    /**
     * 表前缀(类名不会包含表前缀)
     */
    public static String getTablePrefix()
    {
        return StringUtils.nvl(getConfig("gen.tablePrefix"), "sys_");
    }

	/**
	 * 版本
	 */
	public static Object getVersion() {
		return StringUtils.nvl(getConfig("gen.version"), "1.0");
	}

	/**
	 * 版权
	 */
	public static Object getCopyright() {
		return StringUtils.nvl(getConfig("gen.copyright"), "版权所有 fans (c)2019 ");
	}

	/**
	 * 是否开启缓存
	 */
	public static Boolean getEnableCache() {
		String enableCache = getConfig("gen.enableCache");
		if (BlankUtils.isBlank(enableCache) || "false".equals(enableCache)) {
			return false;
		} else {
			return true;
		}
	}

}

package com.fans.common.config.db.dynamic.config.util;

/**
 * @ClassName: DataSourceHolder
 * @Description: 用于数据源切换
 * @author fanhaohao
 * @date 2018年12月5日 下午3:15:19
 */
public class DataSourceHolder {
	private static final ThreadLocal<DataSourceKey> dataSourceKey = new ThreadLocal<>();

	public static DataSourceKey getDataSourceKey() {
		return dataSourceKey.get();
	}

	public static void setDataSourceKey(DataSourceKey type) {
		dataSourceKey.set(type);
	}

	public static void clearDataSourceKey() {
		dataSourceKey.remove();
	}

}
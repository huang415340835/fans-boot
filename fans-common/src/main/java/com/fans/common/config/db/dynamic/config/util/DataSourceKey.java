package com.fans.common.config.db.dynamic.config.util;

/**
 * @ClassName: DataSourceKey
 * @Description: 数据源定义
 * @author fanhaohao
 * @date 2018年12月5日 下午3:15:34
 */
public enum DataSourceKey {
	crm, bill
}
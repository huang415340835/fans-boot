package com.fans.common.config.captcha;


import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.fans.common.config.redis.util.JedisUtils;
import com.fans.common.constant.SysConstant;
import com.fans.utils.utils.CaptchaUtils;
import com.fans.utils.utils.HttpServletUtils;
import com.fans.utils.utils.Md5Utils;

/**
 * @ClassName: CaptchaService
 * @Description: 验证码Service实现
 * @author fanhaohao
 * @date 2018年12月14日 上午10:59:48
 */
@Service
public class CaptchaService {
	
    public void buildCaptcha() {
        buildCaptcha(HttpServletUtils.getResponse());
    }

    public void buildCaptcha(HttpServletResponse response) {
        CaptchaUtils.Captcha captcha = CaptchaUtils.createCaptcha(120, 30);
        String captchaId = Md5Utils.hash(Md5Utils.getUUID());
        response.setContentType("image/jpeg");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        Cookie cookie = new Cookie("captchaId", captchaId);
        cookie.setMaxAge(1800);
        response.addCookie(cookie);
		JedisUtils.setObject(SysConstant.CAPTCHA_KEY + captchaId, captcha.getCode(), 300);
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            ImageIO.write(captcha.getImage(), "jpg", outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public boolean valid(String code) {
        return valid(HttpServletUtils.getRequest(), code);
    }


    public boolean valid(HttpServletRequest request, String code) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                Cookie cookie = cookies[i];
                if (cookie.getName().equals("captchaId")) {
                    String key = SysConstant.CAPTCHA_KEY + cookie.getValue();
					String captcha = (String) JedisUtils.getObject(key);
					JedisUtils.deleteKeys(SysConstant.CAPTCHA_KEY + cookie.getValue());
                    if (StringUtils.isNotEmpty(captcha) && StringUtils.isNotEmpty(code) &&
                            code.toLowerCase().equals(captcha.toLowerCase())) {
                        return true;
                    }

                }
            }
        }
        return false;
    }
}
package com.fans.common.constant;

/**
 * @ClassName: SysConstant
 * @Description: 系统常量
 * @author fanhaohao
 * @date 2018年12月5日 下午3:19:02
 */
public class SysConstant {
	public static final String AUTH_KEY = "Authorization";
	public static final String AUTH_PRE_B = "Bearer ";
	public static final String AUTH_PRE_S = "bearer ";
	public static final String SQL_FILTER = "sql_filter"; // 数据权限过滤
	public final static String CAPTCHA_KEY = "CAPTCHA:";// 保存到Redis中的验证码key
	public final static String NAME2ID_KEY = "sys:user:name2id:";//保存到Redis中的系统用户name对于的id key
	public final static String ID2NAME_KEY = "sys:user:id2name:";//保存到Redis中的系统用户id对应的mame key
	public final static String ID2USER_KEY = "sys:user:id2user:";//保存到Redis中的系统用户id对应的用户信息 key
	public final static String BASE_ROLE = "ROLE_USER";// 基础角色
	public final static String INIT_PWD = "123456";// 初始密码
	public final static String PERMS_MENU_DATA = "sys:perms:menu:data:";// 按钮权限数据
	public static final String LOG_TYPE_NORMAL = "1";// 日志类型（1：正常日志；2：异常日志）
	public static final String LOG_TYPE_EXCEPTION = "2";// 日志类型（1：正常日志；2：异常日志）
	/**
	 * UTF-8 字符集
	 */
	public static final String UTF8 = "UTF-8";

	/**
	 * 自动去除表前缀
	 */
	public static String AUTO_REOMVE_PRE = "true";
}

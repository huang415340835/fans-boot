package com.fans.common.constant;

/**
 * @ClassName: Order
 * @Description: 数据库排序
 * @author fanhaohao
 * @date 2018年12月19日 下午5:42:19
 */
public enum Order {

    ASC("asc"), DESC("desc");

    private String des;

    Order(String des) {
        this.des = des;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}

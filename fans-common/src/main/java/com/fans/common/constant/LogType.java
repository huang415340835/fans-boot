package com.fans.common.constant;

/**
 * @ClassName: LogType
 * @Description: 日志级别枚举类
 * @author fanhaohao
 * @date 2018年12月29日 下午3:34:47
 */
public enum LogType {
	INFO, WARN, ERROR
}

package com.fans.common.utils;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.common.constant.Order;
import com.fans.common.vo.PageVo;
import com.fans.utils.utils.BlankUtils;

/**
 * @ClassName: PageFactory
 * @Description: Table默认的分页参数创建
 * @author fanhaohao
 * @date 2018年12月20日 上午9:23:50
 * @param <T>
 */
public class PageFactory<T> {
	public Page<T> page(PageVo pv) {
		int size = BlankUtils.isBlank(pv.getPageSize()) ? 10000 : Integer.valueOf(pv.getPageSize());
		int current = BlankUtils.isBlank(pv.getPage()) ? 0 : Integer.valueOf(pv.getPage());
		String sort = pv.getSortField();
		String order = pv.getSortOrder();
		if (BlankUtils.isBlank(sort)) {
			Page<T> page = new Page<>(current, size);
			page.setOpenSort(false);
			return page;
		} else {
			Page<T> page = new Page<>(current, size, sort);
			if (Order.ASC.getDes().equals(order)) {
				page.setAsc(true);
			} else {
				page.setAsc(false);
			}
			return page;
		}
	}
}

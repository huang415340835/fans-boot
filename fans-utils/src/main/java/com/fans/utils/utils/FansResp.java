package com.fans.utils.utils;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fans.utils.constant.FansRspCon;

/**
 * @ClassName: FansResp
 * @Description: 返回数据
 * @author fanhaohao
 * @date 2018年12月12日 下午3:23:25
 */
public class FansResp extends LinkedHashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	
	public FansResp() {
		put("code", FansRspCon.SUCCESS.getCode());
		put("msg", FansRspCon.SUCCESS.getMsg());
	}
	
	public static FansResp error() {
		return error(FansRspCon.ERROR.getCode(), FansRspCon.ERROR.getMsg());
	}
	
	public static FansResp error(String msg) {
		return error(FansRspCon.ERROR.getCode(), msg);
	}
	
	public static FansResp error(int code, String msg) {
		FansResp r = new FansResp();
		r.put("code", code);
		r.put("msg", msg);
		return r;
	}
	public static FansResp successData(Object datas) {
		FansResp r = new FansResp();
		r.put("datas",datas);
		return r;
	}
	public static FansResp success(String msg) {
		FansResp r = new FansResp();
		r.put("msg", msg);
		return r;
	}
	public static FansResp successData(String msg,Object datas) {
		FansResp r = new FansResp();
		r.put("msg", msg);
		r.put("datas",datas);
		return r;
	}
	
	public static FansResp info(int code,String msg) {
		FansResp r = new FansResp();
		r.put("code", code);
		r.put("msg", msg);
		return r;
	}
	
	public static FansResp info(int code,String msg,Object datas) {
		FansResp r = new FansResp();
		r.put("msg", msg);
		r.put("datas",datas);
		return r;
	}
	
	public static FansResp successData(String code,String msg,Object datas) {
		FansResp r = new FansResp();
		r.put("code", code);
		r.put("msg", msg);
		r.put("datas",datas);
		return r;
	}
	
	public static FansResp success(Map<String, Object> map) {
		FansResp r = new FansResp();
		r.putAll(map);
		return r;
	}
	
	
	
	public static FansResp success() {
		return new FansResp();
	}

	@Override
	public FansResp put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}

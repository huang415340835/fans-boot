package com.fans.utils.constant;

/**
 * @ClassName: BrowserType
 * @Description: 浏览器类型枚举类
 * @author fanhaohao
 * @date 2019年1月11日 上午10:14:57
 */
public enum BrowserType {
	IE11,IE10,IE9,IE8,IE7,IE6,Firefox,Safari,Chrome,Opera,Camino,Gecko
}

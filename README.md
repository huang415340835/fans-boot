# fans-boot

#### 项目介绍
fans-boot 是由 springboot，oauth2，mybatis ，quartz，redis 等技术组成的后端框架（前后分离架构）

#### 前端代码地址
https://gitee.com/fhh/fansbfront

#### springcloud版本代码地址
前端：https://gitee.com/fhh/fans-cloud-front 后端：https://gitee.com/fhh/fans-cloud-alibaba

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2019/0910/142158_61677a08_79358.png "fans-boot.png")

#### 功能模块
![输入图片说明](https://images.gitee.com/uploads/images/2019/0910/144641_258df0b9_79358.png "fansboot.png")
#### 运行方式
1，运行fans-admin模块下的AdminApp启动类（接口为8080）
2，前端启动（参考https://gitee.com/fhh/fansbfront启动方式）
3，登录用户名/密码：admin/123456

#### 安装教程

1. 利用Git下载到自己的pc上
2. 导入到eclipse或idea中
3. 找到总模块fans-boot下db，进入db找到fans_boot.sql的数据库文件，在mysql中执行该文件
4. 编译好后运行fans-admin模块下的AdminApp启动类
5. 前端运行（https://gitee.com/fhh/fansbfront 下载代码根据文档描述运行）

#### 代码配置描述

![输入图片说明](https://images.gitee.com/uploads/images/2019/0910/142851_7e4b58c4_79358.png "代码配置描述.png")

#### 运行效果

![登录](https://images.gitee.com/uploads/images/2019/0910/112019_47145f00_79358.png "登录")
![首页](https://images.gitee.com/uploads/images/2019/0910/111321_b4880644_79358.png "首页")
![用户管理](https://images.gitee.com/uploads/images/2019/0910/111604_e3f1cf85_79358.png "用户管理")
![菜单配置](https://images.gitee.com/uploads/images/2019/0910/111628_69d0e0f3_79358.png "菜单配置")
![角色管理](https://images.gitee.com/uploads/images/2019/0910/112326_472f02ab_79358.png "角色管理")
![字典管理](https://images.gitee.com/uploads/images/2019/0910/112338_9de7207b_79358.png "字典管理")
![oauth2客户端管理](https://images.gitee.com/uploads/images/2019/0910/113052_1513106d_79358.png "oauth2客户端管理")
![oauth2客户端编辑](https://images.gitee.com/uploads/images/2019/0910/113117_69dbb0f0_79358.png "oauth2客户端编辑.png")
![认证用户](https://images.gitee.com/uploads/images/2019/0910/113143_e1a1e1b6_79358.png "认证用户.png")
![swagger](https://images.gitee.com/uploads/images/2019/0910/113221_c6873d3b_79358.png "swagger.png")
![定时任务管理](https://images.gitee.com/uploads/images/2019/0910/113936_e178e214_79358.png "定时任务管理.png")
![定时任务表达式编辑](https://images.gitee.com/uploads/images/2019/0910/113954_8b352b43_79358.png "定时任务表达式编辑.png")
![代码生成器](https://images.gitee.com/uploads/images/2019/0910/114011_41fb2b23_79358.png "代码生成器.png")
![开发组件](https://images.gitee.com/uploads/images/2019/0910/114024_9b6bbdb2_79358.png "开发组件.png")

#### 参考网址
可以参考iview-admin：https://lison16.github.io/iview-admin-doc/#/
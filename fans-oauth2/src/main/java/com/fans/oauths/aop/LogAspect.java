package com.fans.oauths.aop;

import java.lang.reflect.Method;
import java.net.SocketException;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fans.common.constant.SysConstant;
import com.fans.common.vo.LogVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.oauths.thread.SaveLogThread;
import com.fans.utils.utils.StringUtils;

/**
 * @ClassName: LogAspect
 * @Description: 日志Aop
 * @author fanhaohao
 * @date 2018年12月29日 下午2:36:22
 */
@Component
@Aspect
public class LogAspect {
	private static final Logger log = LoggerFactory.getLogger(LogAspect.class);
	
	/**
     * 切入点
     */
	@Pointcut("@annotation(com.fans.oauths.annotations.Log) ")
    public void entryPoint() {
        // 无需内容
    }
    
    
	/**
	 * 环绕通知处理处理
	 * 
	 * @param joinPoint
	 * @throws Throwable
	 */
	@Around("entryPoint()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		// 先执行业务,注意:业务这样写业务发生异常不会拦截日志。
		Object result = point.proceed();
		try {
			handleAround(point);// 处理日志
		} catch (Exception e) {
			log.error("日志记录异常", e);
		}
		return result;
	}

	/**
	 * around日志记录
	 * 
	 * @param point
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 */
	public void handleAround(ProceedingJoinPoint point) throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		LogVo lv = new LogVo();
		lv.setCreateBy(UserUtil.getUserId());
		lv.setType(SysConstant.LOG_TYPE_NORMAL);// 正常日志
		// 获取IP地址
		String ip = StringUtils.getRemoteAddr(request);
		if ("0:0:0:0:0:0:0:1".equals(ip)) {
			try {
				ip = StringUtils.getRealIp();
			} catch (SocketException e) {
				e.printStackTrace();
			}
		}
		lv.setIp(ip);
		lv.setMethod(request.getMethod());

		Signature sig = point.getSignature();
		MethodSignature msig = null;
		if (!(sig instanceof MethodSignature)) {
			throw new IllegalArgumentException("该注解只能用于方法");
		}
		msig = (MethodSignature) sig;
		Object target = point.getTarget();
		Method currentMethod = target.getClass().getMethod(msig.getName(), msig.getParameterTypes());
		// 方法名称
		String methodName = currentMethod.getName();
		// 获取注解对象
		Log aLog = currentMethod.getAnnotation(Log.class);
		// 类名
		String className = point.getTarget().getClass().getName();
		// 方法的参数
		Object[] params = point.getArgs();

		StringBuilder paramsBuf = new StringBuilder();
		for (Object arg : params) {
			paramsBuf.append(arg);
			paramsBuf.append("&");
		}
		String content = "类:" + className + ",方法名：" + methodName + ",参数:" + paramsBuf.toString();
		// 日志保存
		lv.setContent(content);
		lv.setOperation(aLog.operation());
		new SaveLogThread(lv, request).start();
	}

	@AfterThrowing(pointcut = "entryPoint()", throwing = "ex")
	public void doAfterThrowing(JoinPoint joinPoint, Throwable ex) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		LogVo lv = new LogVo();
		lv.setCreateBy(UserUtil.getUserId());
		lv.setType(SysConstant.LOG_TYPE_EXCEPTION);// 异常日志
		// 获取IP地址
		String ip = StringUtils.getRemoteAddr(request);
		if ("0:0:0:0:0:0:0:1".equals(ip)) {
			try {
				ip = StringUtils.getRealIp();
			} catch (SocketException e) {
				e.printStackTrace();
			}
		}
		lv.setIp(ip);
		lv.setMethod(request.getMethod());

		try {
			String targetName = joinPoint.getTarget().getClass().getName();
			String className = joinPoint.getTarget().getClass().getName();
			String methodName = joinPoint.getSignature().getName();
			Object[] arguments = joinPoint.getArgs();
			Class<?> targetClass = Class.forName(targetName);
			Method[] methods = targetClass.getMethods();
			String operation = "";
			for (Method method : methods) {
				if (method.getName().equals(methodName)) {
					Class<?>[] clazzs = method.getParameterTypes();
					if (clazzs.length == arguments.length) {
						operation = method.getAnnotation(Log.class).operation();
						break;
					}
				}
			}

			StringBuilder paramsBuf = new StringBuilder();
			for (Object arg : arguments) {
				paramsBuf.append(arg);
				paramsBuf.append("&");
			}
			String content = "异常方法:" + className + "." + methodName + "();参数:" + paramsBuf.toString() + ",异常信息:" + ex;

			log.error("异常信息:{}", ex.getMessage());
			//日志保存
			lv.setContent(content);
			lv.setOperation(operation);
			new SaveLogThread(lv, request).start();
			
		} catch (Exception e) {
			log.error("异常信息:{}", e.getMessage());
		}
	}

    

}
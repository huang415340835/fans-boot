package com.fans.oauths.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fans.oauths.interceptor.RequiredPermissionInterceptor;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {
	@Bean
	public RequiredPermissionInterceptor requiredPermissionInterceptor() {
		return new RequiredPermissionInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(requiredPermissionInterceptor()).addPathPatterns("/**");
	}

}

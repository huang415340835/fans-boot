package com.fans.oauths.config;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fans.utils.utils.BlankUtils;

import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * @ClassName: FeignConfig
 * @Description: TODO
 * @author fanhaohao
 * @date 2018年12月5日 下午2:53:22
 */
@Configuration
public class FeignConfig implements RequestInterceptor {

	private static final String TOKEN_KEY = "access_token";

	@Override
	public void apply(RequestTemplate requestTemplate) {
		requestTemplate.header("access_token", getToken(getHttpServletRequest()));
	}

	/**
	 * @Title：getHttpServletRequest
	 * @Description: 获取HttpServletRequest
	 * @author: fanhaohao
	 * @date 2018年12月5日 下午2:52:08
	 * @param @return
	 * @return HttpServletRequest
	 * @throws
	 */
	private HttpServletRequest getHttpServletRequest() {
		try {
			return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @Title：getToken
	 * @Description: 根据参数或者header获取token
	 * @author: fanhaohao
	 * @date 2018年12月5日 下午2:51:50
	 * @param @param request
	 * @param @return
	 * @return String
	 * @throws
	 */
	public static String getToken(HttpServletRequest request) {
		String token = request.getParameter(TOKEN_KEY);
		if (BlankUtils.isBlank(token)) {
			token = request.getHeader(TOKEN_KEY);
		}
		return token;
	}

}

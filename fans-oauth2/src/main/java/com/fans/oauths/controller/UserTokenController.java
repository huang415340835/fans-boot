package com.fans.oauths.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fans.oauths.oauth2.redis.service.RedisClientDetailsService;
import com.fans.oauths.oauth2.util.TokenUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.WriterUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @ClassName: UserTokenController
 * @Description: 用户token接口
 * @author fanhaohao
 * @date 2018年12月5日 下午3:03:18
 */
@Api(tags = "Token处理")
@RestController
public class UserTokenController {

	private static final Logger logger = LoggerFactory.getLogger(UserTokenController.class);
	// redis clientdetial工具类
	@Autowired(required = false)
	private RedisClientDetailsService redisClientDetailsService;

	// jdbc clientdetial工具类
	@Autowired(required = false)
	private JdbcClientDetailsService jdbcClientDetailsService;
																
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private AuthorizationServerTokenServices authorizationServerTokenServices;

	@Autowired
	private TokenStore tokenStore;

	/**
	 * @Title：logout
	 * @Description: 删除token
	 * @author: fanhaohao
	 * @date 2018年12月7日 上午11:49:14
	 * @param @param request
	 * @return void
	 * @throws
	 */
	@RequestMapping(value = "/oauth/revokeToken", method = RequestMethod.GET)
	public void logout(HttpServletRequest request, HttpServletResponse response) {
		String rtStr = "falure";
		String tokenValue = TokenUtil.getToken();
		if (BlankUtils.isNotBlank(tokenValue)) {
			OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
			if (BlankUtils.isNotBlank(accessToken)) {
				tokenStore.removeAccessToken(accessToken);
				rtStr = "ok";
			}
		}
		WriterUtil.renderString(response, rtStr);
	}

	@ApiOperation(value = "用户名密码获取token")
	@PostMapping("/user/token")
	public void onAuthenticationSuccess(
			@ApiParam(required = true, name = "username", value = "账号") @RequestParam(value = "username") String username,
			@ApiParam(required = true, name = "password", value = "密码") @RequestParam(value = "password") String password,
			HttpServletRequest request, HttpServletResponse response) {
		String clientId = request.getHeader("client_id");
		String clientSecret = request.getHeader("client_secret");

		try {

			if (BlankUtils.isBlank(clientId)) {
				throw new UnapprovedClientAuthenticationException("请求头中无client_id信息");
			}

			if (BlankUtils.isBlank(clientSecret)) {
				throw new UnapprovedClientAuthenticationException("请求头中无client_secret信息");
			}
			// 从redis或者jdbc中获取ClientDetails信息
			ClientDetails clientDetails = getClientDetailsService().loadClientByClientId(clientId);

			if (BlankUtils.isBlank(clientDetails)) {
				throw new UnapprovedClientAuthenticationException("clientId对应的信息不存在");
			} else if (!StringUtils.equals(clientDetails.getClientSecret(), clientSecret)) {
				throw new UnapprovedClientAuthenticationException("clientSecret不匹配");
			}

			TokenRequest tokenRequest = new TokenRequest(MapUtils.EMPTY_MAP, clientId, clientDetails.getScope(),
					"customer");

			OAuth2Request oAuth2Request = tokenRequest.createOAuth2Request(clientDetails);

			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);

			Authentication authentication = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);

			OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(oAuth2Request, authentication);

			OAuth2AccessToken oAuth2AccessToken = authorizationServerTokenServices
					.createAccessToken(oAuth2Authentication);

			oAuth2Authentication.setAuthenticated(true);

			WriterUtil.renderString(response, oAuth2AccessToken);

		} catch (Exception e) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			Map<String, String> rsp = new HashMap<>();
			rsp.put("resp_code", HttpStatus.UNAUTHORIZED.value() + "");
			rsp.put("rsp_msg", e.getMessage());
			WriterUtil.renderString(response, rsp);
		}
	}

	@ApiOperation(value = "clientId获取token")
	@PostMapping("/client/token")
	public void client(
			@ApiParam(required = true, name = "clientId", value = "应用ID") @RequestParam(value = "clientId") String clientId,
			@ApiParam(required = true, name = "clientSecret", value = "应用密钥") @RequestParam(value = "clientSecret") String clientSecret,
			HttpServletRequest request, HttpServletResponse response) {
		try {

			if (clientId == null || "".equals(clientId)) {
				throw new UnapprovedClientAuthenticationException("请求参数中无clientId信息");
			}

			if (clientSecret == null || "".equals(clientSecret)) {
				throw new UnapprovedClientAuthenticationException("请求参数中无clientSecret信息");
			}

			// 从redis或者jdbc中获取ClientDetails信息
			ClientDetails clientDetails = getClientDetailsService().loadClientByClientId(clientId);

			if (clientDetails == null) {
				throw new UnapprovedClientAuthenticationException("clientId对应的信息不存在");
			} else if (!StringUtils.equals(clientDetails.getClientSecret(), clientSecret)) {
				throw new UnapprovedClientAuthenticationException("clientSecret不匹配");
			}

			Map<String, String> map = new HashMap<>();
			map.put("client_secret", clientSecret);
			map.put("client_id", clientId);
			map.put("grant_type", "client_credentials");
			TokenRequest tokenRequest = new TokenRequest(map, clientId, clientDetails.getScope(), "client_credentials");

			OAuth2Request oAuth2Request = tokenRequest.createOAuth2Request(clientDetails);

			OAuth2RequestFactory requestFactory = new DefaultOAuth2RequestFactory(getClientDetailsService());
			ClientCredentialsTokenGranter clientCredentialsTokenGranter = new ClientCredentialsTokenGranter(
					authorizationServerTokenServices, getClientDetailsService(), requestFactory);

			clientCredentialsTokenGranter.setAllowRefresh(true);
			OAuth2AccessToken oAuth2AccessToken = clientCredentialsTokenGranter.grant("client_credentials",
					tokenRequest);
			WriterUtil.renderString(response, oAuth2AccessToken);
		} catch (Exception e) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			Map<String, String> rsp = new HashMap<>();
			rsp.put("resp_code", HttpStatus.UNAUTHORIZED.value() + "");
			rsp.put("rsp_msg", e.getMessage());
			WriterUtil.renderString(response, rsp);
		}
	}

	/**
	 * @Title：getCurrentUserDetail
	 * @Description: 获取用户信息
	 * @author: fanhaohao
	 * @date 2018年12月6日 下午4:47:03
	 * @param @return
	 * @return Map<String,Object>
	 * @throws
	 */
	@RequestMapping(value = { "/users" }, produces = "application/json") // 获取用户信息。/auth/user
	public Map<String, Object> getCurrentUserDetail() {
		Map<String, Object> userInfo = new HashMap<>();
		userInfo.put("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		logger.debug("认证详细信息:" + SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
		userInfo.put("authorities", AuthorityUtils
				.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
		return userInfo;
	}

	private ClientDetailsService getClientDetailsService() {
		if (BlankUtils.isNotBlank(redisClientDetailsService)) {
			return redisClientDetailsService;
		} else if (BlankUtils.isNotBlank(jdbcClientDetailsService)) {
			return jdbcClientDetailsService;
		}
		return null;
	}


}

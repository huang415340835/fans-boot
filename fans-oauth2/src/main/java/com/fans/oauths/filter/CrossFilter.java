package com.fans.oauths.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @ClassName: CrossFilter
 * @Description: 跨域过滤器
 * @author fanhaohao
 * @date 2018年12月6日 下午5:30:16
 */
@Order(2)
@Component
public class CrossFilter implements Filter {

	final static Logger logger = LoggerFactory.getLogger(CrossFilter.class);

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET,PUT, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		//response.setHeader("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Authorization,authentication");
		
		response.setHeader("Access-Control-Allow-Headers","DNT,Accept,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,token,Authorization,authorization");

		if ("OPTIONS".equals(request.getMethod())) {
			filterChain.doFilter(servletRequest, servletResponse);
		} else {
			// In case of HTTP OPTIONS method, just return the response
		}

		String requestURI = request.getRequestURI();
		logger.info("-------------------来访的URL：" + requestURI);

		// jwt 模式的时候，获取token，claims以及用户信息
		/*logger.info("获取头信息中的token: " + JwtUtil.getToken());
		logger.info("获取头信息中的claims: " + JwtUtil.getClaims());
		logger.info("获取头信息中的登录用户名：" + JwtUtil.getUserName());*/

		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void init(FilterConfig filterConfig) {
	}

	@Override
	public void destroy() {
	}
}

package com.fans.oauths.util;

import java.net.SocketException;

import javax.servlet.http.HttpServletRequest;

import com.fans.common.constant.SysConstant;
import com.fans.common.vo.LogVo;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.oauths.thread.SaveLogThread;
import com.fans.utils.utils.StringUtils;

/**
 * @ClassName: LogUtils
 * @Description: 日志保存
 * @author fanhaohao
 * @date 2019年1月9日 下午3:40:23
 */
public class LogUtils {
	/**
	 * 保存日志
	 */
	public static void saveLog(HttpServletRequest request, String operation, Exception e) {
		LogVo lv = new LogVo();
		lv.setCreateBy(UserUtil.getUserId());
		lv.setType(SysConstant.LOG_TYPE_EXCEPTION);// 异常日志
		lv.setOperation(operation);
		lv.setContent(e.toString());
		// 获取IP地址
		String ip = StringUtils.getRemoteAddr(request);
		if ("0:0:0:0:0:0:0:1".equals(ip)) {
			try {
				ip = StringUtils.getRealIp();
			} catch (SocketException se) {
				se.printStackTrace();
			}
		}
		lv.setIp(ip);
		lv.setMethod(request.getMethod());
		new SaveLogThread(lv, request).start();
	}

}

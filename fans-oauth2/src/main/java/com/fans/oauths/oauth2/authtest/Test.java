package com.fans.oauths.oauth2.authtest;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fans.common.constant.SysConstant;
import com.fans.utils.utils.HttpClientAuthUtils;

/**
 * @ClassName: HttpOauth2Test
 * @Description: http访问拿认证服务器的token
 * @author fanhaohao
 * @date 2018年12月5日 下午3:04:25
 */
public class Test {

	private static final String USER_URL = "http://127.0.0.1:8080/fansb/admin/users";
	private static final String TOKEN_URL = "http://127.0.0.1:8080/fansb/admin/oauth/token";
	private static final String REVOKE_TOKEN_URL = "http://127.0.0.1:8080/fansb/admin/oauth/revokeToken";
	private static final String SYS_USER_LIST_URL = "http://127.0.0.1:8080/fansb/admin/v1/sys/sysUser/list";
	private static final String ISLOGINED = "http://127.0.0.1:8080/fansb/admin/auth/isLogined";

	private HttpClientBuilder httpClientBuilder;
	private CloseableHttpClient httpClient;

	// private static final Object MAPPER = new ObjectMapper();
	public void start() throws Exception {

		httpClientBuilder = HttpClientBuilder.create();
		// CloseableHttpClient httpClient = HttpClients.createDefault();
		// http POST
		// HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		HttpPost httpPost = new HttpPost(TOKEN_URL);
		// HttpGet httpGet = new HttpGet(url);
		// httpPost.addHeader("Authorization", "Basic cGljYzpzZWNyZXQ=");//
		//Base64Util.encode("app:app"); ---> Basic YXBwOmFwcA==
		CredentialsProvider provider = new BasicCredentialsProvider();

		AuthScope scope = new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_REALM);
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("app", "app"); // 生成方式Base64Util工具类
		provider.setCredentials(scope, credentials);

		List<NameValuePair> formParams = new ArrayList<NameValuePair>();
		formParams.add(new BasicNameValuePair("grant_type", "password"));
		formParams.add(new BasicNameValuePair("scope", "app"));
		formParams.add(new BasicNameValuePair("username", "admin"));
		formParams.add(new BasicNameValuePair("password", "123456"));
		HttpEntity entity = new UrlEncodedFormEntity(formParams, "UTF-8");
		httpPost.setEntity(entity);
		httpClientBuilder.setDefaultCredentialsProvider(provider);
		httpClient = httpClientBuilder.build();
		CloseableHttpResponse response = null;
		try {
			response = httpClient.execute(httpPost);
			String content = EntityUtils.toString(response.getEntity(), "UTF-8");
			JSONObject jsonObject = JSONObject.parseObject(content);
			System.out.println("得到:jsonObject : " + JSON.toJSONString(jsonObject));
			String access_token = jsonObject.getString("access_token");
			System.out.println("得到:access_token : " + access_token);

			// 获取认证后，验证token
			String contentIsLogined = testIsLogined(access_token);
			System.out.println("获取认证后，验证token：" + contentIsLogined);

			//获取认证后接口测试
			String contentTest = testGet(access_token);
			System.out.println("认证后获取user信息：" + contentTest);

			String userListContentTest = testGetUserList(access_token);
			System.out.println("userList信息：" + userListContentTest);

			String revokeToken = revokeToken(access_token);
			System.out.println("删除token：" + revokeToken);

			String contentTest1 = testGet(access_token);
			System.out.println("删除token后获取user信息：" + contentTest1);

			// 获取认证后，验证token
			String contentIsLogined1 = testIsLogined(access_token);
			System.out.println("获取认证后，验证token：" + contentIsLogined1);

		} finally {
			if (response != null) {
				response.close();
			}

			httpClient.close();

		}
	}

	/**
	 * http post 验证是否登录
	 * 
	 * @param json
	 * @return
	 * @throws Exception
	 */
	private String testIsLogined(String authV) throws Exception {
		String content = HttpClientAuthUtils.doPostByAuth(ISLOGINED, null, SysConstant.AUTH_KEY, SysConstant.AUTH_PRE_B,
				authV);
		return content;
	}

	/**
	 * http get 认证请求测试
	 * 
	 * @param json
	 * @return
	 * @throws Exception
	 */
	private String testGet(String authV) throws Exception {
		String content = HttpClientAuthUtils.doGetByAuth(USER_URL, null, SysConstant.AUTH_KEY, SysConstant.AUTH_PRE_B,
				authV);
		return content;
	}

	/**
	 * http get 认证请求测试
	 * 
	 * @param json
	 * @return
	 * @throws Exception
	 */
	private String testGetUserList(String authV) throws Exception {
		String content = HttpClientAuthUtils.doGetByAuth(SYS_USER_LIST_URL, null, SysConstant.AUTH_KEY,
				SysConstant.AUTH_PRE_B, authV);
		return content;
	}

	/**
	 * http get
	 * @param json
	 * @return
	 * @throws Exception
	 */
	private String revokeToken(String authV) throws Exception {
		String content = HttpClientAuthUtils.doGetByAuth(REVOKE_TOKEN_URL, null, SysConstant.AUTH_KEY,
				SysConstant.AUTH_PRE_B, authV);
		return content;
	}

	public static void main(String[] args) throws Exception {
		 new Test().start();
		 //System.out.println(new Test().testGetUserList("59f0de24-6fe5-4105-8dbd-1eed68c182041"));
	}

}
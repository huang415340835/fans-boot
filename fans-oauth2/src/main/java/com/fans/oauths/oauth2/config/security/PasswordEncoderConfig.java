package com.fans.oauths.oauth2.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @ClassName: PasswordEncoderConfig
 * @Description: 加密方式配置
 * @author fanhaohao
 * @date 2018年12月5日 下午3:09:09
 */
@Configuration
public class PasswordEncoderConfig {
	@Bean
	public PasswordEncoder passwordEncoder() {
		//BCryptPasswordEncoder为 spring security中的方法，采用SHA-256 +随机盐+密钥对密码进行加密。
		return new BCryptPasswordEncoder();
	}

	//获取加密后的密码
	public static void main(String[] args) {
		String encode = new BCryptPasswordEncoder().encode("123456");
		System.out.println(encode);
		System.out.println(new BCryptPasswordEncoder().matches("1234561", encode));
	}
}

package com.fans.oauths.oauth2.util;


import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;

import com.fans.common.config.redis.util.JedisUtils;
import com.fans.common.constant.SysConstant;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.SpringUtil;

/**
 * @ClassName: UserUtil
 * @Description: 获取token
 * @author fanhaohao
 * @date 2018年12月7日 下午1:28:39
 */
public class UserUtil {

	/**
	 * 
	 * @Title：getUser  
	 * @Description: 获取当前用户信息
	 * @author: fanhaohao
	 * @date 2018年12月12日 下午4:58:04 
	 * @param @param request
	 * @param @return 
	 * @return User 
	 * @throws
	 */
	public static User getUser() {
		if (BlankUtils.isNotBlank(SecurityContextHolder.getContext().getAuthentication())) {
			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return user;
		} else {
			return null;
		}
	}
	
	/**
	 * @Title：getUsername  
	 * @Description: 获取用户名
	 * @author: fanhaohao
	 * @date 2018年12月12日 下午5:01:40 
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	public static String getUsername() {
		if (BlankUtils.isNotBlank(getUser())) {// 经过认证过的获取方式（security获取方式）
			return getUser().getUsername();
		} else {// OAuth2获取方式
			TokenStore tokenStore = SpringUtil.getBean(TokenStore.class);
			OAuth2AccessToken accessToken = tokenStore.readAccessToken(TokenUtil.getToken());
			OAuth2Authentication authentication = tokenStore.readAuthentication(accessToken);
			return authentication.getName();
		}
	}

	/**
	 * @Title：getUserId
	 * @Description: 获取用户id
	 * @author: fanhaohao
	 * @date 2018年12月12日 下午5:01:40 
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	public static String getUserId() {
		String id = null;
		String username = null;
		if (BlankUtils.isNotBlank(getUsername())) {
			username = getUsername();
		}
		if(BlankUtils.isNotBlank(username)){
			id= String.valueOf(JedisUtils.getObject(SysConstant.NAME2ID_KEY + username));
		}
		return id;
	}
}

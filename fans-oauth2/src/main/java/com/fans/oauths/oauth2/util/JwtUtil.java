package com.fans.oauths.oauth2.util;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.fans.utils.utils.BlankUtils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

/**
 * @ClassName: JwtUtil
 * @Description: jwt 根据请求获取token，claims以及用户信息
 * @author fanhaohao
 * @date 2018年12月7日 上午9:51:49
 */
public class JwtUtil {
	private static final Logger log = LoggerFactory.getLogger(JwtUtil.class);
	
	@Value("${security.oauth2.jwt.signing-key}")
	private static String signingKey = "fans";

	/**
	 * @Title：getUserName  
	 * @Description: 获取用户名
	 * @author: fanhaohao
	 * @date 2018年12月7日 上午9:40:24 
	 * @param @param request
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	public static String getUserName() {
		String userName = null;
		Claims claims = getClaims();
		if (BlankUtils.isNotBlank(claims)) {
			userName = String.valueOf(claims.get("user_name"));
		}
		return userName;
	}

	/**
	 * @Title：getClaims  
	 * @Description: 根据token获取Claims
	 * @author: fanhaohao
	 * @date 2018年12月7日 上午9:24:00 
	 * @param @param token
	 * @param @return 
	 * @return Claims 
	 * @throws
	 */
	public static Claims getClaims() {
		Claims claims = null;
		String token = TokenUtil.getToken();
		if (BlankUtils.isNotBlank(token)) {
			try {
				claims = Jwts.parser().setSigningKey(signingKey.getBytes("UTF-8")).parseClaimsJws(token).getBody();
			} catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException
					| IllegalArgumentException | UnsupportedEncodingException e) {
				log.error("Jwts=》Claims解析token异常", e);
			}
		}
		return claims;
	}



}

package com.fans.oauths.oauth2.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @ClassName: UserRoleDao  
 * @Description: 获取用户的角色信息 
 * @author fanhaohao
 * @date 2018年12月20日 上午10:53:26
 */
@Mapper
public interface UserRoleDao {

	/**
	 * @Title：getUserRoleList  
	 * @Description: 获取用户的角色信息 
	 * @author: fanhaohao
	 * @date 2018年12月20日 上午10:53:56 
	 * @param @param userId
	 * @param @return 
	 * @return List<String> 
	 * @throws
	 */
	@Select("select sr.role as role from sys_role sr,sys_user_role sur where sur.role_id=sr.id and sur.del_flag='0' and sr.del_flag='0' and sur.user_id = #{userId}")
	List<String> getUserRoleList(String userId);

}

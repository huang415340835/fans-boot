package com.fans.oauths.oauth2.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fans.common.constant.SysConstant;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.SpringUtil;

/**
 * @ClassName: TokenUtil
 * @Description: 获取token
 * @author fanhaohao
 * @date 2018年12月7日 下午1:28:39
 */
public class TokenUtil {
	/**
	 * @Title：getToken
	 * @Description: 获取token
	 * @author: fanhaohao
	 * @date 2018年12月7日 上午9:51:25
	 * @param @return
	 * @return String
	 * @throws
	 */
	public static String getToken() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		String token = null;
		String authorization = request.getHeader(SysConstant.AUTH_KEY);
		if (BlankUtils.isNotBlank(authorization)
				&& (authorization.contains(SysConstant.AUTH_PRE_S) || authorization.contains(SysConstant.AUTH_PRE_B))) {
			if (authorization.contains(SysConstant.AUTH_PRE_S)) {
				token = StringUtils.substringAfter(authorization, SysConstant.AUTH_PRE_S);
			} else if (authorization.contains(SysConstant.AUTH_PRE_B)) {
				token = StringUtils.substringAfter(authorization, SysConstant.AUTH_PRE_B);
			}
		}
		return token;
	}

	/**
	 * @Title：validateToken  
	 * @Description: 验证token是否存在且不过期
	 * @author: fanhaohao
	 * @date 2018年12月14日 下午2:22:10 
	 * @param @return 
	 * @return Boolean 
	 * @throws
	 */
	public static Boolean validateToken() {
		Boolean flag = false;
		String token = getToken();
		if (BlankUtils.isNotBlank(token)) {
			TokenStore tokenStore = SpringUtil.getBean(TokenStore.class);
			OAuth2AccessToken accessToken = tokenStore.readAccessToken(token);
			if (BlankUtils.isNotBlank(accessToken) && !accessToken.isExpired()) {
				flag = true;
			}
		}
		return flag;
	}
}

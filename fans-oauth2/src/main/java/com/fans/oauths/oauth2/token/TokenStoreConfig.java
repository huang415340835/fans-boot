package com.fans.oauths.oauth2.token;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import com.fans.oauths.oauth2.token.store.RedisTemplateTokenStore;

/**
 * @ClassName: TokenStoreConfig
 * @Description: token存储存储方式（redis jdbc jwt）
 * @author fanhaohao
 * @date 2018年12月5日 下午3:13:09
 */
@Configuration
public class TokenStoreConfig {

	@Resource
	private DataSource dataSource;

	@Autowired(required = false)
	private RedisTemplate<String, Object> redisTemplate;

	/**
	 * @Title：jdbcTokenStore
	 * @Description: token存储存储方式（jdbc）
	 * @author: fanhaohao
	 * @date 2018年12月6日 下午4:16:24
	 * @param @return
	 * @return JdbcTokenStore
	 * @throws
	 */
	@Bean
	@ConditionalOnProperty(prefix = "security.oauth2.token.store", name = "type", havingValue = "jdbc", matchIfMissing = true)
	public JdbcTokenStore jdbcTokenStore() {
		return new JdbcTokenStore(dataSource);
	}

	/**
	 * @Title：redisTokenStore
	 * @Description: token存储存储方式（redis）
	 * @author: fanhaohao
	 * @date 2018年12月6日 下午4:17:32
	 * @param @return
	 * @return RedisTemplateTokenStore
	 * @throws
	 */
	@Bean
	@ConditionalOnProperty(prefix = "security.oauth2.token.store", name = "type", havingValue = "redis", matchIfMissing = true)
	public RedisTemplateTokenStore redisTokenStore() {
		RedisTemplateTokenStore redisTemplateStore = new RedisTemplateTokenStore();
		redisTemplateStore.setRedisTemplate(redisTemplate);
		return redisTemplateStore;
	}

	/**
	 * @ClassName: JWTTokenConfig
	 * @Description: token存储存储方式（jwt）
	 * @author fanhaohao
	 * @date 2018年12月6日 下午4:17:58
	 */
	@Configuration
	@ConditionalOnProperty(prefix = "security.oauth2.token.store", name = "type", havingValue = "jwt", matchIfMissing = true)
	public static class JWTTokenConfig {
		@Value("${security.oauth2.jwt.signing-key}")
		private String signingKey = "fans";

		@Bean
		public JwtTokenStore jwtTokenStore() {
			return new JwtTokenStore(jwtAccessTokenConverter());
		}
		
		/**
		 * token converter
		 * 
		 * @return
		 */
		@Bean
		public JwtAccessTokenConverter jwtAccessTokenConverter() {
			JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter() {
				/***
				 * 重写增强token方法,用于自定义一些token返回的信息
				 */
				@Override
				public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
					String userName = authentication.getUserAuthentication().getName();
					User user = (User) authentication.getUserAuthentication().getPrincipal();// 与登录时候放进去的UserDetail实现类一直查看link{SecurityConfiguration}
					/** 自定义一些token属性 ***/
					final Map<String, Object> additionalInformation = new HashMap<>();
					additionalInformation.put("userName", userName);
					additionalInformation.put("sysName", "fansBoot");
					additionalInformation.put("roles", user.getAuthorities());
					((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInformation);
					OAuth2AccessToken enhancedToken = super.enhance(accessToken, authentication);
					return enhancedToken;
				}

			};
			// 签名达到一个对称加密的效果,使用RSA非对称加密方式
			accessTokenConverter.setSigningKey(signingKey);
			return accessTokenConverter;
		}

		@Bean
		@Primary
		public DefaultTokenServices tokenServices() {
			DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
			defaultTokenServices.setTokenStore(jwtTokenStore());
			defaultTokenServices.setSupportRefreshToken(true);
			return defaultTokenServices;
		}

	}

}

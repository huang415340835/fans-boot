package com.fans.oauths.oauth2.config.swagger;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ClassName: SwaggerConfig
 * @Description: swagger 配置
 * @author fanhaohao
 * @date 2018年12月5日 下午3:10:54
 */
@Component
@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurerAdapter {
	@Bean
	public Docket api() {
		List<Parameter> pars = new ArrayList<>();
		//header增加令牌参数 ,非必填
		ParameterBuilder tokenPar = new ParameterBuilder();
		tokenPar.name("Authorization").description("令牌").modelRef(new ModelRef("string")).parameterType("header")
				.required(false).build();
		pars.add(tokenPar.build());
		//header增加应用ID参数,非必填
		ParameterBuilder clientPar = new ParameterBuilder();
		clientPar.name("client_id").description("应用ID").modelRef(new ModelRef("string")).parameterType("header")
				.required(false).build();
		pars.add(clientPar.build());
		//header增加应用密钥参数,非必填
		ParameterBuilder secretPar = new ParameterBuilder();
		secretPar.name("client_secret").description("应用密钥").modelRef(new ModelRef("string")).parameterType("header")
				.required(false).build();
		pars.add(secretPar.build());

		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class)) //有该注解的显示
				.apis(RequestHandlerSelectors.any())
				.paths(input -> PathSelectors.regex("/user.*").apply(input)
						|| PathSelectors.regex("/client.*").apply(input)
						|| PathSelectors.regex("/oauth.*").apply(input))//根据正则表达式来暴露接口
				.paths(PathSelectors.any()).build().globalOperationParameters(pars);
	}

	@SuppressWarnings("deprecation")
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("认证中心api").description("认证中心api").termsOfServiceUrl("no terms of service")
				.version("2.0").contact("fans").build();
	}

	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setViewClass(JstlView.class);
		resolver.setPrefix("/");
		resolver.setSuffix(".html");
		return resolver;

	}

	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("messages");
		return messageSource;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		super.addResourceHandlers(registry);
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

}
package com.fans.oauths.oauth2.config.oauth;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.code.RandomValueAuthorizationCodeServices;

import com.fans.oauths.oauth2.redis.service.RedisAuthorizationCodeServices;
import com.fans.oauths.oauth2.redis.service.RedisClientDetailsService;

/**
 * @ClassName: OAuth2ServerConfig
 * @Description: clientdetail获取方式配置
 * @author fanhaohao
 * @date 2018年12月5日 下午3:05:43
 */
@Configuration
public class OAuth2ServerConfig {
	@Resource
	private DataSource dataSource;
	@Resource
	private RedisTemplate<String, Object> redisTemplate;

	/**
	 * @Title：clientDetailsService
	 * @Description: 从redis中获取clientdetail信息
	 * @author: fanhaohao
	 * @date 2018年12月5日 下午3:06:20
	 * @param @return
	 * @return RedisClientDetailsService
	 * @throws
	 */
	@Bean
	@ConditionalOnProperty(prefix = "security.oauth2.token.store", name = "type", havingValue = "redis", matchIfMissing = true)
	public RedisClientDetailsService redisClientDetailsService() {
		RedisClientDetailsService clientDetailsService = new RedisClientDetailsService(dataSource);
		clientDetailsService.setRedisTemplate(redisTemplate);
		return clientDetailsService;
	}
	
	/**
	 * @Title：JdbcClientDetailsService
	 * @Description: 从mysql（jdbc）中获取clientdetail信息
	 * @author: fanhaohao
	 * @date 2018年12月5日 下午3:06:20
	 * @param @return
	 * @return JdbcClientDetailsService
	 * @throws
	 */
	@Bean
	@ConditionalOnProperty(prefix = "security.oauth2.token.store.type", value = {"jdbc","jwt"}, matchIfMissing = true)
	public JdbcClientDetailsService jdbcClientDetailsService() {
		return new JdbcClientDetailsService(dataSource);
	}
	
	/**
	 * @Title：authorizationCodeServices
	 * @Description: 向容器中 注入 RandomValueAuthorizationCodeServices 对象
	 * @author: fanhaohao
	 * @date 2018年12月5日 下午3:06:33
	 * @param @return
	 * @return RandomValueAuthorizationCodeServices
	 * @throws
	 */
	@Bean
	public RandomValueAuthorizationCodeServices authorizationCodeServices() {
		RedisAuthorizationCodeServices redisAuthorizationCodeServices = new RedisAuthorizationCodeServices();
		redisAuthorizationCodeServices.setRedisTemplate(redisTemplate);
		return redisAuthorizationCodeServices;
	}
}

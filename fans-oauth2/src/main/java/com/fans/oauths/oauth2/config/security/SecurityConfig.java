package com.fans.oauths.oauth2.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.web.cors.CorsUtils;

import com.fans.common.config.properties.IgnoreUrlsProperties;
import com.fans.oauths.oauth2.config.oauth.OauthLogoutHandler;
import com.fans.utils.utils.BlankUtils;

/**
 * @ClassName: SecurityConfig
 * @Description: spring security配置 在WebSecurityConfigurerAdapter不拦截oauth要开放的资源
 * @author fanhaohao
 * @date 2018年12月5日 下午3:09:30
 */
@Order(4)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;
	@Autowired
	private AuthenticationFailureHandler authenticationFailureHandler;
	@Autowired(required = false)
	private AuthenticationEntryPoint authenticationEntryPoint;
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private OauthLogoutHandler oauthLogoutHandler;
	@Autowired
	private IgnoreUrlsProperties ignoreUrlsProperties;

	@Override
	public void configure(WebSecurity web) throws Exception {
		ignoreUrlsProperties.getWebUrls().forEach(url -> {
			if (BlankUtils.isNotBlank(url)) {
				web.ignoring().antMatchers(url);
			}
		});
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = http.formLogin()
				.loginPage("/login.html").loginProcessingUrl("/user/login").successHandler(authenticationSuccessHandler)
				.failureHandler(authenticationFailureHandler).and().authorizeRequests();
		// 让Spring security放行所有preflight request (允许跨域)
		registry.requestMatchers(CorsUtils::isPreFlightRequest).permitAll();
		ignoreUrlsProperties.getHttpUrls().forEach(url -> {
			if (BlankUtils.isNotBlank(url)) {
				registry.antMatchers(url).permitAll();
			}
		});

		registry.anyRequest().authenticated().and().cors().and().csrf().disable();

		// 基于密码 等模式可以无session,不支持授权码模式
		if (authenticationEntryPoint != null) {
			http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
			http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		} else {
			// 授权码模式单独处理，需要session的支持，此模式可以支持所有oauth2的认证
			http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
		}

		http.logout().logoutUrl("/user/logout").clearAuthentication(true)
				.logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler())
				.addLogoutHandler(oauthLogoutHandler).deleteCookies("JSESSIONID", "FP-UID").invalidateHttpSession(true)
				.logoutSuccessUrl("/login");// 退出成功后，跳转到/路径。

		// 解决不允许显示在iframe的问题
		http.headers().frameOptions().disable();
		http.headers().cacheControl();

	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		//加载用户信息
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
	}


}

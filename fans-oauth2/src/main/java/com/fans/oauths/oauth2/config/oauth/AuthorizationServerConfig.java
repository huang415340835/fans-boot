package com.fans.oauths.oauth2.config.oauth;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.code.RandomValueAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.stereotype.Component;

import com.fans.oauths.oauth2.redis.service.RedisClientDetailsService;
import com.fans.oauths.oauth2.token.store.RedisTemplateTokenStore;
import com.fans.utils.utils.BlankUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @ClassName: AuthorizationServerConfig
 * @Description: 默认token存储在内存中 DefaultTokenServices默认处理
 * @author fanhaohao
 * @date 2018年12月5日 下午3:04:44
 */
@Component
@Configuration
@EnableAuthorizationServer
@AutoConfigureAfter(AuthorizationServerEndpointsConfigurer.class)
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
	/**
	 * 注入authenticationManager 来支持 password grant type
	 */
	@Autowired
	private AuthenticationManager authenticationManager;

	@Resource
	private ObjectMapper objectMapper; // springmvc启动时自动装配json处理类

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired(required = false)
	private RedisTemplateTokenStore redisTokenStore;

	@Autowired(required = false)
	private JwtTokenStore jwtTokenStore;

	@Autowired(required = false)
	private JdbcTokenStore jdbcTokenStore;

	@Autowired(required = false)
	private JwtAccessTokenConverter jwtAccessTokenConverter;

	@Autowired
	private WebResponseExceptionTranslator webResponseExceptionTranslator;

	@Autowired(required = false)
	private RedisClientDetailsService redisClientDetailsService;

	@Autowired(required = false)
	private RandomValueAuthorizationCodeServices authorizationCodeServices;

	@Autowired(required = false)
	private JdbcClientDetailsService jdbcClientDetailsService;

	@Value("${access_token.validity_period:3600}") // 默认值3600
	int accessTokenValiditySeconds = 3600;

	@Value("${resource.id:spring-boot-application}") // 默认值spring-boot-application
	private String resourceId;

	/**
	 * 配置身份认证器，配置认证方式，TokenStore，TokenGranter，OAuth2RequestFactory
	 */
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		if (BlankUtils.isNotBlank(jwtTokenStore)) {
			endpoints.tokenStore(jwtTokenStore).authenticationManager(authenticationManager).accessTokenConverter(jwtAccessTokenConverter).userDetailsService(userDetailsService);// 支持jwt
		} else if (BlankUtils.isNotBlank(redisTokenStore)) {
			endpoints.tokenStore(redisTokenStore).authenticationManager(authenticationManager).userDetailsService(userDetailsService); // 支持 redis
		} else if (BlankUtils.isNotBlank(jdbcTokenStore)) {
			endpoints.tokenStore(jdbcTokenStore).authenticationManager(authenticationManager).userDetailsService(userDetailsService); // 支持 jdbc
		}
		endpoints.authorizationCodeServices(authorizationCodeServices);
		endpoints.exceptionTranslator(webResponseExceptionTranslator);
	}

	// 配置OAuth2的客户端相关信息
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		if (BlankUtils.isNotBlank(redisClientDetailsService)) {// 从redis中来获取客户端相关信息
			clients.withClientDetails(redisClientDetailsService);
			redisClientDetailsService.loadAllClientToCache();
		} else if (BlankUtils.isNotBlank(jdbcClientDetailsService)) {// 从jdbc中来获取客户端相关信息
			clients.withClientDetails(jdbcClientDetailsService);
		}
	}

	// 对应于配置AuthorizationServer安全认证的相关信息，创建ClientCredentialsTokenEndpointFilter核心过滤器
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()") /// url:/oauth/token_key,exposes
				.checkTokenAccess("isAuthenticated()") // url:/oauth/check_token
				.allowFormAuthenticationForClients();
		// security.allowFormAuthenticationForClients();
		// security.tokenKeyAccess("permitAll()");
		// security.tokenKeyAccess("isAuthenticated()");
	}

}

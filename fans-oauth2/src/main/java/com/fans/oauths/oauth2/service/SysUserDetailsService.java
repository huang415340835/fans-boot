package com.fans.oauths.oauth2.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.fans.common.config.redis.util.JedisUtils;
import com.fans.common.constant.SysConstant;
import com.fans.oauths.oauth2.dao.UserDao;
import com.fans.oauths.oauth2.dao.UserRoleDao;
import com.fans.utils.utils.BlankUtils;


/**
 * @ClassName: SysUserDetailsService
 * @Description: 重写 security的 UserDetailsService 方法（设置用户即权限信息）
 * @author fanhaohao
 * @date 2018年12月6日 下午4:30:51
 */
@Component
public class SysUserDetailsService implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private UserDao userDao;
    
    @Resource
    private UserRoleDao userRoleDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Map<String, Object> sysUser = userDao.getUser(username);

        if (BlankUtils.isBlank(sysUser)) {
        	logger.info(username+" ---> 该用户名不存在!");
            throw new AuthenticationCredentialsNotFoundException("用户名不存在!");
        }
        
        //利用Redis来保存
        storeIdAndName(sysUser);
        //获取密码
        String password = String.valueOf(sysUser.get("password"));
        //获取用户id
        String userId = String.valueOf(sysUser.get("id"));
        //根据用户id来获取角色标识list
        List<String> userRoleList = userRoleDao.getUserRoleList(userId);

        User user = new User(username, password,     
                true, true, true, true, //是否有效，账户是否过期，密码是否过期，是否锁定，
                getAuthorities(userRoleList));
        return user;
    }
    
	// 角色授权
    public Collection<? extends GrantedAuthority> getAuthorities(List<String> userRoleList) {
        List<GrantedAuthority> authorityList = new ArrayList<>();
        for (String role : userRoleList) {
			if (BlankUtils.isNotBlank(role)) {
				authorityList.add(new SimpleGrantedAuthority(role));
			}
        }
		authorityList.add(new SimpleGrantedAuthority(SysConstant.BASE_ROLE));
        return authorityList;
    }

    //利用Redis来保存
    private void storeIdAndName(Map<String, Object> sysUser){
    	String id=String.valueOf(sysUser.get("id"));
    	String username=String.valueOf(sysUser.get("username"));
    	if(BlankUtils.isNotBlank(id)&& BlankUtils.isNotBlank(username)){
    		JedisUtils.setObject(SysConstant.NAME2ID_KEY + username, id,0);
    		JedisUtils.setObject(SysConstant.ID2NAME_KEY + id, username,0);
    		JedisUtils.setObject(SysConstant.ID2USER_KEY + id, sysUser,0);
    	}
    }

}

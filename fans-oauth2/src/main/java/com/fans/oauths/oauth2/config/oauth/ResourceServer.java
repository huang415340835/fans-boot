package com.fans.oauths.oauth2.config.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.web.cors.CorsUtils;

import com.fans.common.config.properties.IgnoreUrlsProperties;
import com.fans.utils.utils.BlankUtils;

/**
 * @ClassName: ResourceServer
 * @Description: add for sso 在ResourceServerConfigurerAdapter配置需要token验证的资源
 * @author fanhaohao
 * @date 2018年12月5日 下午3:08:32
 */
@Order(6)
@Configuration
@EnableResourceServer
public class ResourceServer extends ResourceServerConfigurerAdapter {

	@Autowired(required = false)
	private DefaultTokenServices jwtDefaultTokenServices;
	
	@Autowired
	private IgnoreUrlsProperties ignoreUrlsProperties;

	@Override
	public void configure(HttpSecurity http) throws Exception {
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = http.formLogin().and().authorizeRequests();
		// 让Spring security放行所有preflight request (允许跨域)
		registry.requestMatchers(CorsUtils::isPreFlightRequest).permitAll();
		ignoreUrlsProperties.getHttpUrls().forEach(url -> {
			if (BlankUtils.isNotBlank(url)) {
				registry.antMatchers(url).permitAll();
			}
		});

		registry.anyRequest().authenticated().and().cors().and().csrf().disable();
	}

	@Override
	@ConditionalOnProperty(prefix = "security.oauth2.token.store", name = "type", havingValue = "jwt", matchIfMissing = true)
	public void configure(ResourceServerSecurityConfigurer config) {
		config.tokenServices(jwtDefaultTokenServices);
	}

}
﻿package com.fans.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @ClassName: AdminApp
 * @Description: 启动类
 * @author fanhaohao
 * @date 2018年12月5日 下午3:13:28
 */

@SpringBootApplication()
@ComponentScan(basePackages = { "com.fans" })
public class AdminApp {

	public static void main(String[] args) {
		SpringApplication.run(AdminApp.class, args);
	}

}

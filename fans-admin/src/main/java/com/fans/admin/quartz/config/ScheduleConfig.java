package com.fans.admin.quartz.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * 定时任务配置
 *
 * @author fhh
 * @email fhh@gmail.com
 * @date 2017-10-20 23:38
 */
@Configuration
@EnableScheduling
public class ScheduleConfig {
	
	
	  @Autowired
	  private JobStoreProperties jobStoreProperties;
	  
	  @Autowired
	  private SchedulerProperties schedulerProperties;
	  
	  @Autowired
	  private ThreadPoolProperties threadPoolProperties;
	  
	  @Autowired
	  private PlatformTransactionManager platformTransactionManager;
	  @Autowired
	  private ApplicationContext applicationContext;
	  

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(DataSource dataSource) {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        factory.setTransactionManager(platformTransactionManager);
        factory.setApplicationContext(applicationContext);
        factory.setSchedulerFactoryClass(StdSchedulerFactory.class);
        factory.setWaitForJobsToCompleteOnShutdown(schedulerProperties.isWaitOnShutdown());
        factory.setDataSource(dataSource);

        //quartz参数
        Properties prop = new Properties();
        prop.put("org.quartz.scheduler.instanceName", schedulerProperties.getInstanceName());
        prop.put("org.quartz.scheduler.instanceId", schedulerProperties.getInstanceId());
        //线程池配置
        prop.put("org.quartz.threadPool.class", threadPoolProperties.getThreadPoolClass());
        prop.put("org.quartz.threadPool.threadCount", threadPoolProperties.getThreadCount());
        prop.put("org.quartz.threadPool.threadPriority", threadPoolProperties.getThreadPriority());
        //JobStore配置
        prop.put("org.quartz.jobStore.class", jobStoreProperties.getJobStoreClass());
        //集群配置
        prop.put("org.quartz.jobStore.isClustered", jobStoreProperties.isClustered());
        prop.put("org.quartz.jobStore.clusterCheckinInterval", jobStoreProperties.getClusterCheckinInterval());
        prop.put("org.quartz.jobStore.maxMisfiresToHandleAtATime", jobStoreProperties.getMaxMisfiresToHandleAtATime());
        prop.put("org.quartz.jobStore.misfireThreshold", jobStoreProperties.getMisfireThreshold());
        prop.put("org.quartz.jobStore.tablePrefix", jobStoreProperties.getTablePrefix());
        factory.setQuartzProperties(prop);

        factory.setSchedulerName(schedulerProperties.getInstanceName());
        //延时启动
        factory.setStartupDelay(schedulerProperties.getStartupDelay());
        factory.setApplicationContextSchedulerContextKey(schedulerProperties.getApplicationContextKey());
        //可选，QuartzScheduler 启动时更新己存在的Job，这样就不用每次修改targetObject后删除qrtz_job_details表对应记录了
        factory.setOverwriteExistingJobs(schedulerProperties.isOverwriteExistingJobs());
        //设置自动启动，默认为true
        factory.setAutoStartup(schedulerProperties.isAutoStartup());

        return factory;
    }
}

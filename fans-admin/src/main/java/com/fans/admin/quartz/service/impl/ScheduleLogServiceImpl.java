package com.fans.admin.quartz.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.fans.admin.quartz.entity.ScheduleLog;
import com.fans.admin.quartz.dao.ScheduleLogMapper;
import com.fans.admin.quartz.service.IScheduleLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fans.oauths.oauth2.util.UserUtil;

/**
 * @description: 定时任务执行日志表服务实现类
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月14 16:55:11
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class ScheduleLogServiceImpl extends ServiceImpl<ScheduleLogMapper,ScheduleLog> implements IScheduleLogService {

	@Autowired
    private ScheduleLogMapper scheduleLogMapper;
    
	/**
	 * @Title：list  
	 * @Description: 定时任务执行日志表分页查询
	 * @author: fanhaohao
	 * @date 2019年01月14 16:55:11
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<ScheduleLog> page,Map<String, Object> map){
		return scheduleLogMapper.list(page,map);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 定时任务执行日志表根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年01月14 16:55:11
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdLogic(Object id){
		ScheduleLog scheduleLog = new ScheduleLog();
		scheduleLog.setId(String.valueOf(id));
		scheduleLog.preUpdate();
		scheduleLog.setUpdateBy(UserUtil.getUserId());
		scheduleLog.setDelFlag("1");
		this.updateById(scheduleLog);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 定时任务执行日志表逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年01月14 16:55:11
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<ScheduleLog> list = new ArrayList<>();
		for (Object id : ids) {
			ScheduleLog scheduleLog = new ScheduleLog();
			scheduleLog.setId(String.valueOf(id));
			scheduleLog.preUpdate();
			scheduleLog.setUpdateBy(UserUtil.getUserId());
			scheduleLog.setDelFlag("1");
			list.add(scheduleLog);
		}
		this.updateBatchById(list);
	}
}

package com.fans.admin.quartz.qz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fans.admin.quartz.entity.ScheduleJob;
import com.fans.utils.utils.BlankUtils;
@Component
public class QzTest {
	private Logger logger = LoggerFactory.getLogger(QzTest.class);

	public void execute(ScheduleJob job) {
		logger.info("=============================测试定时任务【" + job.getJobName() + "】起来了!===========================");
		if(BlankUtils.isNotBlank(job)){
			logger.info("=============================测试定时任务【" + job.getJobName() + "】执行中===========================");
		}
		logger.info("=============================测试定时任务【" + job.getJobName() + "】结束了!===========================");
	}
}

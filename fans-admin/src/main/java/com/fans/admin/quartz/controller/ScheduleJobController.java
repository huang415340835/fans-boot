package com.fans.admin.quartz.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.quartz.constant.QtzConstant.ScheduleJobStatusEnum;
import com.fans.admin.quartz.constant.QtzConstant.ScheduleStatusEnum;
import com.fans.admin.quartz.entity.ScheduleJob;
import com.fans.admin.quartz.service.IScheduleJobService;
import com.fans.admin.quartz.warpper.ScheduleJobWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 定时任务表数据接口
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月14 16:55:11
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/quartz/scheduleJob")
public class ScheduleJobController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(ScheduleJobController.class);

    @Autowired
    private IScheduleJobService scheduleJobService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取定时任务表
     * @author: fanhaohao
     * @date 2019年01月14 16:55:11
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		ScheduleJob scheduleJob = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				scheduleJob = scheduleJobService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取定时任务表异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取定时任务表异常", e);
			return renderString(response, FansResp.error("根据id获取定时任务表异常！"));
		}
		return renderString(response, FansResp.successData(scheduleJob));
	}

	/**
	 * @Title：list  
	 * @Description: 获取定时任务表列表
	 * @author: fanhaohao
	 * @date 2019年01月14 16:55:11
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("scheduleJob:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(ScheduleJob scheduleJob, PageVo pv, String condition, HttpServletResponse response) {
		Page<ScheduleJob> page = new PageFactory<ScheduleJob>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(scheduleJob);
		map.put("condition", condition);
		List<Map<String, Object>> result = scheduleJobService.list(page, map);
		page.setRecords((List<ScheduleJob>) new ScheduleJobWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：create  
	 * @Description: 新建定时任务表
	 * @author: fanhaohao
	 * @date 2019年01月14 16:55:11
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建定时任务表")
	@RequiredPermission("scheduleJob:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(ScheduleJob scheduleJob, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isBlank(scheduleJob.getId())) {
				scheduleJob.setJobClassIsBeanName(0);
				scheduleJob.setJobStatus(ScheduleStatusEnum.USABLE.getCode());// 默认启用
				scheduleJob.setStatus(ScheduleJobStatusEnum.NOSTART.getCode());// 默认未启动
			}
			scheduleJobService.createOrUpdate(request, scheduleJob);
		} catch (Exception e) {
			logger.error("新建定时任务表异常", e);
			return renderString(response, FansResp.error("新建定时任务表异常！"));
		}
		return renderString(response, FansResp.successData(scheduleJob));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改定时任务表
	 * @author: fanhaohao
	 * @date 2019年01月14 16:55:11
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改定时任务表")
	@RequiredPermission("scheduleJob:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(ScheduleJob scheduleJob, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(scheduleJob.getId())) {
				scheduleJobService.createOrUpdate(request, scheduleJob);
			}else{
				return renderString(response, FansResp.error("修改定时任务表异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改定时任务表异常", e);
			return renderString(response, FansResp.error("修改定时任务表异常！"));
		}
		return renderString(response, FansResp.successData(scheduleJob));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 删除定时任务表
	 * @author: fanhaohao
	 * @date 2019年01月14 16:55:11
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除定时任务表")
	@RequiredPermission("scheduleJob:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				scheduleJobService.deleteScheduleJob(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除定时任务表异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除定时任务表异常", e);
			return renderString(response, FansResp.error("删除定时任务表异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	/**
	 * @Title：ServiceResultun  
	 * @Description: 立即执行任务
	 * @author: fanhaohao
	 * @date 2019年1月15日 下午1:27:32 
	 * @param @param ids
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "立即执行任务")
	@RequiredPermission("scheduleJob:run")
	@RequestMapping(value = "/run", method = RequestMethod.POST)
	public String ServiceResultun(String[] ids, HttpServletResponse response) {
		try {
			scheduleJobService.triggerScheduleJob(Arrays.asList(ids));
		} catch (SchedulerException e) {
			logger.error("立即执行任务异常", e);
			return renderString(response, FansResp.error("立即执行任务异常！"));
		}
		return renderString(response, FansResp.success());
	}

	/**
	 * 暂停定时任务
	 */
	@Log(operation = "暂停定时任务")
	@RequestMapping(value = "/pause", method = RequestMethod.POST)
	@RequiredPermission("scheduleJob:pause")
	public String pause(String[] ids, HttpServletResponse response) {
		try {
			scheduleJobService.pauseScheduleJob(Arrays.asList(ids));
		} catch (SchedulerException e) {
			logger.error("暂停定时任务异常", e);
			return renderString(response, FansResp.error("暂停定时任务异常！"));
		}
		return renderString(response, FansResp.success());
	}

	/**
	 * 恢复定时任务
	 */
	@Log(operation = "恢复定时任务")
	@RequestMapping(value = "/resume", method = RequestMethod.POST)
	@RequiredPermission("scheduleJob:resume")
	public String ServiceResultesume(String[] ids, HttpServletResponse response) {
		try {
			scheduleJobService.resumeScheduleJob(Arrays.asList(ids));
		} catch (SchedulerException e) {
			logger.error("恢复定时任务异常", e);
			return renderString(response, FansResp.error("恢复定时任务异常！"));
		}
		return renderString(response, FansResp.success());
	}
}

package com.fans.admin.quartz.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.admin.quartz.constant.QtzConstant.JobStatusEnum;
import com.fans.common.base.BaseEntity;
import com.fans.utils.utils.BlankUtils;

/**
 * @description: 定时任务表
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月14 16:55:11
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="quartz_schedule_job",resultMap="BaseResultMap")
public class ScheduleJob extends BaseEntity<ScheduleJob> {

    private static final long serialVersionUID = 1L;

	public static final String JOB_PARAM_KEY = "JOB_PARAM_KEY";

    /**
     * 任务名称
     */
    @TableField("job_name")
    private String jobName;
    /**
     * 任务状态（0：禁用；1：启用；2：删除）
     */
    @TableField("job_status")
    private String jobStatus;
    /**
     * 任务运行时间表达式
     */
    @TableField("cron_expression")
    private String cronExpression;
    /**
     * 指定jobClass是否是Spring Bean；（0：否；1：是）
     */
    @TableField("job_class_is_bean_name")
    private Integer jobClassIsBeanName;
    /**
     * 任务执行的类名或者Bean名
     */
    @TableField("job_class")
    private String jobClass;
    /**
     * 任务执行的方法
     */
    @TableField("job_method")
    private String jobMethod;
    /**
     * 任务执行总数
     */
    @TableField("job_exec_count")
    private Long jobExecCount;
    /**
     * 任务执行耗时，毫秒
     */
    @TableField("job_used_time")
    private Long jobUsedTime;
    /**
     * 任务异常总数
     */
    @TableField("job_exception_count")
    private Long jobExceptionCount;
    /**
     * 最后执行时间
     */
    @TableField("last_exec_time")
    private Date lastExecTime;
    /**
     * 下次执行时间
     */
    @TableField("next_exec_time")
    private Date nextExecTime;
    /**
     * 任务执行状态
     */
    private String status;
    /**
     * 最后异常时间
     */
    @TableField("last_exception_time")
    private Date lastExceptionTime;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Integer getJobClassIsBeanName() {
		return jobClassIsBeanName;
	}

	public void setJobClassIsBeanName(Integer jobClassIsBeanName) {
		this.jobClassIsBeanName = jobClassIsBeanName;
	}

	public String getJobClass() {
		return jobClass;
	}

	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}

	public String getJobMethod() {
		return jobMethod;
	}

	public void setJobMethod(String jobMethod) {
		this.jobMethod = jobMethod;
	}

	public Long getJobExecCount() {
		return jobExecCount;
	}

	public void setJobExecCount(Long jobExecCount) {
		this.jobExecCount = jobExecCount;
	}

	public Long getJobUsedTime() {
		return jobUsedTime;
	}

	public void setJobUsedTime(Long jobUsedTime) {
		this.jobUsedTime = jobUsedTime;
	}

	public Long getJobExceptionCount() {
		return jobExceptionCount;
	}

	public void setJobExceptionCount(Long jobExceptionCount) {
		this.jobExceptionCount = jobExceptionCount;
	}

	public Date getLastExecTime() {
		return lastExecTime;
	}

	public void setLastExecTime(Date lastExecTime) {
		this.lastExecTime = lastExecTime;
	}

	public Date getNextExecTime() {
		return nextExecTime;
	}

	public void setNextExecTime(Date nextExecTime) {
		this.nextExecTime = nextExecTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastExceptionTime() {
		return lastExceptionTime;
	}

	public void setLastExceptionTime(Date lastExceptionTime) {
		this.lastExceptionTime = lastExceptionTime;
	}

	// 指定jobClass是否是Spring Bean；（0：否；1：是）
	public boolean jobClassIsBeanNameOrNot() {
		if ("0".equals(this.getJobClassIsBeanName())) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 触发器状态 None：Trigger已经完成，且不会在执行，或者找不到该触发器，或者Trigger已经被删除； NORMAL:正常状态；
	 * PAUSED：暂停状态； COMPLETE：触发器完成，但是任务可能还正在执行中； BLOCKED：线程阻塞状态； ERROR：出现错误
	 */
	@TableField(exist = false)
	private String triggerStatus;

	/**
	 * 获取
	 * None：Trigger已经完成，且不会在执行，或者找不到该触发器，或者Trigger已经被删除；NORMAL:正常状态；PAUSED：暂停状态；
	 * COMPLETE：触发器完成，但是任务可能还正在执行中；BLOCKED：线程阻塞状态；ERROR：出现错误
	 * 
	 * @return triggerStatus
	 *         None：Trigger已经完成，且不会在执行，或者找不到该触发器，或者Trigger已经被删除；NORMAL:正常状态；
	 *         PAUSED：暂停状态；COMPLETE：触发器完成，但是任务可能还正在执行中；BLOCKED：线程阻塞状态；ERROR：出现错误
	 */
	public String getTriggerStatus() {
		return triggerStatus;
	}

	/**
	 * 设置
	 * None：Trigger已经完成，且不会在执行，或者找不到该触发器，或者Trigger已经被删除；NORMAL:正常状态；PAUSED：暂停状态；
	 * COMPLETE：触发器完成，但是任务可能还正在执行中；BLOCKED：线程阻塞状态；ERROR：出现错误
	 * 
	 * @param triggerStatus
	 *            None：Trigger已经完成，且不会在执行，或者找不到该触发器，或者Trigger已经被删除；NORMAL:正常状态；
	 *            PAUSED：暂停状态；COMPLETE：触发器完成，但是任务可能还正在执行中；BLOCKED：线程阻塞状态；ERROR：
	 *            出现错误
	 */
	public void setTriggerStatus(String triggerStatus) {
		this.triggerStatus = triggerStatus;
	}

	/**
	 * 获取
	 * None：Trigger已经完成，且不会在执行，或者找不到该触发器，或者Trigger已经被删除；NORMAL:正常状态；PAUSED：暂停状态；
	 * COMPLETE：触发器完成，但是任务可能还正在执行中；BLOCKED：线程阻塞状态；ERROR：出现错误
	 * 
	 * @return triggerStatus
	 *         None：Trigger已经完成，且不会在执行，或者找不到该触发器，或者Trigger已经被删除；NORMAL:正常状态；
	 *         PAUSED：暂停状态；COMPLETE：触发器完成，但是任务可能还正在执行中；BLOCKED：线程阻塞状态；ERROR：出现错误
	 */
	public String getTriggerStatusStr() {
		try {
			JobStatusEnum triggerStatusEnum = JobStatusEnum.valueOf(triggerStatus);
			return null != triggerStatusEnum ? triggerStatusEnum.getLabel() : triggerStatus;
		} catch (Exception e) {

		}
		return triggerStatus;
	}

	public String getTriggerStatusName() {
		if (BlankUtils.isBlank(triggerStatus)) {
			return "未启动";
		}
		return getTriggerStatusStr();
	}
}

package com.fans.admin.quartz.job;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;

/**
 * 由于JOB需要存储到数据库中，会产生PROPERTY的问题，需剔除JOB-DATA
 * Needed to set Quartz useProperties=true when using Spring classes,
 * because Spring sets an object reference on JobDataMap that is not a String
 * @author chenchunrong
 *
 */
public class PersistableCronTriggerFactoryBean extends CronTriggerFactoryBean {
	
	private Logger logger = LoggerFactory.getLogger(PersistableCronTriggerFactoryBean.class);
	
	@Override
	public void afterPropertiesSet() {
		try {
			super.afterPropertiesSet();
			logger.info("==========================PersistableCronTriggerFactoryBean===========================");
			// Remove the JobDetail element
			//getJobDataMap().remove(JobDetailAwareTrigger.JOB_DETAIL_KEY);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}

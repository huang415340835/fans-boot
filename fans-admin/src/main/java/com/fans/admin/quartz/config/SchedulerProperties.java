package com.fans.admin.quartz.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * #============================================================================
 * # Configure Main Scheduler Properties # Needed to manage cluster instances
 * #============================================================================
 * ## \u9ed8\u8ba4\u4f1a\u8054\u7f51\u68c0\u67e5\u662f\u5426\u6709\u66f4\u65b0\
 * uff0c\u8fd8\u662fskip\u4e3a\u597d org.quartz.scheduler.skipUpdateCheck=true #
 * Needed to manage cluster instances org.quartz.scheduler.instanceId=AUTO
 * org.quartz.scheduler.instanceName=MY_JOB_SCHEDULER
 * org.quartz.scheduler.rmi.export=false org.quartz.scheduler.rmi.proxy=false
 * 
 * @author yubingfeng
 *
 */
@Component
@ConfigurationProperties(prefix = "org.quartz.scheduler")
@PropertySource("classpath:quartz.properties")
public class SchedulerProperties {

	private boolean skipUpdateCheck = true;
	private String instanceId = "AUTO";
	private String instanceName = "MY_JOB_SCHEDULER";
	private boolean rmiexport = false;
	private boolean rmiproxy = false;

	private int startupDelay=5;

	private String applicationContextKey="applicationContextKey";

	private boolean overwriteExistingJobs=true;

	private boolean  autoStartup=true;
	
	private boolean  waitOnShutdown=true;

	

	public int getStartupDelay() {
		return startupDelay;
	}

	public void setStartupDelay(int startupDelay) {
		this.startupDelay = startupDelay;
	}

	public String getApplicationContextKey() {
		return applicationContextKey;
	}

	public void setApplicationContextKey(String applicationContextKey) {
		this.applicationContextKey = applicationContextKey;
	}

	

	public boolean isSkipUpdateCheck() {
		return skipUpdateCheck;
	}

	public void setSkipUpdateCheck(boolean skipUpdateCheck) {
		this.skipUpdateCheck = skipUpdateCheck;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public boolean isRmiexport() {
		return rmiexport;
	}

	public void setRmiexport(boolean rmiexport) {
		this.rmiexport = rmiexport;
	}

	public boolean isRmiproxy() {
		return rmiproxy;
	}

	public void setRmiproxy(boolean rmiproxy) {
		this.rmiproxy = rmiproxy;
	}

	public boolean isOverwriteExistingJobs() {
		return overwriteExistingJobs;
	}

	public void setOverwriteExistingJobs(boolean overwriteExistingJobs) {
		this.overwriteExistingJobs = overwriteExistingJobs;
	}

	public boolean isAutoStartup() {
		return autoStartup;
	}

	public void setAutoStartup(boolean autoStartup) {
		this.autoStartup = autoStartup;
	}

	public boolean isWaitOnShutdown() {
		return waitOnShutdown;
	}

	public void setWaitOnShutdown(boolean waitOnShutdown) {
		this.waitOnShutdown = waitOnShutdown;
	}
	
	
	

}

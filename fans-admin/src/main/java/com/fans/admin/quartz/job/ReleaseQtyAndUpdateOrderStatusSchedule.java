package com.fans.admin.quartz.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 * 应为要持久化等特性操作,需要继承 QuartzJobBean
 * <br>由于要被持久化,所以不能存放xxxxManager类似对象,
 * 只能从每次从QuartzJobBean注入的ApplicationContext 中去取出
 * @author chenchunrong
 *
 */
@Component
public class ReleaseQtyAndUpdateOrderStatusSchedule extends QuartzJobBean {
	
	private Logger logger = LoggerFactory.getLogger(ReleaseQtyAndUpdateOrderStatusSchedule.class);
	
	//@Autowired
	//private OrderService orderService;
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		// TODO Auto-generated method stub
		logger.info("dennis test execute schedule start ");
		logger.info("==========================ReleaseQtyAndUpdateOrderStatusSchedule===========================");
		try {
			//orderService.releaseQtyAndUpdateOrderStatus();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("execute ReleaseQtyAndUpdateOrderStatus schedule error:"
			      + e.getMessage());
		}
		logger.info("dennis test exxcute schedule end");
	}

}

package com.fans.admin.quartz.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.quartz.entity.ScheduleLog;
import com.fans.admin.quartz.service.IScheduleLogService;
import com.fans.admin.quartz.warpper.ScheduleLogWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 定时任务执行日志表数据接口
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月14 16:55:11
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/quartz/scheduleLog")
public class ScheduleLogController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(ScheduleLogController.class);

    @Autowired
    private IScheduleLogService scheduleLogService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取定时任务执行日志表
     * @author: fanhaohao
     * @date 2019年01月14 16:55:11
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		ScheduleLog scheduleLog = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				scheduleLog = scheduleLogService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取定时任务执行日志表异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取定时任务执行日志表异常", e);
			return renderString(response, FansResp.error("根据id获取定时任务执行日志表异常！"));
		}
		return renderString(response, FansResp.successData(scheduleLog));
	}

	/**
	 * @Title：list  
	 * @Description: 获取定时任务执行日志表列表
	 * @author: fanhaohao
	 * @date 2019年01月14 16:55:11
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("scheduleLog:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(ScheduleLog scheduleLog, PageVo pv, String condition, HttpServletResponse response) {
		Page<ScheduleLog> page = new PageFactory<ScheduleLog>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(scheduleLog);
		map.put("condition", condition);
		List<Map<String, Object>> result = scheduleLogService.list(page, map);
		page.setRecords((List<ScheduleLog>) new ScheduleLogWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：create  
	 * @Description: 新建定时任务执行日志表
	 * @author: fanhaohao
	 * @date 2019年01月14 16:55:11
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建定时任务执行日志表")
	@RequiredPermission("scheduleLog:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(ScheduleLog scheduleLog, HttpServletRequest request, HttpServletResponse response) {
		try {
			scheduleLog.preInsert();
			scheduleLog.setCreateBy(UserUtil.getUserId());
			scheduleLogService.insert(scheduleLog);
		} catch (Exception e) {
			logger.error("新建定时任务执行日志表异常", e);
			return renderString(response, FansResp.error("新建定时任务执行日志表异常！"));
		}
		return renderString(response, FansResp.successData(scheduleLog));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改定时任务执行日志表
	 * @author: fanhaohao
	 * @date 2019年01月14 16:55:11
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改定时任务执行日志表")
	@RequiredPermission("scheduleLog:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(ScheduleLog scheduleLog, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(scheduleLog.getId())) {
				scheduleLog.preUpdate();
				scheduleLog.setUpdateBy(UserUtil.getUserId());
				scheduleLogService.updateById(scheduleLog);
			}else{
				return renderString(response, FansResp.error("修改定时任务执行日志表异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改定时任务执行日志表异常", e);
			return renderString(response, FansResp.error("修改定时任务执行日志表异常！"));
		}
		return renderString(response, FansResp.successData(scheduleLog));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 删除定时任务执行日志表
	 * @author: fanhaohao
	 * @date 2019年01月14 16:55:11
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除定时任务执行日志表")
	@RequiredPermission("scheduleLog:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				scheduleLogService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除定时任务执行日志表异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除定时任务执行日志表异常", e);
			return renderString(response, FansResp.error("删除定时任务执行日志表异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	
}

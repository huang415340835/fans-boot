package com.fans.admin.quartz.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 
 * 
 * #============================================================================
# Configure ThreadPool  
#============================================================================
org.quartz.threadPool.class=org.quartz.simpl.SimpleThreadPool
org.quartz.threadPool.threadCount=10
org.quartz.threadPool.threadPriority=5
org.quartz.threadPool.threadsInheritContextClassLoaderOfInitializingThread=true
 * @author yubingfeng
 *
 */
@Component
@ConfigurationProperties(prefix="org.quartz.threadPool")
@PropertySource("classpath:quartz.properties")  
public class ThreadPoolProperties {
	
	private String threadPoolClass="org.quartz.simpl.SimpleThreadPool";
	private  String threadCount="10";
    private String threadPriority="5";
    private boolean threadsInheritContextClassLoaderOfInitializingThread=true;
	public String getThreadPoolClass() {
		return threadPoolClass;
	}
	public void setThreadPoolClass(String threadPoolClass) {
		this.threadPoolClass = threadPoolClass;
	}
	
	public boolean isThreadsInheritContextClassLoaderOfInitializingThread() {
		return threadsInheritContextClassLoaderOfInitializingThread;
	}
	public void setThreadsInheritContextClassLoaderOfInitializingThread(
			boolean threadsInheritContextClassLoaderOfInitializingThread) {
		this.threadsInheritContextClassLoaderOfInitializingThread = threadsInheritContextClassLoaderOfInitializingThread;
	}
	public String getThreadCount() {
		return threadCount;
	}
	public void setThreadCount(String threadCount) {
		this.threadCount = threadCount;
	}
	public String getThreadPriority() {
		return threadPriority;
	}
	public void setThreadPriority(String threadPriority) {
		this.threadPriority = threadPriority;
	}
    

}

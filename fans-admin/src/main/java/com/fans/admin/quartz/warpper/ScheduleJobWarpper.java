package com.fans.admin.quartz.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
 * @ClassName: ScheduleJobWarpper
 * @description: 定时任务表
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月14 16:55:11
 * @author: fanhaohao
 * @version: 1.0
 */
public class ScheduleJobWarpper extends BaseControllerWarpper {

	public ScheduleJobWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}

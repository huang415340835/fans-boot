package com.fans.admin.test.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.test.entity.TestDict;
import com.fans.admin.test.service.ITestDictService;
import com.fans.admin.test.warpper.TestDictWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 字典控制层
 * @date: 2019-06-25
 * @author: fanhaohao
 * @version: 1.0
 * @copyright: 版权所有 fans (c)2019 
 */
@Controller
@RequestMapping("/v1/test/testDict")
public class TestDictController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(TestDictController.class);

    @Autowired
    private ITestDictService testDictService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取字典
     * @author: fanhaohao
     * @date 2019-06-25
     * @param id
     * @param request
     * @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		TestDict testDict = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				testDict = testDictService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取字典异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取字典异常", e);
			return renderString(response, FansResp.error("根据id获取字典异常！"));
		}
		return renderString(response, FansResp.successData(testDict));
	}

	/**
	 * @Title：list  
	 * @Description: 获取字典列表
	 * @author: fanhaohao
	 * @date 2019-06-25
	 * @param condition
	 * @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(TestDict testDict, PageVo pv, String condition, HttpServletResponse response) {
		Page<TestDict> page = new PageFactory<TestDict>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(testDict);
		map.put("condition", condition);
		List<Map<String, Object>> result = testDictService.list(page, map);
		page.setRecords((List<TestDict>) new TestDictWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：create  
	 * @Description: 新建字典
	 * @author: fanhaohao
	 * @date 2019-06-25
	 * @param sysMenu
	 * @param request
	 * @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(TestDict testDict, HttpServletRequest request, HttpServletResponse response) {
		try {
			testDict.preInsert();
			testDict.setCreateBy(UserUtil.getUserId());
			testDictService.insert(testDict);
		} catch (Exception e) {
			logger.error("新建字典异常", e);
			return renderString(response, FansResp.error("新建字典异常！"));
		}
		return renderString(response, FansResp.successData(testDict));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改字典
	 * @author: fanhaohao
	 * @date 2019-06-25
	 * @param sysMenu
	 * @param request
	 * @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(TestDict testDict, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(testDict.getId())) {
				testDict.preUpdate();
				testDict.setUpdateBy(UserUtil.getUserId());
				testDictService.updateById(testDict);
			}else{
				return renderString(response, FansResp.error("修改字典异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改字典异常", e);
			return renderString(response, FansResp.error("修改字典异常！"));
		}
		return renderString(response, FansResp.successData(testDict));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 删除字典
	 * @author: fanhaohao
	 * @date 2019-06-25
	 * @param ids
	 * @param request
	 * @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				testDictService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除字典异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除字典异常", e);
			return renderString(response, FansResp.error("删除字典异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	
}

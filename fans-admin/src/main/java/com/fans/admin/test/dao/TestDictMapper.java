package com.fans.admin.test.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.test.entity.TestDict;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * @description: 字典Mapper接口
 * @date: 2019-06-25
 * @author: fanhaohao
 * @version: 1.0
 * @copyright: 版权所有 fans (c)2019 
 */ 
public interface TestDictMapper extends BaseMapper<TestDict> {

	/**
	 * @Title：list  
	 * @Description: 字典分页查询
	 * @author: fanhaohao
	 * @date 2019-06-25
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<TestDict> page, Map<String, Object> map);

}
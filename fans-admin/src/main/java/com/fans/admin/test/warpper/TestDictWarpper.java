package com.fans.admin.test.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
  * @description: 字典Warpper
  * @date: 2019-06-25
  * @author: fanhaohao
  * @version: 1.0
  * @copyright: 版权所有 fans (c)2019 
  */ 
public class TestDictWarpper extends BaseControllerWarpper {

	public TestDictWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}

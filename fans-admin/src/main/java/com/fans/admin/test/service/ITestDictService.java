package com.fans.admin.test.service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.test.entity.TestDict;
import com.baomidou.mybatisplus.service.IService;
 

/**
 * @description: 字典服务类
 * @date: 2019-06-25
 * @author: fanhaohao
 * @version: 1.0
 * @copyright: 版权所有 fans (c)2019 
 */
public interface ITestDictService extends IService<TestDict> {

	/**
	 * @Title：list  
	 * @Description: 字典分页查询
	 * @author: fanhaohao
	 * @date 2019-06-25
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<TestDict> page,Map<String, Object> map);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 字典根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019-06-25
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 字典根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019-06-25
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);
}

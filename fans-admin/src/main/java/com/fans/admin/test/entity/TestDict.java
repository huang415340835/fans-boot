package com.fans.admin.test.entity;

import java.util.Date;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;

/**
 * @description: 字典实体类
 * @date: 2019-06-25
 * @author: fanhaohao
 * @version: 1.0
 * @copyright: 版权所有 fans (c)2019 
 */ 
@TableName(value="fans_test_dict",resultMap="TestDictResultMap")
public class TestDict extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
	@TableField("code")
    private String code;
    /**
     * 名称
     */
	@TableField("name")
    private String name;
    /**
     * 父节点
     */
	@TableField("parent_id")
    private Long parentId;
    /**
     * 排序
     */
	@TableField("weight")
    private Integer weight;



	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}


}

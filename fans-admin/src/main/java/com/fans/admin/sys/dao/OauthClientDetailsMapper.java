package com.fans.admin.sys.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.OauthClientDetails;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * @description: 客户端详情Mapper接口
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月11 15:27:01
 * @author: fanhaohao
 * @version: 1.0
 */
public interface OauthClientDetailsMapper extends BaseMapper<OauthClientDetails> {

	/**
	 * @Title：list  
	 * @Description: 客户端详情分页查询
	 * @author: fanhaohao
	 * @date 2019年01月11 15:27:01
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<OauthClientDetails> page, Map<String, Object> map);

}
package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.cache.SysMenuCache;
import com.fans.admin.sys.entity.SysMenu;
import com.fans.admin.sys.service.ISysMenuService;
import com.fans.admin.sys.warpper.SysMenuWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 系统_资源数据接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月25 09:57:55
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/sysMenu")
public class SysMenuController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysMenuController.class);

    @Autowired
    private ISysMenuService sysMenuService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取系统_资源
     * @author: fanhaohao
     * @date 2018年12月25 09:57:55
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		SysMenu sysMenu = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysMenu = sysMenuService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取系统_资源异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_资源异常", e);
			return renderString(response, FansResp.error("根据id获取系统_资源异常！"));
		}
		return renderString(response, FansResp.successData(sysMenu));
	}

	/**
	 * @Title：list  
	 * @Description: 获取系统_资源列表
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysMenu:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysMenu sysMenu, PageVo pv, String condition, HttpServletResponse response) {
		if (BlankUtils.isBlank(pv.getSortField())) {// 设置默认的排序方式
			pv.setSortField("weight");
			pv.setSortOrder("asc");
		}
		Page<SysMenu> page = new PageFactory<SysMenu>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(sysMenu);
		map.put("condition", condition);
		List<Map<String, Object>> result = sysMenuService.list(page, map);
		page.setRecords((List<SysMenu>) new SysMenuWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：listAll  
	 * @Description: 获取系统_资源列表
	 * @author: fanhaohao
	 * @date 2019年1月10日 上午9:14:00 
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequiredPermission("sysMenu:listAll")
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	public Object listAll(HttpServletResponse response) {
		Map<String, Object> datas = new HashMap<>();
		List<Map<String, Object>> result = SysMenuCache.getAllMenuList();
		datas.put("records", result);
		return renderString(response, FansResp.successData(datas));
	}

	/**
	 * @Title：create  
	 * @Description: 新建系统_资源
	 * @author: fanhaohao
	 * @date 2019年01月02 16:04:16
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建系统_资源")
	@RequiredPermission("sysMenu:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(SysMenu sysMenu, HttpServletRequest request, HttpServletResponse response) {
		try {
			sysMenu.preInsert();
			sysMenu.setCreateBy(UserUtil.getUserId());
			sysMenuService.insert(sysMenu);
		} catch (Exception e) {
			logger.error("新建系统_资源异常", e);
			return renderString(response, FansResp.error("新建系统_资源异常！"));
		}
		return renderString(response, FansResp.successData(sysMenu));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改系统_资源
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改系统_资源")
	@RequiredPermission("sysMenu:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(SysMenu sysMenu, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(sysMenu.getId())) {
				sysMenu.preUpdate();
				sysMenu.setUpdateBy(UserUtil.getUserId());
				sysMenuService.updateById(sysMenu);
			}else{
				return renderString(response, FansResp.error("修改系统_资源异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改系统_资源异常", e);
			return renderString(response, FansResp.error("修改系统_资源异常！"));
		}
		return renderString(response, FansResp.successData(sysMenu));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 系统_资源
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除系统_资源")
	@RequiredPermission("sysMenu:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysMenuService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除系统_资源异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除系统_资源异常", e);
			return renderString(response, FansResp.error("删除系统_资源异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	
}

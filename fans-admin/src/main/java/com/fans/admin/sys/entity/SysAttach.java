package com.fans.admin.sys.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;

/**
 * @description: 系统_附件
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月26 14:26:39
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="sys_attach",resultMap="BaseResultMap")
public class SysAttach extends BaseEntity<SysAttach> {

    private static final long serialVersionUID = 1L;

    /**
     * 路径
     */
    private String path;
    /**
     * 附件名
     */
    private String name;
    /**
     * 文件大小
     */
    private Long size;
    /**
     * 文件后缀
     */
    private String extention;
    /**
     * 引用计数
     */
    private Integer count;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getExtention() {
		return extention;
	}

	public void setExtention(String extention) {
		this.extention = extention;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}


}

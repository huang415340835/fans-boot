package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysAttach;
import com.fans.admin.sys.service.ISysAttachService;
import com.fans.admin.sys.warpper.SysAttachWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 系统_附件数据接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月26 14:26:39
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/sysAttach")
public class SysAttachController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysAttachController.class);

    @Autowired
    private ISysAttachService sysAttachService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取系统_附件
     * @author: fanhaohao
     * @date 2018年12月26 14:26:39
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		SysAttach sysAttach = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysAttach = sysAttachService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取系统_附件异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_附件异常", e);
			return renderString(response, FansResp.error("根据id获取系统_附件异常！"));
		}
		return renderString(response, FansResp.successData(sysAttach));
	}

	/**
	 * @Title：list  
	 * @Description: 获取系统_附件列表
	 * @author: fanhaohao
	 * @date 2018年12月26 14:26:39
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysAttach:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysAttach sysAttach, PageVo pv, String condition, HttpServletResponse response) {
		Page<SysAttach> page = new PageFactory<SysAttach>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(sysAttach);
		map.put("condition", condition);
		List<Map<String, Object>> result = sysAttachService.list(page, map);
		page.setRecords((List<SysAttach>) new SysAttachWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：create  
	 * @Description: 新建系统_附件
	 * @author: fanhaohao
	 * @date 2019年01月02 16:04:16
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建系统_附件")
	@RequiredPermission("sysAttach:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(SysAttach sysAttach, HttpServletRequest request, HttpServletResponse response) {
		try {
			sysAttach.preInsert();
			sysAttach.setCreateBy(UserUtil.getUserId());
			sysAttachService.insert(sysAttach);
		} catch (Exception e) {
			logger.error("新建系统_附件异常", e);
			return renderString(response, FansResp.error("新建系统_附件异常！"));
		}
		return renderString(response, FansResp.successData(sysAttach));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改系统_附件
	 * @author: fanhaohao
	 * @date 2018年12月26 14:26:39
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改系统_附件")
	@RequiredPermission("sysAttach:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(SysAttach sysAttach, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(sysAttach.getId())) {
				sysAttach.preUpdate();
				sysAttach.setUpdateBy(UserUtil.getUserId());
				sysAttachService.updateById(sysAttach);
			}else{
				return renderString(response, FansResp.error("修改系统_附件异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改系统_附件异常", e);
			return renderString(response, FansResp.error("修改系统_附件异常！"));
		}
		return renderString(response, FansResp.successData(sysAttach));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 系统_附件
	 * @author: fanhaohao
	 * @date 2018年12月26 14:26:39
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除系统_附件")
	@RequiredPermission("sysAttach:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysAttachService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除系统_附件异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除系统_附件异常", e);
			return renderString(response, FansResp.error("删除系统_附件异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
}

package com.fans.admin.sys.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import com.fans.admin.sys.entity.SysRole;
import com.fans.admin.sys.entity.SysUser;
import com.fans.admin.sys.service.IAuthService;
import com.fans.admin.sys.service.ISysLoginLogService;
import com.fans.admin.sys.service.ISysRoleMenuService;
import com.fans.admin.sys.service.ISysUserRoleService;
import com.fans.admin.sys.service.ISysUserService;
import com.fans.admin.sys.vo.RoleMenuPerms;
import com.fans.common.config.redis.util.JedisUtils;
import com.fans.common.constant.SysConstant;
import com.fans.oauths.oauth2.util.TokenUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;

@Service
public class AuthServiceImpl implements IAuthService {
    private static Logger logger = LoggerFactory.getLogger(AuthServiceImpl.class);

	@Autowired
	private ISysUserService sysUserService;
	@Autowired
	private ISysUserRoleService sysUserRoleService;
	@Autowired
	private ISysRoleMenuService sysRoleMenuService;
	@Autowired
	private ISysLoginLogService sysLoginLogService;

	/**
	 * @Title：getLoginDatas  
	 * @Description: 获取登录后的信息
	 * @author: fanhaohao
	 * @date 2018年12月20日 下午2:06:23 
	 * @param @param username
	 * @param @param password
	 * @param @param menuPerms
	 * @param @return 
	 * @return Map<String, Object> 
	 * @throws
	 */
    @Override
	public Map<String, Object> getLoginDatas(String username, boolean menuPerms) {
		SysUser sysUser = sysUserService.selectByUsername(username);
		// 返回信息
		Map<String, Object> rtDatas = new HashMap<>();
		// 用户信息
		Map<String, Object> userData = new HashMap<>();
		userData.put("userName", sysUser.getUsername());
		userData.put("id", sysUser.getId());
		// 获得角色和角色权限
		List<SysRole> sysRoleList = sysUserRoleService.listByUserId(sysUser.getId(), null);
		if (BlankUtils.isNotBlank(sysRoleList)) {
			int roleSize = sysRoleList.size();
			Set<String> roleIds = new HashSet<>(roleSize);
            List<String> roleNames = menuPerms ? new ArrayList<>(roleSize) : null;
            List<String> mains = menuPerms ? new ArrayList<>(roleSize) : null;
			for (SysRole r : sysRoleList) {
				if (BlankUtils.isNotBlank(r)) {
                    roleIds.add(r.getId());
                    if (menuPerms) {
                        roleNames.add(r.getName());
                        mains.add(r.getMain());
                    }
                }
            }
            // 是否获取菜单权限
            if (menuPerms) {
                userData.put("roleName", roleNames.toString());
                userData.put("main", mains.toString());
				RoleMenuPerms roleMenuPerms = sysRoleMenuService.getRoleMenuAndPerms(coverRoleIds(roleIds));
				rtDatas.put("menuData", roleMenuPerms.getMenuList());
				rtDatas.put("permsData", roleMenuPerms.getPermsList());
				// 登录后把按钮权限标识保存到redis缓存中
				JedisUtils.setObject(SysConstant.PERMS_MENU_DATA + sysUser.getId(), roleMenuPerms.getPermsList(), 0);
            }
        }
		rtDatas.put("userData", userData);
		sysUserService.updateLoginResult(sysUser.getUsername(), true);// 更新登录成功次数
		return rtDatas;
    }


	/**
	 * @Title：coverRoleIds  
	 * @Description: roleids 拼接成字符串 ‘id1’，‘id2’的格式
	 * @author: fanhaohao
	 * @date 2018年12月26日 下午5:40:00 
	 * @param @param roleIdsroleIds
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	private String coverRoleIds(Set<String> roleIdsroleIds) {
		StringBuffer sb = new StringBuffer();
		for (String roleId : roleIdsroleIds) {
			sb.append("'").append(roleId).append("',");
		}
		String rt = sb.toString();
		if (rt.contains(",")) {
			rt = rt.substring(0, rt.lastIndexOf(","));
		}
		return rt;
	}

    
    /**
	 * @Title：logout  
	 * @Description: 退出登录
	 * @author: fanhaohao
	 * @date 2018年12月20日 下午2:18:26 
	 * @param @param request
	 * @param @param username
	 * @param @param tokenStore
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
    @Override
	public FansResp logout(HttpServletRequest request, String username, TokenStore tokenStore) {
    	String tokenValue = TokenUtil.getToken();
		if (BlankUtils.isNotBlank(tokenValue)) {
			OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
			if (BlankUtils.isNotBlank(accessToken)) {
				OAuth2RefreshToken refreshToken = accessToken.getRefreshToken();
				if (BlankUtils.isNotBlank(refreshToken)) {
					tokenStore.removeRefreshToken(refreshToken);// 删除refresh相关信息
				}
				tokenStore.removeAccessToken(accessToken);// 删除token相关信息
				sysLoginLogService.saveLoginLog(request, username, "2", "1", "退出成功！");
				return FansResp.success();
			}
		}
		sysLoginLogService.saveLoginLog(request, username, "2", "2", "退出成功！");
		return FansResp.error();
    }


}

package com.fans.admin.sys.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;
import com.fans.utils.utils.BlankUtils;

 /**
 * @ClassName: SysUserWarpper
 * @description: 系统_用户
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月19 14:53:59
 * @author: fanhaohao
 * @version: 1.0
 */
public class SysUserWarpper extends BaseControllerWarpper {

	public SysUserWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		if (BlankUtils.isNotBlank(map.get("isLock"))) {
			if ("0".equals(map.get("isLock"))) {
				map.put("lockTime", "");
			}
		}
		
	}

}

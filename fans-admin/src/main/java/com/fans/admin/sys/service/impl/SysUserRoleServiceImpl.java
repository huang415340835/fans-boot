package com.fans.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fans.admin.sys.dao.SysUserRoleMapper;
import com.fans.admin.sys.entity.SysRole;
import com.fans.admin.sys.entity.SysUserRole;
import com.fans.admin.sys.service.ISysUserRoleService;
import com.fans.oauths.oauth2.util.UserUtil;

/**
 * @description: 系统_用户角色服务实现类
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月19 15:26:08
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper,SysUserRole> implements ISysUserRoleService {

	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;
    
	/**
	 * @Title：list  
	 * @Description: 系统_用户角色分页查询
	 * @author: fanhaohao
	 * @date 2018年12月19 15:26:08
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<SysUserRole> page,Map<String, Object> map){
		return sysUserRoleMapper.list(page, map);
	}

	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 系统_用户角色根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:31:34
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdLogic(Object id){
		SysUserRole sysUserRole = new SysUserRole();
		sysUserRole.setId(String.valueOf(id));
		sysUserRole.preUpdate();
		sysUserRole.setUpdateBy(UserUtil.getUserId());
		sysUserRole.setDelFlag("1");
		this.updateById(sysUserRole);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 系统_用户角色逻辑批量删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:31:34
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<SysUserRole> list = new ArrayList<>();
		for (Object id : ids) {
			SysUserRole sysUserRole = new SysUserRole();
			sysUserRole.setId(String.valueOf(id));
			sysUserRole.preUpdate();
			sysUserRole.setUpdateBy(UserUtil.getUserId());
			sysUserRole.setDelFlag("1");
			list.add(sysUserRole);
		}
		this.updateBatchById(list);
	}
	
	/**
	 * @Title：listByUserId  
	 * @Description: 根据用户id和角色id来获取角色信息
	 * @author: fanhaohao
	 * @date 2018年12月20日 下午2:01:37 
	 * @param @param userId
	 * @param @param roleId
	 * @param @return 
	 * @return List<SysRole> 
	 * @throws
	 */
	@Override
	public List<SysRole> listByUserId(String userId, String roleId) {
		return sysUserRoleMapper.listByUserId(userId, roleId);
	}

	/**
	 * roleIdListByUserId  
	 * @Description: 根据用户id来获取用户所拥有的角色id
	 * @author: fanhaohao
	 * @date 2018年12月20日 下午2:01:37 
	 * @param @param userId
	 * @param @return 
	 * @return List<SysRole> 
	 * @throws
	 */
	@Override
	public List<String> roleIdListByUserId(String userId) {
		return sysUserRoleMapper.roleIdListByUserId(userId);
	}

	/**
	 * @Title：deleteBatchByRoleIds  
	 * @Description: 根据roleid list集合来删除
	 * @author: fanhaohao
	 * @date 2018年12月27日 下午2:05:07 
	 * @param @param roleIdList
	 * @param @param userId 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByRoleIds(List<String> roleIdList, String userId) {
		sysUserRoleMapper.deleteBatchByRoleIds(roleIdList, userId);
	}
}

package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysLog;
import com.fans.admin.sys.service.ISysLogService;
import com.fans.admin.sys.warpper.SysLogWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 数据接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月28 09:27:28
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/sysLog")
public class SysLogController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysLogController.class);

    @Autowired
    private ISysLogService sysLogService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取
     * @author: fanhaohao
     * @date 2018年12月28 09:27:28
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		SysLog sysLog = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysLog = sysLogService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取异常", e);
			return renderString(response, FansResp.error("根据id获取异常！"));
		}
		return renderString(response, FansResp.successData(sysLog));
	}

	/**
	 * @Title：list  
	 * @Description: 获取列表
	 * @author: fanhaohao
	 * @date 2018年12月28 09:27:28
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysLog:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysLog sysLog, PageVo pv, String condition, HttpServletResponse response) {
		Page<SysLog> page = new PageFactory<SysLog>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(sysLog);
		map.put("condition", condition);
		List<Map<String, Object>> result = sysLogService.list(page, map);
		page.setRecords((List<SysLog>) new SysLogWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：save  
	 * @Description: 新建
	 * @author: fanhaohao
	 * @date 2018年12月28 09:27:28
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建")
	@RequiredPermission("sysLog:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(SysLog sysLog, HttpServletRequest request, HttpServletResponse response) {
		try {
			sysLog.preInsert();
			sysLog.setCreateBy(UserUtil.getUserId());
			sysLogService.insert(sysLog);
		} catch (Exception e) {
			logger.error("新建异常", e);
			return renderString(response, FansResp.error("新建异常！"));
		}
		return renderString(response, FansResp.successData(sysLog));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改
	 * @author: fanhaohao
	 * @date 2018年12月28 09:27:28
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改")
	@RequiredPermission("sysLog:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(SysLog sysLog, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(sysLog.getId())) {
				sysLog.preUpdate();
				sysLog.setUpdateBy(UserUtil.getUserId());
				sysLogService.updateById(sysLog);
			}else{
				return renderString(response, FansResp.error("修改异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改异常", e);
			return renderString(response, FansResp.error("修改异常！"));
		}
		return renderString(response, FansResp.successData(sysLog));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 
	 * @author: fanhaohao
	 * @date 2018年12月28 09:27:28
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequiredPermission("sysLog:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysLogService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除异常", e);
			return renderString(response, FansResp.error("删除异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	/**
	 * @Title：deleteAll  
	 * @Description: 清除日志
	 * @author: fanhaohao
	 * @date 2018年12月28 09:27:28
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequiredPermission("sysLog:deleteAll")
	@RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE)
	public String deleteAll(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<String> allIds = sysLogService.getAllIds();// 获取所有的日志id
			// sysLogService.deleteBatchByIdsLogic(allIds);
			sysLogService.deleteBatchIds(allIds);
		} catch (Exception e) {
			logger.error("清除日志异常", e);
			return renderString(response, FansResp.error("清除日志异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	
}

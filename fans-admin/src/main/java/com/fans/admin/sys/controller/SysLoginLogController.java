package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysLoginLog;
import com.fans.admin.sys.service.ISysLoginLogService;
import com.fans.admin.sys.warpper.SysLoginLogWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 用户登录日志数据接口
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月10 16:53:50
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/sysLoginLog")
public class SysLoginLogController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysLoginLogController.class);

    @Autowired
    private ISysLoginLogService sysLoginLogService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取用户登录日志
     * @author: fanhaohao
     * @date 2019年01月10 16:53:50
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		SysLoginLog sysLoginLog = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysLoginLog = sysLoginLogService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取用户登录日志异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取用户登录日志异常", e);
			return renderString(response, FansResp.error("根据id获取用户登录日志异常！"));
		}
		return renderString(response, FansResp.successData(sysLoginLog));
	}

	/**
	 * @Title：list  
	 * @Description: 获取用户登录日志列表
	 * @author: fanhaohao
	 * @date 2019年01月10 16:53:50
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysLoginLog:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysLoginLog sysLoginLog, PageVo pv, String condition, HttpServletResponse response) {
		Page<SysLoginLog> page = new PageFactory<SysLoginLog>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(sysLoginLog);
		map.put("condition", condition);
		List<Map<String, Object>> result = sysLoginLogService.list(page, map);
		page.setRecords((List<SysLoginLog>) new SysLoginLogWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：create  
	 * @Description: 新建用户登录日志
	 * @author: fanhaohao
	 * @date 2019年01月10 16:53:50
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建用户登录日志")
	@RequiredPermission("sysLoginLog:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(SysLoginLog sysLoginLog, HttpServletRequest request, HttpServletResponse response) {
		try {
			sysLoginLog.preInsert();
			sysLoginLog.setCreateBy(UserUtil.getUserId());
			sysLoginLogService.insert(sysLoginLog);
		} catch (Exception e) {
			logger.error("新建用户登录日志异常", e);
			return renderString(response, FansResp.error("新建用户登录日志异常！"));
		}
		return renderString(response, FansResp.successData(sysLoginLog));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改用户登录日志
	 * @author: fanhaohao
	 * @date 2019年01月10 16:53:50
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改用户登录日志")
	@RequiredPermission("sysLoginLog:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(SysLoginLog sysLoginLog, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(sysLoginLog.getId())) {
				sysLoginLog.preUpdate();
				sysLoginLog.setUpdateBy(UserUtil.getUserId());
				sysLoginLogService.updateById(sysLoginLog);
			}else{
				return renderString(response, FansResp.error("修改用户登录日志异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改用户登录日志异常", e);
			return renderString(response, FansResp.error("修改用户登录日志异常！"));
		}
		return renderString(response, FansResp.successData(sysLoginLog));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 删除用户登录日志
	 * @author: fanhaohao
	 * @date 2019年01月10 16:53:50
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除用户登录日志")
	@RequiredPermission("sysLoginLog:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysLoginLogService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除用户登录日志异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除用户登录日志异常", e);
			return renderString(response, FansResp.error("删除用户登录日志异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	/**
	 * @Title：deleteAll  
	 * @Description: 清除日志
	 * @author: fanhaohao
	 * @date 2018年12月28 09:27:28
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequiredPermission("sysLoginLog:deleteAll")
	@RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE)
	public String deleteAll(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<String> allIds = sysLoginLogService.getAllIds();// 获取所有的日志id
			// sysLogService.deleteBatchByIdsLogic(allIds);
			sysLoginLogService.deleteBatchIds(allIds);
		} catch (Exception e) {
			logger.error("清除日志异常", e);
			return renderString(response, FansResp.error("清除日志异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
}

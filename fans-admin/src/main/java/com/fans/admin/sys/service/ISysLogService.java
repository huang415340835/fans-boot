package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.sys.entity.SysLog;
 
/**
 * @description: 服务类
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月28 09:27:28
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ISysLogService extends IService<SysLog> {

	/**
	 * @Title：list  
	 * @Description: 分页查询
	 * @author: fanhaohao
	 * @date 2018年12月28 09:27:28
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<SysLog> page,Map<String, Object> map);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2018年12月28 09:27:28
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2018年12月28 09:27:28
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<String> ids);
	
	/**
	 * @Title：getAllIds  
	 * @Description: 获取所有的日志id
	 * @author: fanhaohao
	 * @date 2019年1月3日 下午4:42:24 
	 * @param @return 
	 * @return List<String> 
	 * @throws
	 */
	public List<String> getAllIds();
}

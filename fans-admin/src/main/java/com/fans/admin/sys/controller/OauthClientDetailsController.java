package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.OauthClientDetails;
import com.fans.admin.sys.service.IOauthClientDetailsService;
import com.fans.admin.sys.warpper.OauthClientDetailsWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 客户端详情数据接口
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月11 15:27:01
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/oauthClientDetails")
public class OauthClientDetailsController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(OauthClientDetailsController.class);

    @Autowired
    private IOauthClientDetailsService oauthClientDetailsService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取客户端详情
     * @author: fanhaohao
     * @date 2019年01月11 15:27:01
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		OauthClientDetails oauthClientDetails = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				oauthClientDetails = oauthClientDetailsService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取客户端详情异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取客户端详情异常", e);
			return renderString(response, FansResp.error("根据id获取客户端详情异常！"));
		}
		return renderString(response, FansResp.successData(oauthClientDetails));
	}

	/**
	 * @Title：list  
	 * @Description: 获取客户端详情列表
	 * @author: fanhaohao
	 * @date 2019年01月11 15:27:01
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("oauthClientDetails:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(OauthClientDetails oauthClientDetails, PageVo pv, String condition, HttpServletResponse response) {
		Page<OauthClientDetails> page = new PageFactory<OauthClientDetails>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(oauthClientDetails);
		map.put("condition", condition);
		List<Map<String, Object>> result = oauthClientDetailsService.list(page, map);
		page.setRecords((List<OauthClientDetails>) new OauthClientDetailsWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：create  
	 * @Description: 新建客户端详情
	 * @author: fanhaohao
	 * @date 2019年01月11 15:27:01
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建客户端详情")
	@RequiredPermission("oauthClientDetails:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(OauthClientDetails oauthClientDetails, HttpServletRequest request, HttpServletResponse response) {
		try {
			oauthClientDetails.preInsert();
			oauthClientDetails.setCreateBy(UserUtil.getUserId());
			oauthClientDetailsService.insert(oauthClientDetails);
		} catch (Exception e) {
			logger.error("新建客户端详情异常", e);
			return renderString(response, FansResp.error("新建客户端详情异常！"));
		}
		return renderString(response, FansResp.successData(oauthClientDetails));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改客户端详情
	 * @author: fanhaohao
	 * @date 2019年01月11 15:27:01
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改客户端详情")
	@RequiredPermission("oauthClientDetails:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(OauthClientDetails oauthClientDetails, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(oauthClientDetails.getId())) {
				oauthClientDetails.preUpdate();
				oauthClientDetails.setUpdateBy(UserUtil.getUserId());
				oauthClientDetailsService.updateById(oauthClientDetails);
			}else{
				return renderString(response, FansResp.error("修改客户端详情异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改客户端详情异常", e);
			return renderString(response, FansResp.error("修改客户端详情异常！"));
		}
		return renderString(response, FansResp.successData(oauthClientDetails));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 删除客户端详情
	 * @author: fanhaohao
	 * @date 2019年01月11 15:27:01
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除客户端详情")
	@RequiredPermission("oauthClientDetails:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				oauthClientDetailsService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除客户端详情异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除客户端详情异常", e);
			return renderString(response, FansResp.error("删除客户端详情异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	
}

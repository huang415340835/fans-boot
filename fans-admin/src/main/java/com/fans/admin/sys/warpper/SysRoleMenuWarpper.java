package com.fans.admin.sys.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
 * @ClassName: SysRoleMenuWarpper
 * @description: 系统_角色资源
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月19 14:53:59
 * @author: fanhaohao
 * @version: 1.0
 */
public class SysRoleMenuWarpper extends BaseControllerWarpper {

	public SysRoleMenuWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}

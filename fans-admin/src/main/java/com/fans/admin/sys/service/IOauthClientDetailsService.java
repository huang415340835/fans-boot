package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.OauthClientDetails;
import com.baomidou.mybatisplus.service.IService;
 
/**
 * @description: 客户端详情服务类
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月11 15:27:01
 * @author: fanhaohao
 * @version: 1.0
 */
public interface IOauthClientDetailsService extends IService<OauthClientDetails> {

	/**
	 * @Title：list  
	 * @Description: 客户端详情分页查询
	 * @author: fanhaohao
	 * @date 2019年01月11 15:27:01
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<OauthClientDetails> page,Map<String, Object> map);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 客户端详情根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年01月11 15:27:01
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 客户端详情根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年01月11 15:27:01
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);
	
	/**
	 * @Title：listAll  
	 * @Description: 查询所有
	 * @author: fanhaohao
	 * @date 2019年1月10日 上午9:02:05 
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> listAll();
}

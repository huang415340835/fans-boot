package com.fans.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import com.fans.admin.sys.entity.OauthClientDetails;
import com.fans.admin.sys.entity.SysRole;
import com.fans.admin.sys.dao.OauthClientDetailsMapper;
import com.fans.admin.sys.service.IOauthClientDetailsService;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fans.oauths.oauth2.util.UserUtil;

/**
 * @description: 客户端详情服务实现类
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月11 15:27:01
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class OauthClientDetailsServiceImpl extends ServiceImpl<OauthClientDetailsMapper,OauthClientDetails> implements IOauthClientDetailsService {

	@Autowired
    private OauthClientDetailsMapper oauthClientDetailsMapper;
    
	/**
	 * @Title：list  
	 * @Description: 客户端详情分页查询
	 * @author: fanhaohao
	 * @date 2019年01月11 15:27:01
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<OauthClientDetails> page,Map<String, Object> map){
		return oauthClientDetailsMapper.list(page,map);
	}
	
	/**
	 * @Title：listAll  
	 * @Description: 查询所有
	 * @author: fanhaohao
	 * @date 2019年1月10日 上午9:02:05 
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> listAll() {
		PageVo pv = new PageVo();
		Page<OauthClientDetails> page = new PageFactory<OauthClientDetails>().page(pv);
		return oauthClientDetailsMapper.list(page, null);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 客户端详情根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2019年01月11 15:27:01
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdLogic(Object id){
		OauthClientDetails oauthClientDetails = new OauthClientDetails();
		oauthClientDetails.setId(String.valueOf(id));
		oauthClientDetails.preUpdate();
		oauthClientDetails.setUpdateBy(UserUtil.getUserId());
		oauthClientDetails.setDelFlag("1");
		this.updateById(oauthClientDetails);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 客户端详情逻辑批量删除
	 * @author: fanhaohao
	 * @date 2019年01月11 15:27:01
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<OauthClientDetails> list = new ArrayList<>();
		for (Object id : ids) {
			OauthClientDetails oauthClientDetails = new OauthClientDetails();
			oauthClientDetails.setId(String.valueOf(id));
			oauthClientDetails.preUpdate();
			oauthClientDetails.setUpdateBy(UserUtil.getUserId());
			oauthClientDetails.setDelFlag("1");
			list.add(oauthClientDetails);
		}
		this.updateBatchById(list);
	}
}

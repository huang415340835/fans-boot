package com.fans.admin.sys.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fans.admin.sys.service.ISysDictService;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.SpringUtil;
/**
 * @ClassName: SysDictCache  
 * @Description: 字典缓存类 
 * @author fanhaohao
 * @date 2019年1月8日 上午11:00:03
 */
public class SysDictCache {
	
	/**
	 * @Title：getDictList  
	 * @Description: 根据code获取字典缓存list
	 * @author: fanhaohao
	 * @date 2019年1月8日 上午11:01:44 
	 * @param @param code
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public static List<Map<String, Object>> getDictList(String code){
		ISysDictService sysDictService = SpringUtil.getBean(ISysDictService.class);
		return sysDictService.selectDictListBycode(code);
	}
	
	/**
	 * @Title：getDictMap  
	 * @Description: 根据code获取字典缓存Map
	 * @author: fanhaohao
	 * @date 2019年1月8日 上午11:03:20 
	 * @param @param code
	 * @param @return 
	 * @return Map<String,Object> 
	 * @throws
	 */
	public static Map<String, Object> getDictMap(String code){
		Map<String, Object> rt = new HashMap<String, Object>();
		List<Map<String, Object>> dictList = getDictList(code);
		if (BlankUtils.isNotBlank(dictList)) {
			for (Map<String, Object> dl : dictList) {
				String itemCode = String.valueOf(dl.get("itemCode"));
				String itemValue = String.valueOf(dl.get("itemValue"));
				rt.put(itemCode, itemValue);
			}
		}
		return rt;
	}
}

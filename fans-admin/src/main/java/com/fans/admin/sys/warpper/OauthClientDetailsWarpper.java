package com.fans.admin.sys.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
 * @ClassName: OauthClientDetailsWarpper
 * @description: 客户端详情
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月11 15:27:01
 * @author: fanhaohao
 * @version: 1.0
 */
public class OauthClientDetailsWarpper extends BaseControllerWarpper {

	public OauthClientDetailsWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}

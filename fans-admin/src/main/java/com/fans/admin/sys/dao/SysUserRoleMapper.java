package com.fans.admin.sys.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysRole;
import com.fans.admin.sys.entity.SysUserRole;

/**
 * @description: 系统_用户角色Mapper接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月19 15:26:08
 * @author: fanhaohao
 * @version: 1.0
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

	/**
	 * @Title：list  
	 * @Description: 系统_用户角色分页查询
	 * @author: fanhaohao
	 * @date 2018年12月19 15:26:08
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<SysUserRole> page, Map<String, Object> map);
	
	/**
	 * @Title：listByUserId  
	 * @Description: 根据用户id获得角色列表
	 * @author: fanhaohao
	 * @date 2018年12月20日 下午2:03:32 
	 * @param @param userId
	 * @param @param roleId
	 * @param @return 
	 * @return List<SysRole> 
	 * @throws
	 */
	public List<SysRole> listByUserId(@Param("userId") String userId, @Param("roleId") String roleId);
	
	/**
	 * roleIdListByUserId  
	 * @Description: 根据用户id来获取用户所拥有的角色id
	 * @author: fanhaohao
	 * @date 2018年12月20日 下午2:01:37 
	 * @param @param userId
	 * @param @return 
	 * @return List<SysRole> 
	 * @throws
	 */
	public List<String> roleIdListByUserId(@Param("userId") String userId);
	
	/**
	 * @Title：deleteBatchByRoleIds  
	 * @Description: 根据roleid list集合来删除
	 * @author: fanhaohao
	 * @date 2018年12月27日 下午2:05:07 
	 * @param @param roleIdList
	 * @param @param userId 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByRoleIds(@Param("roleIdList") List<String> roleIdList, @Param("userId") String userId);

}
package com.fans.admin.sys.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysUser;
import com.fans.admin.sys.service.IOauthClientDetailsService;
import com.fans.admin.sys.service.ISysLoginLogService;
import com.fans.admin.sys.service.ISysUserService;
import com.fans.admin.sys.warpper.SysUserWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 系统_用户数据接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月24 16:17:35
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/sysOauthUsers")
public class SysOauthUsersController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysOauthUsersController.class);

    @Autowired
    private ISysUserService sysUserService;
    
    @Autowired
    private IOauthClientDetailsService oauthClientDetailsService;

	@Autowired
	private TokenStore tokenStore;
	
	@Autowired
	private ISysLoginLogService sysLoginLogService;
	
	/**
	 * @throws Exception 
	 * @Title：list
	 * @Description: 获取系统_在线用户列表
	 * @author: fanhaohao
	 * @date 2018年12月24 16:17:35
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysOauthUsers:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysUser sysUser, PageVo pv, String condition, HttpServletResponse response) {
		List<Map<String, Object>> oauthClientDetailsListAll = oauthClientDetailsService.listAll();
		List<String> usernameList = new ArrayList<>();
		if(BlankUtils.isNotBlank(oauthClientDetailsListAll)){
			for(Map<String, Object> ocdla:oauthClientDetailsListAll){
				String clientId=String.valueOf(ocdla.get("clientId"));
				Collection<OAuth2AccessToken> oAuth2AccessTokenC = tokenStore.findTokensByClientId(clientId);
				for (OAuth2AccessToken oat : oAuth2AccessTokenC) {
					OAuth2Authentication authentication = tokenStore.readAuthentication(oat.getValue());
					if (BlankUtils.isNotBlank(authentication)) {
						usernameList.add(authentication.getName());
					}
				}
			}
		}
		List<Map<String, Object>> result=new ArrayList<>();
		Page<SysUser> page = new PageFactory<SysUser>().page(pv);
		if(BlankUtils.isNotBlank(usernameList)){
			Map<String, Object> map = ObjectUtils.javaBean2Map(sysUser);
			map.put("usernameList", usernameList);
			result = sysUserService.list(page, map);
		}
		page.setRecords((List<SysUser>) new SysUserWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}
	
	/**
	 * @throws Exception 
	 * @Title：listByUsername
	 * @Description: 获取系统_在线用户所拥有的认证列表
	 * @author: fanhaohao
	 * @date 2018年12月24 16:17:35
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysOauthUsers:listByUsername")
	@RequestMapping(value = "/listByUsername", method = RequestMethod.GET)
	public Object listByUsername(String username, HttpServletResponse response) {
		List<Map<String, Object>> rtList=new ArrayList<>();
		if(BlankUtils.isNotBlank(username)){
			List<Map<String, Object>> oauthClientDetailsListAll = oauthClientDetailsService.listAll();
			if(BlankUtils.isNotBlank(oauthClientDetailsListAll)){
				for(Map<String, Object> ocdla:oauthClientDetailsListAll){
					String clientId=String.valueOf(ocdla.get("clientId"));
					Collection<OAuth2AccessToken> oAuth2AccessTokenC = tokenStore.findTokensByClientIdAndUserName(clientId, username);
					if(BlankUtils.isNotBlank(oAuth2AccessTokenC)){
						for (OAuth2AccessToken oat : oAuth2AccessTokenC) {
							OAuth2Authentication authentication = tokenStore.readAuthentication(oat);
							if (BlankUtils.isNotBlank(authentication)) {
								Map<String, Object> mp=new HashMap<>();
								mp.putAll(ocdla);
								mp.put("accessToken", oat.getValue());
								mp.put("refreshToken", oat.getRefreshToken().getValue());
								mp.put("expiration", oat.getExpiration());
								mp.put("expiresIn", oat.getExpiresIn());
								mp.put("username", authentication.getName());
								rtList.add(mp);
							}
						}
					}
				}
			}
		}
		Page<Map<String, Object>> page = new PageFactory<Map<String, Object>>().page(new PageVo());
		page.setRecords((List<Map<String, Object>>) new SysUserWarpper(rtList).warp());
		return renderString(response, FansResp.successData(page));
	}
	
	
	/**
	 * @Title：removeOauths
	 * @Description: 根据tokens移除用户认证信息
	 * @author: fanhaohao
	 * @date 2018年12月24 16:17:35
	 * @param @param tokens
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "移除用户认证信息")
	@RequiredPermission("sysOauthUsers:removeOauths")
	@RequestMapping(value = "/removeOauths/{tokens}", method = RequestMethod.DELETE)
	public String removeOauths(@PathVariable(value = "tokens") String tokens, HttpServletRequest request, HttpServletResponse response) {
		try {
			String adminName=UserUtil.getUsername();//进行移除操作的管理员名称
			if (BlankUtils.isNotBlank(tokens)) {
				String[] tokensArr = tokens.split(",");
				for(String token:tokensArr){
					removeOauthsByToken(token,request,adminName);
				}
			}else{
				return renderString(response, FansResp.error("移除用户认证信息异常！tokens不能为空"));
			}
		} catch (Exception e) {
			logger.error("移除用户认证信息异常", e);
			return renderString(response, FansResp.error("移除用户认证信息异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	/**
	 * @Title：removeOauthsByToken  
	 * @Description: 移除用户信息
	 * @author: fanhaohao
	 * @date 2019年1月12日 下午11:13:24 
	 * @param @param token
	 * @param @param request
	 * @param @param adminName 
	 * @return void 
	 * @throws
	 */
	private void removeOauthsByToken(String token, HttpServletRequest request,String adminName){
		OAuth2AccessToken accessToken = tokenStore.readAccessToken(token);
		if (BlankUtils.isNotBlank(accessToken)) {
			OAuth2Authentication authentication = tokenStore.readAuthentication(accessToken);
			String delUsername=authentication.getName();
			OAuth2RefreshToken refreshToken = accessToken.getRefreshToken();
			if (BlankUtils.isNotBlank(refreshToken)) {
				tokenStore.removeRefreshToken(refreshToken);// 删除refresh相关信息
			}
			tokenStore.removeAccessToken(accessToken);// 删除token相关信息
			sysLoginLogService.saveLoginLog(request, delUsername , "2", "1", "被管理员"+adminName+"移除成功！");
		}
	}
}

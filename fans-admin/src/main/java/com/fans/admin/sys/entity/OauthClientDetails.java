package com.fans.admin.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;
import com.fans.utils.utils.BlankUtils;

/**
 * @description: 客户端详情
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月11 15:27:01
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="oauth_client_details",resultMap="BaseResultMap")
public class OauthClientDetails extends BaseEntity<OauthClientDetails> {

    private static final long serialVersionUID = 1L;

    /**
     * 客户端ID
     */
    @TableField("client_id")
    private String clientId;
    /**
     * 资源id集合
     */
    @TableField("resource_ids")
    private String resourceIds;
    /**
     * 客户端(client)的访问密匙
     */
    @TableField("client_secret")
    private String clientSecret;
    /**
     * 客户端申请的权限范围
     */
    private String scope;
    /**
     * 指定客户端支持的grant_type,可选值包括authorization_code,password,refresh_token,implicit,client_credentials
     */
    @TableField("authorized_grant_types")
    private String authorizedGrantTypes;
    /**
     * 客户端的重定向URI
     */
    @TableField("web_server_redirect_uri")
    private String webServerRedirectUri;
    /**
     * 指定客户端所拥有的Spring Security的权限值
     */
    private String authorities;
    /**
     * 设定客户端的access_token的有效时间值
     */
    @TableField("access_token_validity")
    private Integer accessTokenValidity;
    /**
     * 设定客户端的refresh_token的有效时间值
     */
    @TableField("refresh_token_validity")
    private Integer refreshTokenValidity;
    /**
     * 预留字段
     */
    @TableField("additional_information")
    private String additionalInformation;
    /**
     * 设置用户是否自动Approval操作, 默认值为 'false', 可选值包括 'true','false'. 
     */
    private String autoapprove;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getResourceIds() {
		return resourceIds;
	}

	public void setResourceIds(String resourceIds) {
		this.resourceIds = resourceIds;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getAuthorizedGrantTypes() {
		return authorizedGrantTypes;
	}

	public void setAuthorizedGrantTypes(String authorizedGrantTypes) {
		this.authorizedGrantTypes = authorizedGrantTypes;
	}

	public String getWebServerRedirectUri() {
		return webServerRedirectUri;
	}

	public void setWebServerRedirectUri(String webServerRedirectUri) {
		this.webServerRedirectUri = webServerRedirectUri;
	}

	public String getAuthorities() {
		return authorities;
	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	public Integer getAccessTokenValidity() {
		return accessTokenValidity;
	}

	public void setAccessTokenValidity(Integer accessTokenValidity) {
		if(BlankUtils.isBlank(accessTokenValidity)||accessTokenValidity==0){
			this.accessTokenValidity = 60 * 60 * 12;//若不设定值则使用默认的有效时间值(60 * 60 * 12, 12小时);
		}else{
			this.accessTokenValidity = accessTokenValidity;
		}
	}

	public Integer getRefreshTokenValidity() {
		return refreshTokenValidity;
	}

	public void setRefreshTokenValidity(Integer refreshTokenValidity) {
		if(BlankUtils.isBlank(refreshTokenValidity)||refreshTokenValidity==0){
			this.refreshTokenValidity = 60 * 60 * 24 * 30;//若不设定值则使用默认的有效时间值(60 * 60 * 24 * 30, 30天);
		}else{
			this.refreshTokenValidity = refreshTokenValidity;
		}
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public String getAutoapprove() {
		return autoapprove;
	}

	public void setAutoapprove(String autoapprove) {
		this.autoapprove = autoapprove;
	}


}

package com.fans.admin.sys.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * @description: 系统_用户Mapper接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月19 17:17:39
 * @author: fanhaohao
 * @version: 1.0
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

	/**
	 * @Title：list  
	 * @Description: 系统_用户分页查询
	 * @author: fanhaohao
	 * @date 2018年12月19 17:17:39
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<SysUser> page, Map<String, Object> map);
	
	/**
	 * @Title：updateLoginResult  
	 * @Description: 根据登录结果更新登录次数时间信息
	 * @author: fanhaohao
	 * @date 2018年12月20日 上午11:39:53 
	 * @param @param username
	 * @param @param bool 
	 * @return void 
	 * @throws
	 */
	public void updateLoginResult(@Param("username") String username, @Param("bool") Boolean bool);

}
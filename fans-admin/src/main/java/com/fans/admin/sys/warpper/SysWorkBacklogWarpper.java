package com.fans.admin.sys.warpper;

import java.util.Map;

import com.fans.common.base.BaseControllerWarpper;

 /**
 * @ClassName: SysWorkBacklogWarpper
 * @description: 待办事项
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月26 14:40:15
 * @author: fanhaohao
 * @version: 1.0
 */
public class SysWorkBacklogWarpper extends BaseControllerWarpper {

	public SysWorkBacklogWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		
	}

}

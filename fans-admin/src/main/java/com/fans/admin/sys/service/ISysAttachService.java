package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysAttach;
import com.baomidou.mybatisplus.service.IService;
 
/**
 * @description: 系统_附件服务类
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月26 14:26:39
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ISysAttachService extends IService<SysAttach> {

	/**
	 * @Title：list  
	 * @Description: 系统_附件分页查询
	 * @author: fanhaohao
	 * @date 2018年12月26 14:26:39
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<SysAttach> page,Map<String, Object> map);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 系统_附件根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:20:42
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 系统_附件根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:20:42
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);
}

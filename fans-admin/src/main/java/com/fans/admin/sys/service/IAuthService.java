package com.fans.admin.sys.service;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.oauth2.provider.token.TokenStore;

import com.fans.utils.utils.FansResp;

public interface IAuthService {
	/**
	 * @Title：getLoginDatas  
	 * @Description: 获取登录后的信息
	 * @author: fanhaohao
	 * @date 2018年12月20日 下午2:06:23 
	 * @param @param username
	 * @param @param password
	 * @param @param menuPerms
	 * @param @return 
	 * @return Map<String, Object> 
	 * @throws
	 */
	public Map<String, Object> getLoginDatas(String username, boolean menuPerms);

	/**
	 * @Title：logout  
	 * @Description: 退出登录
	 * @author: fanhaohao
	 * @date 2018年12月29日 上午9:10:32 
	 * @param @param tokenStore
	 * @param @param username
	 * @param @param request
	 * @param @return 
	 * @return FansResp 
	 * @throws
	 */
	public FansResp logout(HttpServletRequest request,String username,TokenStore tokenStore);

}

package com.fans.admin.sys.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysMenu;

/**
 * @description: 系统_资源Mapper接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月19 15:26:08
 * @author: fanhaohao
 * @version: 1.0
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

	/**
	 * @Title：list  
	 * @Description: 系统_资源分页查询
	 * @author: fanhaohao
	 * @date 2018年12月19 15:26:08
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<SysMenu> page, Map<String, Object> map);

}
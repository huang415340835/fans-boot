package com.fans.admin.sys.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysWorkBacklog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * @description: 待办事项Mapper接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月26 14:26:39
 * @author: fanhaohao
 * @version: 1.0
 */
public interface SysWorkBacklogMapper extends BaseMapper<SysWorkBacklog> {

	/**
	 * @Title：list  
	 * @Description: 待办事项分页查询
	 * @author: fanhaohao
	 * @date 2018年12月26 14:26:39
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<SysWorkBacklog> page, Map<String, Object> map);

}
package com.fans.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fans.admin.sys.dao.SysDictMapper;
import com.fans.admin.sys.entity.SysDict;
import com.fans.admin.sys.service.ISysDictService;
import com.fans.oauths.oauth2.util.UserUtil;

/**
 * @description: 基础字典类型服务实现类
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper,SysDict> implements ISysDictService {

	@Autowired
    private SysDictMapper sysDictMapper;
    
	/**
	 * @Title：list  
	 * @Description: 基础字典类型分页查询
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<SysDict> page,Map<String, Object> map){
		return sysDictMapper.list(page,map);
	}
	
	/**
	 * @Title：insert
	 * @Description: 基础字典添加
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:dict:" }, allEntries = true, beforeInvocation = true)
	public boolean insert(SysDict sysDict){
		return super.insert(sysDict);
	}
	
	/**
	 * @Title：updateById
	 * @Description: 基础字典更新
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:dict:" }, allEntries = true, beforeInvocation = true)
	public boolean updateById(SysDict sysDict){
		return super.updateById(sysDict);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 基础字典类型根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:dict:" }, allEntries = true, beforeInvocation = true)
	public void deleteBatchByIdLogic(Object id){
		SysDict sysDict = new SysDict();
		sysDict.setId(String.valueOf(id));
		sysDict.preUpdate();
		sysDict.setUpdateBy(UserUtil.getUserId());
		sysDict.setDelFlag("1");
		this.updateById(sysDict);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 基础字典类型逻辑批量删除
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:dict:" }, allEntries = true, beforeInvocation = true)
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<SysDict> list = new ArrayList<>();
		for (Object id : ids) {
			SysDict sysDict = new SysDict();
			sysDict.setId(String.valueOf(id));
			sysDict.preUpdate();
			sysDict.setUpdateBy(UserUtil.getUserId());
			sysDict.setDelFlag("1");
			list.add(sysDict);
		}
		this.updateBatchById(list);
	}

	/**
	 * @Title：selectDictMapBycode  
	 * @Description: 根据code来获取字典信息
	 * @author: fanhaohao
	 * @date 2019年1月7日 下午4:54:01 
	 * @param @param code
	 * @param @return 
	 * @return SysDict 
	 * @throws
	 */
	@Override
	@Cacheable(value = { "sys:dict:" }, key = "'sys:dict:'+#code")
	public List<Map<String, Object>> selectDictListBycode(String code) {
		return sysDictMapper.selectDictListBycode(code);
	}
	
}

package com.fans.admin.sys.cache;

import java.util.List;
import java.util.Map;

import com.fans.admin.sys.service.ISysRoleService;
import com.fans.utils.utils.SpringUtil;
/**
 * @ClassName: SysRoleCache  
 * @Description: 角色缓存
 * @author fanhaohao
 * @date 2019年1月10日 上午10:56:22
 */
public class SysRoleCache {
	/**
	 * @Title：getAllRoleList  
	 * @Description: 获取所有的角色节点
	 * @author: fanhaohao
	 * @date 2019年1月8日 上午11:01:44 
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public static List<Map<String, Object>> getAllRoleList() {
		ISysRoleService sysRoleService = SpringUtil.getBean(ISysRoleService.class);
		return sysRoleService.listAll();
	}
}

package com.fans.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.fans.admin.sys.entity.SysWorkBacklog;
import com.fans.admin.sys.dao.SysWorkBacklogMapper;
import com.fans.admin.sys.service.ISysWorkBacklogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.fans.oauths.oauth2.util.UserUtil;

/**
 * @description: 待办事项服务实现类
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月27 10:31:34
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class SysWorkBacklogServiceImpl extends ServiceImpl<SysWorkBacklogMapper,SysWorkBacklog> implements ISysWorkBacklogService {

	@Autowired
    private SysWorkBacklogMapper sysWorkBacklogMapper;
    
	/**
	 * @Title：list  
	 * @Description: 待办事项分页查询
	 * @author: fanhaohao
	 * @date 2018年12月27 10:31:34
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<SysWorkBacklog> page,Map<String, Object> map){
		return sysWorkBacklogMapper.list(page,map);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 待办事项根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:31:34
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdLogic(Object id){
		SysWorkBacklog sysWorkBacklog = new SysWorkBacklog();
		sysWorkBacklog.setId(String.valueOf(id));
		sysWorkBacklog.preUpdate();
		sysWorkBacklog.setUpdateBy(UserUtil.getUserId());
		sysWorkBacklog.setDelFlag("1");
		this.updateById(sysWorkBacklog);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 待办事项逻辑批量删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:31:34
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<SysWorkBacklog> list = new ArrayList<>();
		for (Object id : ids) {
			SysWorkBacklog sysWorkBacklog = new SysWorkBacklog();
			sysWorkBacklog.setId(String.valueOf(id));
			sysWorkBacklog.preUpdate();
			sysWorkBacklog.setUpdateBy(UserUtil.getUserId());
			sysWorkBacklog.setDelFlag("1");
			list.add(sysWorkBacklog);
		}
		this.updateBatchById(list);
	}
}

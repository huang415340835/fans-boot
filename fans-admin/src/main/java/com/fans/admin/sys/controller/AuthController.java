package com.fans.admin.sys.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fans.admin.sys.service.IAuthService;
import com.fans.admin.sys.service.ISysLoginLogService;
import com.fans.admin.sys.service.ISysUserService;
import com.fans.common.base.BaseController;
import com.fans.common.config.captcha.CaptchaService;
import com.fans.oauths.oauth2.redis.service.RedisClientDetailsService;
import com.fans.oauths.oauth2.util.TokenUtil;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.constant.FansRspCon;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;

/**
 * @ClassName: AuthController
 * @Description: 用户登录
 * @author fanhaohao
 * @date 2018年12月13日 下午3:55:43
 */
@Controller
@RequestMapping("/v1/auth")
public class AuthController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Resource
    private CaptchaService captchaService;

	// redis clientdetial工具类
	@Autowired(required = false)
	private RedisClientDetailsService redisClientDetailsService;

	// jdbc clientdetial工具类
	@Autowired(required = false)
	private JdbcClientDetailsService jdbcClientDetailsService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private AuthorizationServerTokenServices authorizationServerTokenServices;

	@Autowired
	private IAuthService authService;

	@Autowired
	private TokenStore tokenStore;

	@Autowired
	private ISysUserService sysUserService;

	@Autowired
	private ISysLoginLogService sysLoginLogService;

    /***
     * 用户登录
     * @return
     */
	@RequestMapping(value = "/login")
	public String login(String username, String password, String captcha,String clientId,String clientSecret,String scope,String grantType, HttpServletRequest request,
			HttpServletResponse response) {
		Integer code = FansRspCon.FLAG_PARAM_ERROR.getCode();
		String msg = "";
		try {
			if (BlankUtils.isBlank(username)) {
				msg="用户名不能为空!";
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			}
			if (BlankUtils.isBlank(password)) {
				msg="密码不能为空!";
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			}
			if (BlankUtils.isBlank(captcha)) {
				msg="验证码不能为空!";
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			}
			if (BlankUtils.isBlank(clientId)) {
				msg="客户端ID不能为空!";
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			}
			if (BlankUtils.isBlank(clientSecret)) {
				msg="客户端密码不能为空!";
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			}
			if (BlankUtils.isBlank(scope)) {
				msg="客户端域不能为空!";
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			}
			if (BlankUtils.isBlank(grantType)) {
				msg="客户端授权类型不能为空!";
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			}
			
			if(!captchaService.valid(captcha)){
				msg="验证码错误!";
				sysLoginLogService.saveLoginLog(request, username, "1", "2", msg);
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			}
			
			// 从redis或者jdbc中获取ClientDetails信息
			ClientDetails clientDetails = getClientDetailsService().loadClientByClientId(clientId);

			if (BlankUtils.isBlank(clientDetails)) {
				msg="clientId对应的信息不存在!";
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			} else if (!StringUtils.equals(clientDetails.getClientSecret(), clientSecret)) {
				msg="clientSecret不匹配!";
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			}
			if(!clientDetails.getScope().contains(scope)){
				msg="客户端域信息错误!";
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			}
			if(!clientDetails.getAuthorizedGrantTypes().contains(grantType)){
				msg="客户端授权类型错误!";
				return renderString(response, FansResp.error(code, msg));// 数据验证失败
			}

			TokenRequest tokenRequest = new TokenRequest(MapUtils.EMPTY_MAP, clientId, clientDetails.getScope(),grantType);

			OAuth2Request oAuth2Request = tokenRequest.createOAuth2Request(clientDetails);

			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);

			Authentication authentication = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);

			OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(oAuth2Request, authentication);

			OAuth2AccessToken oAuth2AccessToken = authorizationServerTokenServices.createAccessToken(oAuth2Authentication);
					
			oAuth2Authentication.setAuthenticated(true);
			
			//登录成功后获取相应的数据
			Map<String, Object> datas = authService.getLoginDatas(username, true);
			datas.put("authInfo", oAuth2AccessToken);
			sysLoginLogService.saveLoginLog(request, username, "1", "1", "登录成功！");
			return renderString(response, FansResp.successData(datas));
		} catch (Exception e) {
			logger.info("登录异常：", e);
			msg="用户名或密码错误！";
			sysUserService.updateLoginResult(username, false);// 登录失败，更新登录失败错误
			sysLoginLogService.saveLoginLog(request, username, "1", "2", msg);
			return renderString(response, FansResp.error(code, msg));// 数据验证失败
		}
	}

	private ClientDetailsService getClientDetailsService() {
		if (BlankUtils.isNotBlank(redisClientDetailsService)) {
			return redisClientDetailsService;
		} else if (BlankUtils.isNotBlank(jdbcClientDetailsService)) {
			return jdbcClientDetailsService;
		}
		return null;
	}

    /**
     * 是否登录
     * @return
     */
	@RequestMapping(value = "/isLogined")
	public String isLogined(HttpServletRequest request, HttpServletResponse response) {
		// 进行token验证
		Boolean validateToken = TokenUtil.validateToken();
		if (validateToken) {
			return renderString(response, FansResp.success());
		} else {
			return renderString(response, FansResp.error());
		}
	}

	/**
	 * @Title：logout  
	 * @Description: 退出登录
	 * @author: fanhaohao
	 * @date 2018年12月20日 下午2:22:25 
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequestMapping(value = "/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		return renderString(response, authService.logout(request, UserUtil.getUsername(), tokenStore));
	}

    /**
	 * 获取验证码
	 *
	 * @return
	 */
    @RequestMapping(value = "/captcha")
	public void captcha(HttpServletRequest request, HttpServletResponse response) {
        captchaService.buildCaptcha(response);
    }

}
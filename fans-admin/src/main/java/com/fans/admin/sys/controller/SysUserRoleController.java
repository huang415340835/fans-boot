package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysUserRole;
import com.fans.admin.sys.service.ISysUserRoleService;
import com.fans.admin.sys.warpper.SysUserRoleWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 系统_用户角色数据接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月25 09:57:55
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/sysUserRole")
public class SysUserRoleController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysUserRoleController.class);

    @Autowired
    private ISysUserRoleService sysUserRoleService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取系统_用户角色
     * @author: fanhaohao
     * @date 2018年12月25 09:57:55
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		SysUserRole sysUserRole = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysUserRole = sysUserRoleService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取系统_用户角色异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_用户角色异常", e);
			return renderString(response, FansResp.error("根据id获取系统_用户角色异常！"));
		}
		return renderString(response, FansResp.successData(sysUserRole));
	}

	/**
	 * @Title：list  
	 * @Description: 获取系统_用户角色列表
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysUserRole:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysUserRole sysUserRole, PageVo pv, String condition, HttpServletResponse response) {
		Page<SysUserRole> page = new PageFactory<SysUserRole>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(sysUserRole);
		map.put("condition", condition);
		List<Map<String, Object>> result = sysUserRoleService.list(page, map);
		page.setRecords((List<SysUserRole>) new SysUserRoleWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：create 
	 * @Description: 新建系统_用户角色
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建系统_用户角色")
	@RequiredPermission("sysUserRole:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(SysUserRole sysUserRole, HttpServletRequest request, HttpServletResponse response) {
		try {
			sysUserRole.preInsert();
			sysUserRole.setCreateBy(UserUtil.getUserId());
			sysUserRoleService.insert(sysUserRole);
		} catch (Exception e) {
			logger.error("新建系统_用户角色异常", e);
			return renderString(response, FansResp.error("新建系统_用户角色异常！"));
		}
		return renderString(response, FansResp.successData(sysUserRole));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改系统_用户角色
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改系统_用户角色")
	@RequiredPermission("sysUserRole:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(SysUserRole sysUserRole, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(sysUserRole.getId())) {
				sysUserRole.preUpdate();
				sysUserRole.setUpdateBy(UserUtil.getUserId());
				sysUserRoleService.updateById(sysUserRole);
			}else{
				return renderString(response, FansResp.error("修改系统_用户角色异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改系统_用户角色异常", e);
			return renderString(response, FansResp.error("修改系统_用户角色异常！"));
		}
		return renderString(response, FansResp.successData(sysUserRole));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 系统_用户角色
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除系统_用户角色")
	@RequiredPermission("sysUserRole:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysUserRoleService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除系统_用户角色异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除系统_用户角色异常", e);
			return renderString(response, FansResp.error("删除系统_用户角色异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	
}

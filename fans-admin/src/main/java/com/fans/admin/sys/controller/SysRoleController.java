package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.cache.SysRoleCache;
import com.fans.admin.sys.entity.SysRole;
import com.fans.admin.sys.service.ISysRoleService;
import com.fans.admin.sys.warpper.SysRoleWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 系统_角色数据接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月25 09:57:55
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/sysRole")
public class SysRoleController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysRoleController.class);

    @Autowired
    private ISysRoleService sysRoleService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取系统_角色
     * @author: fanhaohao
     * @date 2018年12月25 09:57:55
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		SysRole sysRole = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysRole = sysRoleService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取系统_角色异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_角色异常", e);
			return renderString(response, FansResp.error("根据id获取系统_角色异常！"));
		}
		return renderString(response, FansResp.successData(sysRole));
	}

	/**
	 * @Title：list  
	 * @Description: 获取系统_角色列表
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysRole:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysRole sysRole, PageVo pv, String condition, HttpServletResponse response) {
		Page<SysRole> page = new PageFactory<SysRole>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(sysRole);
		map.put("condition", condition);
		List<Map<String, Object>> result = sysRoleService.list(page, map);
		page.setRecords((List<SysRole>) new SysRoleWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}
	
	/**
	 * @Title：listAll  
	 * @Description: 获取系统_角色列表
	 * @author: fanhaohao
	 * @date 2019年1月10日 上午9:14:00 
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@RequiredPermission("sysRole:listAll")
	@RequestMapping(value = "/listAll", method = RequestMethod.GET)
	public Object listAll(HttpServletResponse response) {
		Map<String, Object> datas = new HashMap<>();
		List<Map<String, Object>> result = SysRoleCache.getAllRoleList();
		datas.put("records", result);
		return renderString(response, FansResp.successData(datas));
	}

	/**
	 * @Title：create  
	 * @Description: 新建系统_角色
	 * @author: fanhaohao
	 * @date 2019年01月02 16:04:16
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建系统_角色")
	@RequiredPermission("sysRole:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(SysRole sysRole, HttpServletRequest request, HttpServletResponse response) {
		try {
			sysRole.preInsert();
			sysRole.setCreateBy(UserUtil.getUserId());
			sysRoleService.insert(sysRole);
		} catch (Exception e) {
			logger.error("新建系统_角色异常", e);
			return renderString(response, FansResp.error("新建系统_角色异常！"));
		}
		return renderString(response, FansResp.successData(sysRole));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改系统_角色
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改系统_角色")
	@RequiredPermission("sysRole:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(SysRole sysRole, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(sysRole.getId())) {
				sysRole.preUpdate();
				sysRole.setUpdateBy(UserUtil.getUserId());
				sysRoleService.updateById(sysRole);
			}else{
				return renderString(response, FansResp.error("修改系统_角色异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改系统_角色异常", e);
			return renderString(response, FansResp.error("修改系统_角色异常！"));
		}
		return renderString(response, FansResp.successData(sysRole));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 系统_角色
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除系统_角色")
	@RequiredPermission("sysRole:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysRoleService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除系统_角色异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除系统_角色异常", e);
			return renderString(response, FansResp.error("删除系统_角色异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	
}

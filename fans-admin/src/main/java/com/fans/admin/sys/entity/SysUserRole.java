package com.fans.admin.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;

/**
 * @description: 系统_用户角色
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月20 14:32:42
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="sys_user_role",resultMap="BaseResultMap")
public class SysUserRole extends BaseEntity<SysUserRole> {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    @TableField("role_id")
    private String roleId;
    /**
     * 用户id
     */
    @TableField("user_id")
    private String userId;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


}

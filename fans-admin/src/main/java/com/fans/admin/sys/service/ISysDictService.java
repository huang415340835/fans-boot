package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.sys.entity.SysDict;
 
/**
 * @description: 基础字典类型服务类
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ISysDictService extends IService<SysDict> {

	/**
	 * @Title：list  
	 * @Description: 基础字典类型分页查询
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<SysDict> page,Map<String, Object> map);
	
	/**
	 * @Title：insert
	 * @Description: 基础字典添加
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	public  boolean insert(SysDict sysDict);
	
	/**
	 * @Title：updateById
	 * @Description: 基础字典更新
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	public  boolean updateById(SysDict sysDict);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 基础字典类型根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 基础字典类型根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);

	/**
	 * @Title：selectDictListBycode
	 * @Description: 根据code来获取字典信息
	 * @author: fanhaohao
	 * @date 2019年1月7日 下午4:54:01 
	 * @param @param code
	 * @param @return 
	 * @return SysDict 
	 * @throws
	 */
	public List<Map<String, Object>> selectDictListBycode(String code);
	
}

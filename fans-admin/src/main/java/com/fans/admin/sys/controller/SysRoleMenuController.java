package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.cache.SysMenuCache;
import com.fans.admin.sys.entity.SysRoleMenu;
import com.fans.admin.sys.service.ISysMenuService;
import com.fans.admin.sys.service.ISysRoleMenuService;
import com.fans.admin.sys.warpper.SysRoleMenuWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 系统_角色资源数据接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月25 09:57:55
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/sysRoleMenu")
public class SysRoleMenuController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysRoleMenuController.class);

    @Autowired
    private ISysRoleMenuService sysRoleMenuService;
    
	@Autowired
	private ISysMenuService sysMenuService;

	/**
     * @Title：getById
     * @Description: 根据id获取系统_角色资源
     * @author: fanhaohao
     * @date 2018年12月25 09:57:55
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		SysRoleMenu sysRoleMenu = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysRoleMenu = sysRoleMenuService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取系统_角色资源异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_角色资源异常", e);
			return renderString(response, FansResp.error("根据id获取系统_角色资源异常！"));
		}
		return renderString(response, FansResp.successData(sysRoleMenu));
	}

	/**
	 * @Title：list  
	 * @Description: 获取系统_角色资源列表
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysRoleMenu:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysRoleMenu sysRoleMenu, PageVo pv, String condition, HttpServletResponse response) {
		Page<SysRoleMenu> page = new PageFactory<SysRoleMenu>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(sysRoleMenu);
		map.put("condition", condition);
		List<Map<String, Object>> result = sysRoleMenuService.list(page, map);
		page.setRecords((List<SysRoleMenu>) new SysRoleMenuWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：create  
	 * @Description: 新建系统_角色资源
	 * @author: fanhaohao
	 * @date 2019年01月02 16:04:16
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建系统_角色资源")
	@RequiredPermission("sysRoleMenu:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String save(SysRoleMenu sysRoleMenu, HttpServletRequest request, HttpServletResponse response) {
		try {
			sysRoleMenu.preInsert();
			sysRoleMenu.setCreateBy(UserUtil.getUserId());
			sysRoleMenuService.insert(sysRoleMenu);
		} catch (Exception e) {
			logger.error("新建系统_角色资源异常", e);
			return renderString(response, FansResp.error("新建系统_角色资源异常！"));
		}
		return renderString(response, FansResp.successData(sysRoleMenu));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改系统_角色资源
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改系统_角色资源")
	@RequiredPermission("sysRoleMenu:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(SysRoleMenu sysRoleMenu, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(sysRoleMenu.getId())) {
				sysRoleMenu.preUpdate();
				sysRoleMenu.setUpdateBy(UserUtil.getUserId());
				sysRoleMenuService.updateById(sysRoleMenu);
			}else{
				return renderString(response, FansResp.error("修改系统_角色资源异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改系统_角色资源异常", e);
			return renderString(response, FansResp.error("修改系统_角色资源异常！"));
		}
		return renderString(response, FansResp.successData(sysRoleMenu));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 系统_角色资源
	 * @author: fanhaohao
	 * @date 2018年12月25 09:57:55
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除系统_角色资源")
	@RequiredPermission("sysRoleMenu:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysRoleMenuService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除系统_角色资源异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除系统_角色资源异常", e);
			return renderString(response, FansResp.error("删除系统_角色资源异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	/**
	 * @Title：getRoleMenu 
	 * @Description: 获得所有资源菜单与角色所拥有的资源列表
	 * @author: fanhaohao
	 * @date 2018年12月27日 下午3:49:45 
	 * @param @param roleId
	 * @param @return 
	 * @return ServiceResult 
	 * @throws
	 */
	@RequestMapping(value = "/getRoleMenu", method = RequestMethod.GET)
	public String getRoleMenu(String roleId, HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> datas = new HashMap<>();
		// 获取该角色所拥有的资源id
		List<String> menuIds = sysRoleMenuService.getMenuIdsByRoleId(roleId);
		datas.put("selected", menuIds);
		// 获取 树菜单list；
		Map<String, Object> map = new HashMap<>();
		map.put("delFlag", "0");
		List<Map<String, Object>> menuList = SysMenuCache.getAllMenuList();
		datas.put("records", menuList);
		return renderString(response, FansResp.successData(datas));
    }

	/**
	 * @Title：saveRoleMenu  
	 * @Description: 保存角色资源菜单
	 * @author: fanhaohao
	 * @date 2018年12月27日 下午4:54:19 
	 * @param @param ids
	 * @param @param roleId
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@RequestMapping(value = "/saveRoleMenu", method = RequestMethod.POST)
	public String saveRoleMenu(String ids, String roleId, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(roleId)) {
				//保存角色资源菜单
				sysRoleMenuService.saveRoleMenu(ids, roleId);
			}else{
				return renderString(response, FansResp.error("保存角色资源菜单异常！角色id不能为空"));
			}
		} catch (Exception e) {
			logger.error("保存角色资源菜单异常", e);
			return renderString(response, FansResp.error("保存角色资源菜单异常！"));
		}
		return renderString(response, FansResp.success());
	}
}

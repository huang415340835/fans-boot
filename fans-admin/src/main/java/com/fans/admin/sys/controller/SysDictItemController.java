package com.fans.admin.sys.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysDictItem;
import com.fans.admin.sys.service.ISysDictItemService;
import com.fans.admin.sys.warpper.SysDictItemWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 字典项数据接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/sysDictItem")
public class SysDictItemController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysDictItemController.class);

    @Autowired
    private ISysDictItemService sysDictItemService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取字典项
     * @author: fanhaohao
     * @date 2018年12月30 18:18:58
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		SysDictItem sysDictItem = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysDictItem = sysDictItemService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取字典项异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取字典项异常", e);
			return renderString(response, FansResp.error("根据id获取字典项异常！"));
		}
		return renderString(response, FansResp.successData(sysDictItem));
	}

	/**
	 * @Title：list  
	 * @Description: 获取字典项列表
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysDictItem:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysDictItem sysDictItem, PageVo pv, String condition, HttpServletResponse response) {
		pv.setSortField("showOrder");
		pv.setSortOrder("asc");
		Page<SysDictItem> page = new PageFactory<SysDictItem>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(sysDictItem);
		map.put("condition", condition);
		List<Map<String, Object>> result = sysDictItemService.list(page, map);
		page.setRecords((List<SysDictItem>) new SysDictItemWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：create  
	 * @Description: 新建字典项
	 * @author: fanhaohao
	 * @date 2019年01月02 16:04:16
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建字典项")
	@RequiredPermission("sysDictItem:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(SysDictItem sysDictItem, HttpServletRequest request, HttpServletResponse response) {
		try {
			if(BlankUtils.isBlank(sysDictItem.getShowOrder())){
				BigDecimal shoswOrder = new BigDecimal("0");
				sysDictItem.setShowOrder(shoswOrder);
			}
			sysDictItem.preInsert();
			sysDictItem.setCreateBy(UserUtil.getUserId());
			sysDictItemService.insert(sysDictItem);
		} catch (Exception e) {
			logger.error("新建字典项异常", e);
			return renderString(response, FansResp.error("新建字典项异常！"));
		}
		return renderString(response, FansResp.successData(sysDictItem));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改字典项
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改字典项")
	@RequiredPermission("sysDictItem:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(SysDictItem sysDictItem, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(sysDictItem.getId())) {
				sysDictItem.preUpdate();
				sysDictItem.setUpdateBy(UserUtil.getUserId());
				sysDictItemService.updateById(sysDictItem);
			}else{
				return renderString(response, FansResp.error("修改字典项异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改字典项异常", e);
			return renderString(response, FansResp.error("修改字典项异常！"));
		}
		return renderString(response, FansResp.successData(sysDictItem));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 字典项
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除字典项")
	@RequiredPermission("sysDictItem:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysDictItemService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除字典项异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除字典项异常", e);
			return renderString(response, FansResp.error("删除字典项异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	
}

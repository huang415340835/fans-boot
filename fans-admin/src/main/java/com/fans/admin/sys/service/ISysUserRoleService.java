package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.sys.entity.SysRole;
import com.fans.admin.sys.entity.SysUserRole;
 
/**
 * @description: 系统_用户角色服务类
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月19 15:24:24
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

	/**
	 * @Title：list  
	 * @Description: 系统_用户角色分页查询
	 * @author: fanhaohao
	 * @date 2018年12月19 15:24:24
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<SysUserRole> page,Map<String, Object> map);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 系统_用户角色根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:20:42
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 系统_用户角色根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:20:42
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);
	
	/**
	 * @Title：listByUserId  
	 * @Description: 根据用户id和角色id来获取角色信息
	 * @author: fanhaohao
	 * @date 2018年12月20日 下午2:01:37 
	 * @param @param userId
	 * @param @param roleId
	 * @param @return 
	 * @return List<SysRole> 
	 * @throws
	 */
	public List<SysRole> listByUserId(String userId, String roleId);
	
	/**
	 * roleIdListByUserId  
	 * @Description: 根据用户id来获取用户所拥有的角色id
	 * @author: fanhaohao
	 * @date 2018年12月20日 下午2:01:37 
	 * @param @param userId
	 * @param @return 
	 * @return List<SysRole> 
	 * @throws
	 */
	public List<String> roleIdListByUserId(String userId);

	/**
	 * @Title：deleteBatchByRoleIds  
	 * @Description: 根据roleid list集合来删除
	 * @author: fanhaohao
	 * @date 2018年12月27日 下午5:19:40 
	 * @param @param roleIdList
	 * @param @param userId 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByRoleIds(List<String> roleIdList, String userId);
}

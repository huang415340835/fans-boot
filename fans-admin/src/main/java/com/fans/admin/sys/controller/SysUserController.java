package com.fans.admin.sys.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysUser;
import com.fans.admin.sys.service.IOauthClientDetailsService;
import com.fans.admin.sys.service.ISysUserService;
import com.fans.admin.sys.warpper.SysUserWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.constant.SysConstant;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 系统_用户数据接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月24 16:17:35
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/sysUser")
public class SysUserController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysUserController.class);

    @Autowired
    private ISysUserService sysUserService;
    
    @Autowired
    private IOauthClientDetailsService oauthClientDetailsService;
    
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private TokenStore tokenStore;

	/**
     * @Title：getById
     * @Description: 根据id获取系统_用户
     * @author: fanhaohao
     * @date 2018年12月24 16:17:35
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		SysUser sysUser = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysUser = sysUserService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取系统_用户异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取系统_用户异常", e);
			return renderString(response, FansResp.error("根据id获取系统_用户异常！"));
		}
		return renderString(response, FansResp.successData(sysUser));
	}

	/**
	 * @throws Exception 
	 * @Title：list  
	 * @Description: 获取系统_用户列表
	 * @author: fanhaohao
	 * @date 2018年12月24 16:17:35
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysUser:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysUser sysUser, PageVo pv, String condition, HttpServletResponse response) {
		Page<SysUser> page = new PageFactory<SysUser>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(sysUser);
		map.put("condition", condition);
		List<Map<String, Object>> result = sysUserService.list(page, map);
		page.setRecords((List<SysUser>) new SysUserWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：create
	 * @Description: 创建系统_用户
	 * @author: fanhaohao
	 * @date 2018年12月24 16:17:35
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "创建系统_用户")
	@RequiredPermission("sysUser:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(SysUser sysUser, HttpServletRequest request, HttpServletResponse response) {
		try {
			if ("1".equals(sysUser.getIsLock())) {// 是否锁定，添加锁定时间
				sysUser.setLockTime(new Date());
			}
			sysUser.setPassword(passwordEncoder.encode(SysConstant.INIT_PWD));// 设置默认密码为123456
			sysUser.preInsert();
			sysUser.setCreateBy(UserUtil.getUserId());
			sysUserService.saveUserInfo(sysUser);
		} catch (Exception e) {
			logger.error("创建系统_用户异常", e);
			return renderString(response, FansResp.error("创建系统_用户异常！"));
		}
		return renderString(response, FansResp.successData(sysUser));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改系统_用户
	 * @author: fanhaohao
	 * @date 2018年12月24 16:17:35
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改系统_用户")
	@RequiredPermission("sysUser:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(SysUser sysUser, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(sysUser.getId())) {
				if ("1".equals(sysUser.getIsLock())) {// 是否锁定，锁定时更新锁定时间
					sysUser.setLockTime(new Date());
				}
				sysUser.preUpdate();
				sysUser.setUpdateBy(UserUtil.getUserId());
				sysUserService.updateUserInfo(sysUser);
			}else{
				return renderString(response, FansResp.error("修改系统_用户异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改系统_用户异常", e);
			return renderString(response, FansResp.error("修改系统_用户异常！"));
		}
		return renderString(response, FansResp.successData(sysUser));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 系统_用户
	 * @author: fanhaohao
	 * @date 2018年12月24 16:17:35
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除系统_用户")
	@RequiredPermission("sysUser:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysUserService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除系统_用户异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除系统_用户异常", e);
			return renderString(response, FansResp.error("删除系统_用户异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	/**
	 * @Title：resetPwd  
	 * @Description: 重置密码
	 * @author: fanhaohao
	 * @date 2018年12月25日 下午3:51:25 
	 * @param @param id
	 * @param @return 
	 * @return ServiceResult 
	 * @throws
	 */
	@Log(operation = "重置密码")
	@RequestMapping(value = "/resetPwd", method = RequestMethod.PUT)
	public String resetPwd(String id, HttpServletRequest request, HttpServletResponse response) {
        try {
			if (BlankUtils.isNotBlank(id)) {
				sysUserService.resetPwd(id);
				return renderString(response, FansResp.success());
			} else {
				return renderString(response, FansResp.error("系统_用户重置密码异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("系统_用户重置密码异常！", e);
			return renderString(response, FansResp.error("系统_用户重置密码异常！"));
		}
    }
	
	/**
	 * @Title：updatePwd  
	 * @Description: 修改密码
	 * @author: fanhaohao
	 * @date 2018年12月25日 下午5:28:28 
	 * @param @param pwd 原密码
	 * @param @param newpwd 新密码
	 * @param @param newpwd2 重复新密码
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改密码")
	@RequestMapping(value = "/updatePwd", method = RequestMethod.PUT)
	public String updatePwd(String pwd, String newpwd, String newpwd2, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			if (BlankUtils.isBlank(pwd)) {
				return renderString(response, FansResp.error("系统_用户修改密码异常！原密码不能为空"));
			}
			if (BlankUtils.isBlank(newpwd)) {
				return renderString(response, FansResp.error("系统_用户修改密码异常！新密码不能为空"));
			}
			if (BlankUtils.isBlank(newpwd2)) {
				return renderString(response, FansResp.error("系统_用户修改密码异常！重复新密码不能为空"));
			}
			if (!newpwd.equals(newpwd2)) {
				return renderString(response, FansResp.error("系统_用户修改密码异常！新密码和重复新密码必须一致"));
			}
			String userId = UserUtil.getUserId();
			SysUser sysUser = sysUserService.selectById(userId);
			if(passwordEncoder.matches(pwd, sysUser.getPassword())){
				sysUserService.updatePwd(userId, newpwd);
				return renderString(response, FansResp.success());
			}else{
				return renderString(response, FansResp.error("系统_用户修改密码异常！原密码不正确"));
			}
		} catch (Exception e) {
			logger.error("系统_用户重置密码异常！", e);
			return renderString(response, FansResp.error("系统_用户重置密码异常！"));
		}
	}
}

package com.fans.admin.sys.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fans.common.base.BaseEntity;

/**
 * @description: 系统_资源
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月20 14:32:42
 * @author: fanhaohao
 * @version: 1.0
 */
@TableName(value="sys_menu",resultMap="BaseResultMap")
public class SysMenu extends BaseEntity<SysMenu> {

    private static final long serialVersionUID = 1L;

    /**
     * 标题
     */
    private String name;
    /**
     * 资源标识符
     */
    private String identity;
    /**
     * 菜单url
     */
    private String url;
    /**
     * 父资源
     */
    @TableField("parent_id")
    private String parentId;
    /**
     * 父路径
     */
    @TableField("parent_ids")
    private String parentIds;
    /**
     * 菜单权重
     */
    private Integer weight;
    /**
     * 菜单图标
     */
    private String icon;
    /**
     * 资源类型（1=显示2禁止0=隐藏）
     */
    private String status;
    /**
     * 资源类型（1=菜单2=权限）
     */
	@TableField("menu_type")
	private String menuType;

	/**
	 * 外部链接（0，否，1是）
	 */
	@TableField("out_link")
	private String outLink;

	/**
	 * 显示方式（0，普通内嵌，1，内嵌iframe，2，外窗口）
	 */
	@TableField("display_mode")
	private String displayMode;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public String getOutLink() {
		return outLink;
	}

	public void setOutLink(String outLink) {
		this.outLink = outLink;
	}

	public String getDisplayMode() {
		return displayMode;
	}

	public void setDisplayMode(String displayMode) {
		this.displayMode = displayMode;
	}

}

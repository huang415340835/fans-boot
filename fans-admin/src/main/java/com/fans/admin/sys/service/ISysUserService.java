package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.sys.entity.SysUser;
 
/**
 * @description: 系统_用户服务类
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月19 17:17:39
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ISysUserService extends IService<SysUser> {

	/**
	 * @Title：list  
	 * @Description: 系统_用户分页查询
	 * @author: fanhaohao
	 * @date 2018年12月19 17:17:39
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<SysUser> page,Map<String, Object> map);
	
	/**
	 * @Title：updateLoginResult  
	 * @Description: 根据登录结果更新登录次数时间信息
	 * @author: fanhaohao
	 * @date 2018年12月20日 上午11:38:55 
	 * @param @param username
	 * @param @param bool 
	 * @return void 
	 * @throws
	 */
    void updateLoginResult(String username, Boolean bool);

    /**
     * @Title：selectByUsername  
     * @Description: 根据名字来查询用户信息
     * @author: fanhaohao
     * @date 2018年12月20日 下午2:49:06 
     * @param @param username
     * @param @return 
     * @return SysUser 
     * @throws
     */
	public SysUser selectByUsername(String username);

	/**
	 * @Title：resetPwd  
	 * @Description: 重置用户密码
	 * @author: fanhaohao
	 * @date 2018年12月25日 下午3:53:48 
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void resetPwd(String id);

	/**
	 * @Title：updatePwd  
	 * @Description: 用户修改密码
	 * @author: fanhaohao
	 * @date 2018年12月25日 下午5:32:31 
	 * @param @param userId
	 * @param @param newpwd 
	 * @return void 
	 * @throws
	 */
	public void updatePwd(String userId, String newpwd);

	/**
	 * @Title：saveUserInfo  
	 * @Description: 保存用户信息
	 * @author: fanhaohao
	 * @date 2018年12月26日 下午4:51:51 
	 * @param @param sysUser 
	 * @return void 
	 * @throws
	 */
	public void saveUserInfo(SysUser sysUser);

	/**
	 * @Title：updateUserInfo  
	 * @Description: 更新用户信息
	 * @author: fanhaohao
	 * @date 2018年12月26日 下午4:53:13 
	 * @param @param sysUser 
	 * @return void 
	 * @throws
	 */
	public void updateUserInfo(SysUser sysUser);

	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 系统_用户根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:20:42
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 系统_用户根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:20:42
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);
}

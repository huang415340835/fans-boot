package com.fans.admin.sys.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fans.admin.sys.dao.SysDictItemMapper;
import com.fans.admin.sys.entity.SysDictItem;
import com.fans.admin.sys.service.ISysDictItemService;
import com.fans.oauths.oauth2.util.UserUtil;

/**
 * @description: 字典服务实现类
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
@Service
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper,SysDictItem> implements ISysDictItemService {

	@Autowired
    private SysDictItemMapper sysDictItemMapper;
    
	/**
	 * @Title：list  
	 * @Description: 字典分页查询
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	@Override
	public List<Map<String, Object>> list(Page<SysDictItem> page,Map<String, Object> map){
		return sysDictItemMapper.list(page,map);
	}
	
	/**
	 * @Title：insert
	 * @Description: 基础字典添加
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:dict:" }, allEntries = true, beforeInvocation = true)
	public boolean insert(SysDictItem sysDictItem){
		return super.insert(sysDictItem);
	}
	
	/**
	 * @Title：updateById
	 * @Description: 基础字典更新
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @return 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:dict:" }, allEntries = true, beforeInvocation = true)
	public boolean updateById(SysDictItem sysDictItem){
		return super.updateById(sysDictItem);
	}
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 字典根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:dict:" }, allEntries = true, beforeInvocation = true)
	public void deleteBatchByIdLogic(Object id){
		SysDictItem sysDictItem = new SysDictItem();
		sysDictItem.setId(String.valueOf(id));
		sysDictItem.preUpdate();
		sysDictItem.setUpdateBy(UserUtil.getUserId());
		sysDictItem.setDelFlag("1");
		this.updateById(sysDictItem);
	}
	
	/**
	 * @Title：deleteBatchByIdsLogic    
	 * @Description: 字典逻辑批量删除
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	@Override
	@CacheEvict(value = { "sys:dict:" }, allEntries = true, beforeInvocation = true)
	public void deleteBatchByIdsLogic(List<Object> ids) {
		List<SysDictItem> list = new ArrayList<>();
		for (Object id : ids) {
			SysDictItem sysDictItem = new SysDictItem();
			sysDictItem.setId(String.valueOf(id));
			sysDictItem.preUpdate();
			sysDictItem.setUpdateBy(UserUtil.getUserId());
			sysDictItem.setDelFlag("1");
			list.add(sysDictItem);
		}
		this.updateBatchById(list);
	}
}

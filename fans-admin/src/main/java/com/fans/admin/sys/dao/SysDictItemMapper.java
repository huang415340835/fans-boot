package com.fans.admin.sys.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysDictItem;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * @description: 字典Mapper接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

	/**
	 * @Title：list  
	 * @Description: 字典分页查询
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<SysDictItem> page, Map<String, Object> map);

}
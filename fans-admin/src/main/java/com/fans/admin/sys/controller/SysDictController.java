package com.fans.admin.sys.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.cache.SysDictCache;
import com.fans.admin.sys.entity.SysDict;
import com.fans.admin.sys.service.ISysDictService;
import com.fans.admin.sys.warpper.SysDictWarpper;
import com.fans.common.base.BaseController;
import com.fans.common.utils.PageFactory;
import com.fans.common.vo.PageVo;
import com.fans.oauths.annotations.Log;
import com.fans.oauths.annotations.RequiredPermission;
import com.fans.oauths.oauth2.util.UserUtil;
import com.fans.utils.utils.BlankUtils;
import com.fans.utils.utils.FansResp;
import com.fans.utils.utils.ObjectUtils;

/**
 * @description: 基础字典类型数据接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
@Controller
@RequestMapping("/v1/sys/sysDict")
public class SysDictController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysDictController.class);

    @Autowired
    private ISysDictService sysDictService;
    
	/**
     * @Title：getById
     * @Description: 根据id获取基础字典类型
     * @author: fanhaohao
     * @date 2018年12月30 18:18:58
     * @param @param id
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getById", method = RequestMethod.GET)
	public String getById(String id, HttpServletRequest request, HttpServletResponse response) {
		SysDict sysDict = null;
		try {
			if (BlankUtils.isNotBlank(id)) {
				sysDict = sysDictService.selectById(id);
			}else{
				return renderString(response, FansResp.error("根据id获取基础字典类型异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据id获取基础字典类型异常", e);
			return renderString(response, FansResp.error("根据id获取基础字典类型异常！"));
		}
		return renderString(response, FansResp.successData(sysDict));
	}

	/**
	 * @Title：list  
	 * @Description: 获取基础字典类型列表
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param condition
	 * @param @param response
	 * @param @return 
	 * @return Object 
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	@RequiredPermission("sysDict:list")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Object list(SysDict sysDict, PageVo pv, String condition, HttpServletResponse response) {
		pv.setSortField("showOrder");
		pv.setSortOrder("asc");
		Page<SysDict> page = new PageFactory<SysDict>().page(pv);
		Map<String, Object> map = ObjectUtils.javaBean2Map(sysDict);
		map.put("condition", condition);
		List<Map<String, Object>> result = sysDictService.list(page, map);
		page.setRecords((List<SysDict>) new SysDictWarpper(result).warp());
		return renderString(response, FansResp.successData(page));
	}

	/**
	 * @Title：create  
	 * @Description: 新建基础字典类型
	 * @author: fanhaohao
	 * @date 2019年01月02 16:04:16
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "新建基础字典类型")
	@RequiredPermission("sysDict:create")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String create(SysDict sysDict, HttpServletRequest request, HttpServletResponse response) {
		try {
			if(BlankUtils.isBlank(sysDict.getShowOrder())){
				BigDecimal shoswOrder = new BigDecimal("0");
				sysDict.setShowOrder(shoswOrder);
			}
			sysDict.preInsert();
			sysDict.setCreateBy(UserUtil.getUserId());
			sysDictService.insert(sysDict);
		} catch (Exception e) {
			logger.error("新建基础字典类型异常", e);
			return renderString(response, FansResp.error("新建基础字典类型异常！"));
		}
		return renderString(response, FansResp.successData(sysDict));
	}
	
	/**
	 * @Title：update  
	 * @Description: 修改基础字典类型
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param sysMenu
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "修改基础字典类型")
	@RequiredPermission("sysDict:update")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public String update(SysDict sysDict, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(sysDict.getId())) {
				sysDict.preUpdate();
				sysDict.setUpdateBy(UserUtil.getUserId());
				sysDictService.updateById(sysDict);
			}else{
				return renderString(response, FansResp.error("修改基础字典类型异常！ID不能为空"));
			}
		} catch (Exception e) {
			logger.error("修改基础字典类型异常", e);
			return renderString(response, FansResp.error("修改基础字典类型异常！"));
		}
		return renderString(response, FansResp.successData(sysDict));
	}
	
	/**
	 * @Title：delete  
	 * @Description: 基础字典类型
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param ids
	 * @param @param request
	 * @param @param response
	 * @param @return 
	 * @return String 
	 * @throws
	 */
	@Log(operation = "删除基础字典类型")
	@RequiredPermission("sysDict:delete")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE)
	public String delete(@PathVariable(value = "ids") String ids, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (BlankUtils.isNotBlank(ids)) {
				String[] idsArr = ids.split(",");
				sysDictService.deleteBatchByIdsLogic(Arrays.asList(idsArr));
			}else{
				return renderString(response, FansResp.error("删除基础字典类型异常！IDS不能为空"));
			}
		} catch (Exception e) {
			logger.error("删除基础字典类型异常", e);
			return renderString(response, FansResp.error("删除基础字典类型异常！"));
		}
		return renderString(response, FansResp.success());
	}
	
	/**
     * @Title：getDictMapByCode
     * @Description: 根据code来获取字典信息
     * @author: fanhaohao
     * @date 2018年12月30 18:18:58
     * @param @param code
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getDictListByCode", method = RequestMethod.GET)
	public String getDictListByCode(String code, HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> rt = null;
		try {
			if (BlankUtils.isNotBlank(code)) {
				rt = SysDictCache.getDictList(code);
			}else{
				return renderString(response, FansResp.error("根据code来获取字典信息异常！code不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据code来获取字典信息异常", e);
			return renderString(response, FansResp.error("根据code来获取字典信息异常！"));
		}
		return renderString(response, FansResp.successData(rt));
	}
	
	/**
     * @Title：getDictMapByCode
     * @Description: 根据code来获取字典信息
     * @author: fanhaohao
     * @date 2018年12月30 18:18:58
     * @param @param code
     * @param @param request
     * @param @param response
     * @param @return 
     * @return String 
     * @throws
     */
	@RequestMapping(value = "/getDictMapByCode", method = RequestMethod.GET)
	public String getDictMapByCode(String code, HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> rt = null;
		try {
			if (BlankUtils.isNotBlank(code)) {
				rt = SysDictCache.getDictMap(code);
			}else{
				return renderString(response, FansResp.error("根据code来获取字典信息异常！code不能为空"));
			}
		} catch (Exception e) {
			logger.error("根据code来获取字典信息异常", e);
			return renderString(response, FansResp.error("根据code来获取字典信息异常！"));
		}
		return renderString(response, FansResp.successData(rt));
	}
	
}

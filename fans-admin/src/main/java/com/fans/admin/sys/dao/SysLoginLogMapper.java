package com.fans.admin.sys.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysLoginLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * @description: 用户登录日志Mapper接口
 * @copyright: 版权所有 fans (c)2019
 * @date: 2019年01月11 10:01:55
 * @author: fanhaohao
 * @version: 1.0
 */
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {

	/**
	 * @Title：list  
	 * @Description: 用户登录日志分页查询
	 * @author: fanhaohao
	 * @date 2019年01月11 10:01:55
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<SysLoginLog> page, Map<String, Object> map);

	/**
	 * @Title：getAllIds  
	 * @Description: 获取所有的日志id
	 * @author: fanhaohao
	 * @date 2019年1月3日 下午4:42:24 
	 * @param @return 
	 * @return List<String> 
	 * @throws
	 */
	public List<String> getAllIds();
}
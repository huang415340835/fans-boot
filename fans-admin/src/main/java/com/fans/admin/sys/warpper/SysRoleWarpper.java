package com.fans.admin.sys.warpper;

import java.util.Map;

import com.fans.admin.sys.cache.SysDictCache;
import com.fans.common.base.BaseControllerWarpper;
import com.fans.utils.utils.BlankUtils;

 /**
 * @ClassName: SysRoleWarpper
 * @description: 系统_角色
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月19 14:53:59
 * @author: fanhaohao
 * @version: 1.0
 */
public class SysRoleWarpper extends BaseControllerWarpper {

	public SysRoleWarpper(Object obj) {
		super(obj);
    }
    
	/**
	 * 进行字段转换
	 */
	@Override
	protected void warpTheMap(Map<String, Object> map) {
		/*String status = String.valueOf(map.get("status"));
		if (BlankUtils.isNotBlank(status)) {
			Object statusText = SysDictCache.getDictMap("role_status").get(status);
			map.put("status", BlankUtils.isNotBlank(statusText) ? statusText : "");
		}*/
	}

}

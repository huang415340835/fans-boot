package com.fans.admin.sys.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.fans.admin.sys.entity.SysMenu;
import com.fans.admin.sys.entity.SysRoleMenu;
import com.fans.admin.sys.vo.RoleMenuPerms;
 
/**
 * @description: 系统_角色资源服务类
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月19 15:24:24
 * @author: fanhaohao
 * @version: 1.0
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

	/**
	 * @Title：list  
	 * @Description: 系统_角色资源分页查询
	 * @author: fanhaohao
	 * @date 2018年12月19 15:24:24
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(Page<SysRoleMenu> page,Map<String, Object> map);
	
	/**
	 * @Title：getRoleMenuAndPerms  
	 * @Description: 根据角色id获得权限集合和菜单列表
	 * @author: fanhaohao
	 * @date 2018年12月20日 下午2:58:14 
	 * @param @param roleIds
	 * @param @return 
	 * @return RoleMenuPerms 
	 * @throws
	 */
    RoleMenuPerms getRoleMenuAndPerms(String roleIds);

    /**
     * @Title：getRoleMenuList  
     * @Description: 根据角色ids和菜单类型来获取菜单列表
     * @author: fanhaohao
     * @date 2018年12月20日 下午3:02:20 
     * @param @param roleIds
     * @param @param menuType
     * @param @return 
     * @return List<SysMenu> 
     * @throws
     */
	List<SysMenu> getRoleMenuList(String roleIds, String menuType);
	
	/**
	 * @Title：deleteBatchByIdLogic  
	 * @Description: 系统_角色资源根据id逻辑删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:20:42
	 * @param @param id 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdLogic(Object id);
	
	/**
	 * @Title：deleteBatchByIdsLogic  
	 * @Description: 系统_角色资源根据ids逻辑批量删除
	 * @author: fanhaohao
	 * @date 2018年12月27 10:20:42
	 * @param @param ids 
	 * @return void 
	 * @throws
	 */
	public void deleteBatchByIdsLogic(List<Object> ids);
	
	/**
	 * @Title：getMenuIdsByRoleId  
	 * @Description: 根据 角色id来查询menu id的list集合
	 * @author: fanhaohao
	 * @date 2018年12月27日 下午3:57:38 
	 * @param @param roleId
	 * @param @return 
	 * @return List<String> 
	 * @throws
	 */
	public List<String> getMenuIdsByRoleId(String roleId);
	
	/**
	 * @Title：saveRoleMenu  
	 * @Description: 保存角色资源菜单
	 * @author: fanhaohao
	 * @date 2018年12月27日 下午4:57:28 
	 * @param @param menuIds
	 * @param @param roleId 
	 * @return void 
	 * @throws
	 */
	public void saveRoleMenu(String ids, String roleId);
}

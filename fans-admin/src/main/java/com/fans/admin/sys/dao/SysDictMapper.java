package com.fans.admin.sys.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fans.admin.sys.entity.SysDict;

/**
 * @description: 基础字典类型Mapper接口
 * @copyright: 版权所有 fans (c)2018
 * @date: 2018年12月30 18:18:58
 * @author: fanhaohao
 * @version: 1.0
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

	/**
	 * @Title：list  
	 * @Description: 基础字典类型分页查询
	 * @author: fanhaohao
	 * @date 2018年12月30 18:18:58
	 * @param @param page
	 * @param @param map
	 * @param @return 
	 * @return List<Map<String,Object>> 
	 * @throws
	 */
	public List<Map<String, Object>> list(@Param("page") Page<SysDict> page, Map<String, Object> map);

	/**
	 * @Title：selectDictListBycode  
	 * @Description: 根据code来获取字典信息
	 * @author: fanhaohao
	 * @date 2019年1月7日 下午4:57:15 
	 * @param @param code
	 * @param @return 
	 * @return Map<String,Object> 
	 * @throws
	 */
	public List<Map<String, Object>> selectDictListBycode(@Param("code") String code);

}